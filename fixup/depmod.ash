#!/bin/ash

for KVER in /lib/modules/*
do
	KVER=$(basename "$KVER")
	depmod -b / "$KVER"
done
