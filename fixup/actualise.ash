#!/bin/ash
#
# Replace xxx with xxx.actual
#
# This is used to override package-extracted files
# with ones that CMake produces with .actual added
#
# From: Rick van Rein <rick@openfortress.nl>


for p in $(find . -name \*.actual)
do
	cp "$p" "${p%.actual}"
done
