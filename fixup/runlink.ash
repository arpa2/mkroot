#!/bin/ash
#
# Make a symbolic link from /run to /var/run
#
# From: Rick van Rein <rick@openfortress.nl>

[ -h run  ] || ln -s /var/run run
