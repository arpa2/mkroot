#!/bin/ash
#
# Create harmless / common device nodes
#
# From: Rick van Rein <rick@openfortress.nl>



havedev() {
	if [ ! -b "$1" ]
	then
		if [ ! -c "$1" ]
		then
			mknod "$@"
		fi
	fi
}

havedev dev/null c 1  3
