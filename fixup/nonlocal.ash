#!/bin/ash
#
# Move /usr/local/* to /usr/*
#
# From: Rick van Rein <rick@openfortress.nl>


for DIR in bin sbin
do
	for FILE in $(ls "usr/local/$DIR/" 2>/dev/null)
	do
		mv "usr/local/$DIR/$FILE" "usr/$DIR/$FILE"
	done
done

