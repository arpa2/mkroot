#!/bin/ash
#
# Run ldconfig to setup library paths
#
# This command is difficult when run under fakechroot.
# To remedy that, we use -r to set the root dir in the
# command.  We remove the fakechroot setup to do this.
#
# From: Rick van Rein <rick@openfortress.nl>

/sbin/ldconfig ${FAKECHROOT_BASE:+-r ${FAKECHROOT_BASE}} /usr/local/lib

