#!/bin/ash
#
# dropbearkeys.sh -- Setup keys for use in the dropbear SSH daemon
#
# From: Rick van Rein <rick@openfortress.nl>


# Create /dev/urandom but cleanup afterwards if our root status is fake
#
CLEANUP=""
echo Pre-test dev/urandom and /dev/urandom:
stat dev/urandom
hexdump dev/urandom | head
stat /dev/urandom
hexdump /dev/urandom | head
if [ ! -c dev/urandom ]
then
	rm -f dev/urandom
	mknod dev/urandom c 1 9
	[ $? -ne 0 ] && exit 1
	echo Post-create dev/urandom and /dev/urandom:
	stat dev/urandom
	hexdump dev/urandom | head
	stat /dev/urandom
	hexdump /dev/urandom | head
	if [ -n "$FAKEROOTKEY" ]
	then
		CLEANUP="dev/urandom"
	fi
fi


# Make sure to have an ECDSA host key, but do not overwrite an existing one
#
if [ ! -r /etc/dropbear/dropbear_ecdsa_host_key ]
then
	mkdir -p /etc/dropbear
	chmod go-rwx /etc/dropbear
	LIBS=$(cat etc/ld.so.conf etc/ld.so.conf.d/* | sed -e '/^[^/]/d' -e "s+^+${FAKECHROOT_BASE:+$FAKECHROOT_BASE/}$(pwd)+")
	export LD_LIBRARY_PATH=${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}$(echo $LIBS | sed 's+ +:+g')
	echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
	echo Pre-keygen dev/urandom and /dev/urandom:
	stat dev/urandom
	hexdump dev/urandom | head
	stat /dev/urandom
	hexdump /dev/urandom | head
	dropbearkey -t ecdsa -s 256 -f /etc/dropbear/dropbear_ecdsa_host_key
	RETVAL=$?
else
	echo 'Continue with existing ECDSA dropkearkey'
	RETVAL=0
fi


# Cleanup things that were only possible through fakeroot
#
[ -n "$CLEANUP" ] && rm -f $CLEANUP
echo Post-cleanup dev/urandom and /dev/urandom:
stat dev/urandom
stat /dev/urandom


# Return the result from key generation
#
return $RETVAL
