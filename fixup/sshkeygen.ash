#!/bin/ash
#
# Generate SSH server keys -- if none exist yet
#
# From: Rick van Rein <rick@openfortress.nl>


if [ ! -r /etc/ssh/ssh_host_rsa_key ]
then
	ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa -b 3168
fi

if [ ! -r /etc/ssh/ssh_host_ecdsa_key ]
then
	ssh-keygen -f /etc/ssh/ssh_host_ecdsa_key -N '' -t ecdsa -b 521
fi

if [ ! -r /etc/ssh/ssh_host_ed25519_key ]
then
	ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -N '' -t ed25519
fi
