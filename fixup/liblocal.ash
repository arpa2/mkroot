#!/bin/ash
#
# Move files from /usr/local/lib to /usr/lib
#
# From: Rick van Rein <rick@openfortress.nl>


for f in usr/local/lib/*.so
do
	mv "$f" usr/lib/
done
