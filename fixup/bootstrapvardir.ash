#!/bin/ash
#
# Any _new_ files in /var are copied to ../vardir
#
# This is a NO_CHROOT script.  Be careful, it might damage your host.
#
# From: Rick van Rein <rick@openfortress.nl>


cp -pnr rootfs/var/* ../vardir/
