#!/bin/ash
#
# Remote locale and other i18n stuff -- it occupies unused space
#
# From: Rick van Rein <rick@openfortress.nl>


rm -rf /usr/share/locale
