#!/bin/ash
#
# This is a FIX_BUILDDIR script for fixing caldavd.
#
# From: Rick van Rein <rick@openfortress.nl>


#NOTE# caldavd has been archived


cp -r /rootfs/etc/caldavd/resources.xml /vardir/etc/caldavd

chown 2000:2000 /vardir/*/caldavd /tlsdir/priv/sched0.*.pem 

