#!/bin/ash
#
# This is a FIX_BUILDDIR script for fixing Postgres underneath scheduling software.
#
# From: Rick van Rein <rick@openfortress.nl>

#rcS_initdb# cat > /vardir/lib/postgres/postgresql.conf <<EOF
#rcS_initdb# # This is literally taken from https://www.postgresql.org/docs/10/config-setting.html
#rcS_initdb# log_connections = yes
#rcS_initdb# log_destination = 'syslog'
#rcS_initdb# search_path = '"$user", public'
#rcS_initdb# shared_buffers = 128MB
#rcS_initdb# EOF

chown 2001:2001 /vardir/*/postgres
chmod 750 /vardir/lib/postgres

