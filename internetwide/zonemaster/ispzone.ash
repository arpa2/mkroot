#!/bin/ash
#
# Rename IWO_DOMAIN_ISP.zone to ${IWO_DOMAIN_ISP}.zone

if [ -r /var/lib/knot/zones/IWO_DOMAIN_ISP.zone ]
then
	mv /var/lib/knot/zones/IWO_DOMAIN_ISP.zone /var/lib/knot/zones/${IWO_DOMAIN_ISP:-example.com}.zone
fi

