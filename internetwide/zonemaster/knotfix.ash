#!/bin/ash
#
# FIX_BUILDDIR fix, so be careful
#
# Fix access rights for Knot DNS
#
# From: Rick van Rein <rick@openfortress.nl>


UID_knot=2000
GID_root=0

mkdir -p /vardir/run/knot

# chown $UID_knot:$GID_root /rootfs/etc/knot/knot.conf
chown $UID_knot:$GID_root /vardir/run/knot /vardir/zone/lib/knot.master /vardir/name/lib/knot.slave /vardir/zone/lib/knot.master/zones /vardir/name/lib/knot.slave/zones
# chmod ugo+r               /rootfs/etc/knot/knot.conf
