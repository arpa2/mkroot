# InternetWide Architecture Component: Chat.
#
# The constructs a root file system for chat services, based on
# ARPA2 Identity and ARPA2 Groups, along with ARPA2 Access Control.
#
# From: Rick van Rein <rick@openfortress.nl>


add_rootfs(chat)

#TODO# Add compiled chme.c
#
rootfs_ospackages(
	busybox
	# findutils
	libc-bin
	# netbase
	# libicu67
	# libdb5.3
	# libssl1.1
	# ssl-cert
	#
	# Kerberos5 for server keys
	#l8r# krb5-user
	#
	# SASL for mechanisms anywhere
	# sasl2-bin libsasl2-2 libsasl2-modules
	# libsasl2-modules-gssapi-mit libsasl2-modules-otp libsasl2-modules-db
	#
	# Prosody for XMPP
	prosody prosody-modules
	lua-sec
	#
	# InsIRDd for IRC
	inspircd
	libgnutls30
	#
	# Matrix Synapse (tends to adapt slowly to new Debian stable)
	#CONTRIB4NOW# matrix-synapse
	#
	# Chat and Audio with Mumble
	#AGED_YET_NICE# mumble-server
	#
	# Control Tower access to the ARPA2 Rules DB
	dropbear libtomcrypt1 libtommath1
	#
	# Problem resolution support
	dnsutils unbound-host
	#
	# View SIP calls
	sngrep
	#
	# Debug support
	DEBUG_SUPPORT strace gdb
)

rootfs_packages(
	mkrootutils
	arpa2cm
	#
	# ARPA2 Identity, Rules DB, Communication Access
	arpa2common
	#
	# Quick MEM pools are used for connection storage
	# quickmem
	#
	# Quick DER is used to encode the DiaSASL protocol
	# quickder
	#
	# Quick DiaSASL is used as the C API to DiaSASL
	# quicksasl
	#
	# Asterisk PBX is used as domain-serving SIP server
	asterisk
	#
	# Galene is a straightforward videoconf application
	galene VERSION=master
	#
	# Test automation [also useful on a live system]
	pavlov
	#
	#TODO# arpa2shell	VERSION=1.2.3
	# twin		VERSION=v0.8.1
	#
	# TLS Pool protects connections in another process
	#NON-ROOT-TODO# Cannot locate pulleybacksimu_tlspool.so
	# tlspool
)

rootfs_user (root GROUP wheel
	UID 0 GID 0
	HOME / SHELL /bin/ash
	NOTE "Container Administrator")

rootfs_user (prosody GROUP prosody
	UID 2000 GID 2000
	HOME /var/lib/prosody
	SHELL /usr/sbin/nologin
	NOTE "Prosody XMPP server")

rootfs_user (inspircd GROUP inspircd
	UID 2001 GID 2001
	HOME /var/lib/inspircd
	SHELL /usr/sbin/nologin
	NOTE "InspIRCd IRC server")

rootfs_user (asterisk GROUP asterisk
	UID 2002 GID 2002
	HOME /var/lib/asterisk
	SHELL /usr/sbin/nologin
	NOTE "Asterisk PBX server")

#AGED_YET_NICE# rootfs_user (murmur GROUP murmur
#AGED_YET_NICE# 	UID 2002 GID 2002
#AGED_YET_NICE# 	HOME /var/lib/mumble-server
#AGED_YET_NICE# 	SHELL /usr/sbin/nologin
#AGED_YET_NICE# 	NOTE "Murmur or the Mumble VoIP server")

rootfs_user (galene GROUP galene
	UID 2003 GID 2003
	HOME /var/lib/galene
	SHELL /usr/sbin/nologin
	NOTE "Galene is a video conferencing tool")

# rootfs_user (postfix GROUP postfix
# 	UID 2000 GID 2000
# 	NOTE "Postfix MTA")
# rootfs_user (postdrop GROUP postdrop
# 	UID 2001 GID 2001
# 	NOTE "Postfix Mail Drop")
# rootfs_user (postgrey GROUP postgrey
# 	UID 2002 GID 2002
# 	NOTE "Postfix grey-listing policy daemon")
# rootfs_user (opendkim GROUP opendkim
# 	UID 2003 GID 2003
# 	NOTE "Postfix grey-listing policy daemon")

# rootfs_user (postmaster GROUP users
# 	UID 3001 GID 3000
# 	NOTE "Postmaster has a POP3 mailbox on this server")
# rootfs_user (netadmin GROUP users
# 	UID 3002 GID 3000
# 	NOTE "Network Administration has a POP3 mailbox on this server")

rootfs_user (nobody  GROUP nobody
	UID 65534 GID 65534
	HOME /nonexistent SHELL /usr/sbin/nologin
	NOTE "Ultimately Powerless User")

rootfs_secret (diepass
	SEED d7e4d3a6c6205d5813413336ba2ff164a5b1939cb4b8dd1301a01f3e87ebbe4c
	HEX BITS 128)

rootfs_secret (restartpass
	SEED a45894a41c1f71ae4a9fe24b97d3d29e69de2a16d3e78273057042ffc201fdf9
	HEX BITS 128)

rootfs_secret (operator
	SEED 1d691f3e4a360a2c46b8a2c3807b00faabc96158a8f7436fbc51017af50304da
	HEX BITS 256)

# Determine the @TURN_MODE@ parameter for /etc/inittab
# Use TCP port forwarding, but only if it uses port 1194
if (NAT_PORTFWD_internetwide_chat_TCP_1194 MATCHES "^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+):1194$")
	set (TURN_MODE "${CMAKE_MATCH_1}:1194")
else()
	set (TURN_MODE ":1194")
endif()

rootfs_directories(
	/etc
	/dev
	/bin /sbin /usr/bin /usr/sbin
	/lib /usr/lib
	/tmp
	/proc /sys
	/var/log
	/var/lib
	/etc/inspircd/virtual
	/usr/lib/inspircd/logs
)

# We use the ${IWO_dmz_dmz_if_MAC} variable as Asterisk entityid
derive_linkaddrs (dmz)
rootfs_files(
	# /etc/passwd
	# /etc/shadow
	# /etc/group
	/etc/nsswitch.conf
	/etc/ld.so.conf
	/etc/ld.so.conf.d/*
	README.MD
	etc/inittab
	etc/rcS
	etc/resolv.conf
	etc/hosts
	etc/dropbear/cmd_a2rule.ash
	etc/prosody/prosody.cfg.lua.actual
	etc/prosody/conf.d/IWO_DOMAIN_ISP.cfg.lua
	etc/inspircd/inspircd.conf.actual
	#AGED_YET_NICE# etc/murmur.ini
	opt/converse/index.html
	usr/bin/env
	etc/asterisk/asterisk.conf
	etc/asterisk/modules.conf
	etc/asterisk/logger.conf
	etc/asterisk/stasis.conf
	etc/asterisk/indications.conf.sample
	etc/asterisk/pjsip.conf.sample
	etc/asterisk/pjsip_notify.conf
	etc/asterisk/extensions.conf.sample
	etc/asterisk/voicemail.conf.sample
	etc/asterisk/confbridge.conf
	etc/asterisk/queues.conf.sample
	etc/asterisk/musiconhold.conf
)

rootfs_vardir (
	USER inspircd GROUP inspircd
	DIRECTORIES lib/inspircd run/inspircd)
rootfs_logdir (
	USER inspircd GROUP inspircd
	inspircd)

rootfs_vardir (
	USER prosody GROUP prosody
	DIRECTORIES lib/prosody run/prosody)

rootfs_vardir (
	USER asterisk GROUP asterisk
	DIRECTORIES lib/asterisk run/asterisk)
rootfs_vardir (
	FILES lib/asterisk/*)
rootfs_vardir (
	USER root GROUP wheel
	DIRECTORIES lib/asterisk/conf)
rootfs_vardir (
	USER galene GROUP galene
	DIRECTORIES lib/galene/data lib/galene/groups)

rootfs_certificate (PEM
	SERVER ${IWO_DOMAIN_ISP}
	USER prosody)

rootfs_certificate (PEM
	SERVER conference.${IWO_DOMAIN_ISP}
	USER prosody)

#NO_ACME# rootfs_certificate (PEM
#NO_ACME# 	SERVER localhost
#NO_ACME# 	USER prosody)

rootfs_certificate (PEM
	SERVER chat${IWO_SITEINST_ME}.${IWO_DOMAIN_ISP}
	USER inspircd)

#AGED_YET_NICE# rootfs_certificate (PEM
#AGED_YET_NICE# 	SERVER ${IWO_DOMAIN_ISP}
#AGED_YET_NICE# 	USER murmur)

#TODO# Temporary: TLS on the server itself (port 8443)
rootfs_certificate (PEM
	SERVER ${IWO_DOMAIN_ISP}
	USER galene)

rootfs_dnsrecord ("chat${IWO_SITEINST_ME}" "AAAA" "${IWO_dmz_PREFIX96}:a2:194")
rootfs_dnsrecord ("chat${IWO_SITEINST_ME}" "A" PORTFWD "${IWO_HOST_chat${IWO_SITEINST_ME}_TCP_5222}")
rootfs_dnsrecord ("_xmpp-client._tcp" "SRV" 10 10 5222 "chat${IWO_SITEINST_ME}")
rootfs_dnsrecord ("_xmpp-server._tcp" "SRV" 10 10 5269 "chat${IWO_SITEINST_ME}")
#Galene services:
rootfs_dnsrecord ("_turn._tcp" "SRV" 10 10 1194 "chat${IWO_SITEINST_ME}")
rootfs_dnsrecord ("_turn._udp" "SRV" 10 10 1194 "chat${IWO_SITEINST_ME}")
#AGED_YET_NICE# rootfs_dnsrecord ("_mumble._tcp" "SRV" 10 10 64738 "chat${IWO_SITEINST_ME}")

# Port 5222  for XMPP clients
# Port 5269  for XMPP server crossover
# Port  194  is IRC without TLS
# Port 6697  is IRC over TLS
# Port 5280  is BOSH, so HTTP, but declared as a webite plugin
# Port 5060  is SIP over UDP/TCP
# Port 5061  is SIP over TLS
# Port 8443  is the direct HTTPS interface to galene (TODO)
#IP6ONLY# Port 1194  is the TURN interface to galene
#IP6ONLY# Port 1194x is for TURN as ephemeral port UDP range
#AGED_YET_NICE# Port 64738 is Mumble over TCP and UDP
rootfs_ports (chat
	IFACE_IDENT
		::a2:194
	TCP_IN
		5222 5269
		194 6667
		5060 5061
		8443 1194
		#AGED_YET_NICE# 64738
	UDP
		5060
		1194 # 11940 11941 11942 11943 11944 11945 11946 11947 11947 11949
		#AGED_YET_NICE#64738
	TCP_CLIENT
	UDP_CLIENT
)

# The BOSH service as built into Prosody
# https://prosody.im/doc/setting_up_bosh
rootfs_proxyweb ("/internetwide/chat/xmpp-bosh/"
	HTTP
	SERVER "${IWO_bck_PREFIX96}:a2:194"
	PORT 5280
	PATH "/internetwide/chat/http-bind/")

# The WebSocket service as built into Prosody
# https://prosody.im/doc/websocket
rootfs_proxyweb ("/internetwide/chat/xmpp-websocket"
	WEBSOCKET
	SERVER "${IWO_bck_PREFIX96}:a2:194"
	PORT 5280
	PATH "/internetwide/chat/xmpp-websocket/")

# The ConverseJS directory will hold the user interface
# https://conversejs.org/docs/html/quickstart.html#getting-a-demo-up-and-running
rootfs_proxyweb ("/internetwide/chat/converse/"
	DIRSHARE
	PATH "converse"
	PATHVAR IWO_chat_CONVERSE_DIR)

# The Galene video conferencing tool has a full HTTP interface
# https://galene.org/
rootfs_proxyweb ("/internetwide/chat/galene/"
	HTTP
	SERVER "${IWO_bck_PREFIX96}:a2:194"
	PORT 8443
	PATH "/")

# The Galene video conferencing tool also has a WebSocket interface
# https://galene.org/
rootfs_proxyweb ("/internetwide/chat/galene/ws"
	WEBSOCKET
	SERVER "${IWO_bck_PREFIX96}:a2:194"
	PORT 8443
	PATH "/ws")

rootfs_fixups(
	busybox.ash
	stripdev.ash
	ldconfig.ash
	dropbearkey.ash
	actualise.ash
	stripdocs.ash
	# perlpathfix.ash
	# striplocale.ash
	fixasterisk.ash
	fixgaleneurls.ash
	fixgalenedirs.ash
	FIX_BUILDDIR
	fixprosody.ash
	fixinspircd.ash
	#TODO# fixgalenewww.ash
	#AGED_YET_NICE# fixmurmur.ash
)

rootfs_downloads(
	# "https://cdn.conversejs.org/dist/converse.min.js"
	# "https://cdn.conversejs.org/dist/converse.min.css"
	"https://cdn.conversejs.org/7.0.5/dist/converse.min.js"
	"https://cdn.conversejs.org/7.0.5/dist/converse.min.css"
	DESTDIR /opt/converse/
)

use_bridge (DMZ_BRIDGE dmz)
use_bridge (BCK_BRIDGE bck)
use_bridge (CTL_BRIDGE ctl)
use_bridge (SVC_BRIDGE svc)

set (CHAT_NETNS ${IWO_SITENAME}.chat${IWO_SITEINST_ME})
set (CHAT_DMZ ${IWO_SITENAME}.chat${IWO_SITEINST_ME}.dmz${IWO_SITEINST_ME})
set (CHAT_BCK ${IWO_SITENAME}.chat${IWO_SITEINST_ME}.bck${IWO_SITEINST_ME})
set (CHAT_CTL ${IWO_SITENAME}.chat${IWO_SITEINST_ME}.ctl${IWO_SITEINST_ME})
set (CHAT_SVC ${IWO_SITENAME}.chat${IWO_SITEINST_ME}.svc${IWO_SITEINST_ME})

rootfs_runkind(OCI_bundle
	config.json
	net4chat.sh
	# INSTANCES kip kipctl
	# INSTFILES config.json net4kip.sh
)

# rootfs_kernel(
	# ftdi_sio
	# loop
# )

# execute_process (COMMAND ${CMAKE_COMMAND}
		# -E make_directory
			#rootfs_vardir()# "${CMAKE_CURRENT_BINARY_DIR}/vardir"
			# "${CMAKE_CURRENT_BINARY_DIR}/etc/prosody/conf.d"
			#rootfs_vardir()# "${CMAKE_CURRENT_BINARY_DIR}/vardir/run/prosody"
			#rootfs_vardir()# "${CMAKE_CURRENT_BINARY_DIR}/vardir/lib/prosody"
			#UNSURE_WHY# "${CMAKE_CURRENT_BINARY_DIR}/vardir/tls"
			#rootfs_vardir()# "${CMAKE_CURRENT_BINARY_DIR}/vardir/lib/inspircd"
			#rootfs_vardir()# "${CMAKE_CURRENT_BINARY_DIR}/vardir/run/inspircd"
			#AGED_YET_NICE# "${CMAKE_CURRENT_BINARY_DIR}/vardir/lib/murmur"
			#rootfs_vardir()# "${CMAKE_CURRENT_BINARY_DIR}/vardir/lib/asterisk/documentation"
			#rootfs_logdir()# "${CMAKE_BINARY_DIR}/log/chat${IWO_SITEINST_ME}"
			#rootfs_logdir()# "${CMAKE_CURRENT_DIR}/log/chat${IWO_SITEINST_ME}/inspircd"
# )


