#!/bin/ash
#
# BUILDDIR script to change access to the vardir
#
# From: Rick van Rein <rick@openfortress.nl>


mkdir -p vardir/lib/galene/data/var
chown -R 2003:2003 vardir/lib/galene/data
