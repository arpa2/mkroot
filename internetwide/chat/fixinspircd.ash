#!/bin/ash
#
# Fixup for inspIRCd -- make it log (a bit) more properly
#
# This is a FIX_BUILDDIR script, so be careful about the relative
# position of the file system inside the host file system!
#
# From: Rick van Rein <rick@openfortress.nl>


# touch           /vardir/log/inspircd/inspircd.log
# chown 2001:2001 /vardir/log/inspircd/inspircd.log /vardir/run/inspircd /vardir/lib/inspircd


# if [ ! -h usr/lib/inspircd/logs/startup.log ]
# then
# 	ln -s /var/log/inspircd.log usr/lib/inspircd/logs/startup.log
# fi

