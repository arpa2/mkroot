#!/bin/ash
#
# Fixes for Murmur (the Mumble server)
#
# FIX_BUILDDIR script
#
# From: Rick van Rein <rick@openfortress.nl>

chown -R 2002:2002 /rootfs/etc/murmur.ini /tlsdir/priv/*-murmur.pem /vardir/lib/murmur
