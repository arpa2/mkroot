# ARPA2 Access Control

> *Access Control is important for realtime services, because they are disruptive.*

Realtime communication is great inasfar as it is desirable.
Quiet background services like IRC make you look away very
easily, but those that surface notifications need more
control.

ARPA2 Communication Access classifies communications between
a local and (presumed authenticated) remote party into lists:

  * **Whitelist** for welcomed communication

  * **Greylist** for things that need a bit of checking

  * **Blacklist** for rejected communication

  * **Honeypotted** for communication that is side-tracked
    and occupied so behaviour can be observed or is otherwise
    sent into oblivia.

There is also a number of levels that programs themselves
should decide about in terms of identification:

  * **Nameless** or anonymous communication may include
    parties mindful about their privacy, but they may be
    asked to provide more details to be able to enter
    (and explicitly granted access in spite of their a
    anonymity, in places where the ACL approves).  This may
    be implemented with extension codes, challenges and/or
    signatures that validate those anonymous origins.

  * **Unauthenticated** communication may be allowed only
    where authentication is impossible and false identities
    are not directly problematic.  False communication can
    stand out like a sore and quickly be eliminated, but it
    is a nuisance if such communication sets of active
    notifications, like popups or ringing phones.

    It is assumed that no facilities for protocol-level
    authentication (such as PSTN telco authority, TLS caller
    cerficite, DNSSEC/DANE, or SASL with Realm Crossover)
    are possible.  That means that any attempt at
    authentication is going to involve activity by the
    user.

  * **Authenticated** communication has no reason to hold
    back, although the listing may impose upper limits of
    its own.

The best solution is probably to combine these two into a
matrix that offers options to migrate between the options.
The method of handling can then be related to the point in
the diagram where a communication lands.


## Asterisk Dialing Plan with Access Control

The dialing plan for Asterisk can implement a matrix of
facilities, as follows:

  * **Listing** is spread over contexts.  There is one for
    undecided calls, `[arpa2access]` and another for each of
    the lists, `[arpa2white]`, `[arpa2gray]`, `[arpa2black]`,
    `[arpa2honey]`.

  * **Identification** is annotated as a single-letter prefix
    before the extension dialed, namely `n` for nameless,
    `u` for unauthenticated and `a` for authenticated.

### Dial Plan for `[arpa2access]`

This is where ARPA2 Access Control is tested at the identification
level that is prefixed to the extension with `n`, `u` or `a`.  The
general intention is to jump to one of the contexts for the access
level found.  Those may refine the connection attempt and jump back
here for another round:

  * Extensions may be appended to the dialed number, thus changing
    the called name/number.  The longer numbers given another chance.
    It is possible for callers to already supply the longer number
    during their initial call.  Also, calls may be made with the
    extension in the Caller ID, so return calls may use that too.

  * Signatures may be dialed to grant access.  This needs to be
    aligned with the ARPA2 Signed Identity system.  A point of
    concern is then the difference between base32 and digits,
    which is to be resolved upon entry.  The codes may not be
    friendly for manual entry, but this course of action may ask
    for six-digit access codes.  This would be virtually all
    signature, so 20 bit entropy.  For manual entry, that is
    quite good.

    The domain would be used, except for a replaceable PSTN link.
    The caller number should always be inlucded, so flags are not
    as variable as normal.  Only when an expiration date is set it
    would be prefixed and lead to a longer code, perhaps with an
    asterisk infix.  The expiration date may then be formed as
    DDMM and the year could be the first time this date is hit;
    that would lead to invalid signatures at any later moment
    because the expiration date is also part of the signature.

    The signature would be rewritten to the usual form in an
    ARPA2 Identity, so as a base32 `+XXX+` addition to the userid
    or extension.  Numbers may use codes such as `*` to represent
    these extensions.  ENUM would remove these special symbols;
    it may need to be handled in a way that allows reconstruction
    of any signature, because it really adds value in this sphere.

Note that the use of Access Control may also set the receiving
identity, so a call may be redirected.  This can be helpful for
such uses as redirection of all anonymous calls to an `info@`
address, where it can be whitelisted and possibly dealt with at
a lower priority.  This may be used, for instance, to avoid that
phones ring for anonymous callers when dialed directly.  At the
same time, it avoids passing the caller through an interaction
that makes them do this manually.

In general, the treatment of Access Control used here is somewhat
dependent on the identification level:

**Nameless** calls are handled with one of:

  * Apply ARPA2 Communication Access without userid (only accepting `@caller.domain` rules)
  * Possibly learn a new extension and redirect to it
  * Invoke the appropriate dial plan, repeating the `n` extension prefix

**Unauthenticated** calls are handled with one of:

  * Apply ARPA2 Communication Access with the unauthenticated userid
  * Possibly learn a new extension and redirect to it
  * Invoke the appropriate dial plan, repeating the `u` extension prefix

**Authenticated** calls are handled with one of:

  * Apply ARPA2 Communication Access with the authenticated userid
  * Possibly learn a new extension and redirect to it
  * Invoke the appropriate dial plan, repeating the `a` extension prefix

### Dial Plan for `[arpa2white]`

**Nameless** calls are handled with one of:

  * Offer the entry of a Caller ID, then goto `[arpa2access]` with this unauthenticated number
  * Possibly use a queue for anonymous callers
  * Possibly park a call without ringing a phone
  * Voicemail for an anonymous caller

**Unauthenticated** calls are handled with one of:

  * Possibly use a queue for unauthenticated callers
  * Possibly park a call without ringing a phone
  * Make phones ring, but mark the caller as unauthenticated
  * Voicemail for an unauthenticated caller

**Authenticated** calls are handled with one of:

  * Possibly use a queue for authenticated callers
  * Make phones ring for this authenticated caller
  * Voicemail for an authenticated caller

### Dial Plan for `[arpa2grey]`

Grey listing means that some action must be taken before the
call can be passed.  This action can take on various forms.

**Nameless** calls are handled with one of:

  * *This call is anonymous and greylisted.  Please provide more information.*
  * Enter a Caller ID, then jump to `[arpa2access]` with this unauthenticated number
  * Enter an extension code if none was given, then jump to `[arpa2access]`
  * Enter a signature code if none was given, then jump to `[arpa2access]`
  * When no choice is made, fallback to `[arpa2black]`

**Unauthenticated** calls are handled with one of:

  * *Your caller identity is greylisted.  We would like to confirm it.*
  * Offer callback for online calls, using a longer number that includes a passable extension
  * Enter an extension code if none was given, then jump to `[arpa2access]` with this unauthenticated number
  * Enter a signature code if none was given, then jump to `[arpa2access]`
  * Pass a call to voicemail to request whitelisting after callback for authentication

**Authenticated** calls are handled with one of:

  * *Your call is greylisted.  Please help us route it.*
  * Allow the entry of an extension, then jump to `[arpa2access]` with this authenticated number
  * Enter a signature code if none was given, then jump to `[arpa2access]`
  * Pass a call to voicemail to request whitelisting after callback

### Dial Plan for `[arpa2black]`

Blacklisting is a direct rejection of a call.  This may consist
of a hangup after a short announcement.  A reason may be given
beforehand, by a spoken message.  Note that not all information
can be safely supplied to unauthenticated callers, so some of
the information may be marked usable only for authenticated calls.

Fixed messages therefore come in categories, for arbitrary callers
including nameless ones, for unauthenticated callers from a given
number and for authenticated callers to a given number.  It is also
possible for each of these messages to be specific to the number
called.  The dial plan chooses wisely from these variants.

**Nameless** calls are handled with one of:

  * Play a generic fixed message.
  * Announce imminent termination.
  * Hangup.

**Unauthenticated** calls are handled with one of:

  * Play a fixed message; use a dedicated message if it is marked
    suitable for unauthenticated callers, otherwise look for a
    generic fixed message.
  * Announce imminent termination.
  * Hangup.

**Authenticated** calls are handled with one of:

  * Play a fixed message; this may be the same as the one for
    unauthenticated callers, but it may also be one that will only
    be played to authenticated callers.
  * Announce imminent termination.
  * Hangup.

### Dial Plan for `[arpa2honey]`

Honeypotting really is an end station.  It is deliberately keeping
a caller busy, so we will devise a script that sounds like a queue
and play elevator music to the caller.  They are queued at a low
priority and will hear a number of calls handled before them that
goes down *and up* over time.

The entire handling is fully robotic, without any claim of hope
to be connected to a human (we do want to remain honest).
After a certain time, the attempted call will be terminated.
Some statistics may be gathered about how long people hang around,
in time and in number of rounds.

Do not use this option lightly.  It is intended to push back on the
aggrevation that agressive marketing techniques impose by interrupting
you, but it is not proper conduct with anyone you actually know.
Prefer black listing for any such cases.


