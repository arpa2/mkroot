#!/bin/ash
#
# Fix fixed paths in Galene (which is wrapped inside a subdir)
#
# From: Rick van Rein <rick@openfortress.nl>


find /usr/local/share/galene/static -type f | \
while read STATFILE
do
	case "$STATFILE" in
	*.html)
		#OLD# sed -i -e 's+\(href\|src\)="/+\1="/internetwide/chat/galene/+g' "$STATFILE"
		sed -i -e 's+<base href="[^"]*">+<base href="/internetwide/chat/galene">+' \
		       -e 's+<base href="\([^"]*[^/"]\)">+<base href="\1/">+' \
			"$STATFILE"
		;;
	*.css)
		;;
	*.js)
		#OLD# BACKQUOTE=$(printf 'x27')
		#OLD# sed -i -e 's+\(['$BACKQUOTE'}]\)/\([a-z][a-z]\)+\1/internetwide/chat/galene/\2+g' "$STATFILE"
		;;
	*.woff2|*.ttf)
		;;
	*.json|*/LICENSE)
		;;
	*)
		echo "Unexpected filename extension in $STATFILE"
		exit 1
		;;
	esac
done
