#!/bin/ash
#
# Fix Asterisk, by linking .conf.sample files to /var/lib/asterisk/conf
#
# From: Rick van Rein <rick@openfortress.nl>

for SMP in $(ls etc/asterisk/*.sample)
do
	SMP=$(basename "$SMP")
	BAS="${SMP%.sample}"
	[ -L "etc/asterisk/$BAS" ] || ln -s "/var/lib/asterisk/conf/$BAS" "etc/asterisk/$BAS"
done

