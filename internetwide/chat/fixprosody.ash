#!/bin/ash
#
# Fixes for Prosody
#
# FIX_BUILDDIR script
#
# From: Rick van Rein <rick@openfortress.nl>

chown -R 2000:2000 /rootfs/etc/prosody/* /tlsdir/priv/*-prosody.pem /vardir/run/prosody /vardir/lib/prosody
chown 2001:2001 /tlsdir/priv/irc*-prosody.pem
chmod o+r /rootfs/etc/prosody/prosody.cfg.lua

