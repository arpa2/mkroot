#!/bin/ash
#
# Iterate over IPv4 addresses, map them onto ports
# and setup service in /etc/inittab
#
# From: Rick van Rein <rick@openfortress.nl>


IP6PREFIX=$(cat /etc/arpa2/prefix.ip6)

cat /etc/arpa2/6bed4.ip4 | \
	while read IP4
	do
		XPORT=$(echo "$IP4" | sed -E 's/^.*[.]([0-9]+)$/\1/')
		XPORT=64$(($XPORT % 100))
		echo >> /etc/inittab "::respawn:/usr/local/sbin/6bed4router -D 6bed4-$XPORT -X 10.64.64.3 -x $XPORT -l $IP4 -r $IP4 -L fc64:bed::/32 -R $IP6PREFIX/96"
	done

