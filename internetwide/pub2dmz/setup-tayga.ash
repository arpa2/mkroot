#!/bin/ash
#
# Iterate over IPv4 addresses, mapping them with Tayga
# and setup service in /etc/inittab
#
# From: Rick van Rein <rick@openfortress.nl>


# echo IP6PREFIX=@IP6PREFIX@

IP6ADDR=$(printf fd%02x:%x%02x:%x%02x:%x%02x::10.64.64.64 $((RANDOM%256)) $((RANDOM%256)) $((RANDOM%256)) $((RANDOM%256)) $((RANDOM%256)) $((RANDOM%256)) $((RANDOM%256)))

sed -i \
	-e "s/^ipv4-addr .*\$/ipv4-addr 10.64.64.64/" \
	-e "s/^#ipv6-addr .*\$/ipv6-addr $IP6ADDR/" \
	-e "s/^prefix .*\$/prefix 64:ff9b::\/96/" \
	-e "/^dynamic-pool /d" \
	-e "/^data-dir /d" \
	/etc/tayga.conf

#TODO#NEEDS_DEV_NET_TUN# /usr/sbin/tayga --mktun

