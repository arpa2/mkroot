#!/bin/ash

if [ ! -c /dev/net/tun ]
then
	mkdir -p /dev/net

	if [ -n "$FAKEROOTKEY" ]
	then
		echo >&2 "Tunnel devices do not persist under fakeroot; please do it manually:"
		echo >&2 "shell# mknod -m 666 /dev/net/tun c 10 200"
		exit 1
	fi

	mknod -m 666 /dev/net/tun c 10 200
	if [ $? -ne 0 ]
	then
		echo >&2 "Insufficient rights to create a tunnel device"
		exit 1
	fi

fi
