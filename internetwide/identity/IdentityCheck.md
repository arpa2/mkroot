# Checking Identity and Access for Websites, Shells, ...

> *Authentication (who are you?) and authorisation (what permissions have you got?)
> are pivotal decisions in any secure and private system.  The `id0` container is
> designed to do such things.*

The output of these decisions can come in many forms, such as web or shell or
whatever, but the general input is:

  - a list of variables that help to evaluate rights (realm, alter ego, service, ...)
  - a list of steps to take to verify identity and/or evaluate rights

Based on this, the Identity Provider outputs:

  - a decision: failure, accepted or state with a further challenge
  - the identity that may be used (if it is not private)
  - the rights assigned (under the desired service)

Not all transports construct a trace of messages, with HTTP as the obvious case of
a session-numb protocol.  For this reason there is an `arpa2_session_id` variable.
which is used, among others, to relate a response to a challenge.  These values
may be provided as unique strings by the service, otherwise they are provided by the
identity service.  They must not be trusted from user input, to avoid iteration attacks.
Plus, they are best tied to a connection, perhaps derived from a TLS master key with
the client and an internal salt in the requesting service.  Checking signatures as
part of an identity also makes a lot of sense.

A few simple syntax rules:

  - Actions and variables follow a simple `[a-z0-9_]+` grammar;
  - Variables can hold binary values (or UTF-8) and may need encoded transport.


## A path of commands

An instruction is processed as a sequence of words.  Unknown words lead to rejection.
Do not assume case-insensitivity, this is a maximally abbresive environment!

Commands may depend on the variables, as per their defintion.  They may define
variations depending on which variables are set and which are not.  Again, assume
nothing in terms of a friendly treatment.


### Command `login`

Perform login, by default using SASL.  Under HTTP this will be done with
[HTTP-SASL](https://datatracker.ietf.org/doc/html/draft-vanrein-diameter-sasl),
a major output from our work on InternetWide Realm Crossover.
By default it will use the local setup from doing so (but not if it was not
setup).

The variable `arpa2_domain` must be set to the realm (which is the same as the
domain name) to which the login is tried.  If a variable `arpa2_sasl_mech` is
set, it will be used to prepare a SASL exchange; in this case, it is also
possible to provide `arpa2_resp` as initial step (which only makes sense when
only a single SASL mechanism is accepted, of course).

SASL challenges set variable `arpa2_chal_proto` to `sasl` and usually provide a
challenge in variable `arpa2_sasl_chal`, and it usually expects an `arpa2_sasl_resp`
variable in return.  There may be an additional `arpa2_chal_prompt` with a short
description of what is required (for user interaction).

When the SASL mechanism `EXTERNAL` is requested, then the `arpa2_external_id`
must be set for success.  SASL takes authorization identities into account
under mechanisms that support them.

A successfully confirmed identity will be internally set in variable
`arpa2_remote_user` and returned from that variable (subject to modifications in
following commands).  When this variable is setup when `login` comes up, then
it will be left as is, and no actions are required.

**See:** `switch`, `otp`, `access`, `here`, `my`, `xover`.

### Command `otp`

This command requests a one-time code of (usually) 6 or 8 digits from (often)
the HOTP or TOTP mechanism.  An optional environment variable 
`arpa2_otp_identity` can be provided from which the identity service can
compute an identifier for the token to use with the given service.

Note that `otp` is not sufficient to authenticate a user; a few digits are only
useful as secondary verification.  (In fact, it seems to be mostly used to get
an authentication mechanism from outside the browser, where JavaScript has gotten
too powerful to rely on existing mechanisms...)  It is assumed that the values
`arpa2_realm` and `arpa2_remote_user` are set before `otp` so the identification
of the OTP token can be made in an individual manner.

OTP challenges set variable `arpa2_chal_proto` to `otp` and expects the response
in `arpa2_otp_resp`.  If that variable was already provided, then that value will
instead be consumed and the challenge/response interaction avoided.  There may
be an additional `arpa2_chal_prompt` with a short description of what is required
(for user interaction).

**See:** `login`.

### Command `switch`

When another identity is defined in `arpa2_alter_ego`, try to switch to that
identity.  This permits the user to choose a more private identity without
ever returning the `login` identity to the requesting service.  It can be
used to function under a pseudonym or as a group member for reasons of privacy
or for reasons of scoping the data being accessed.

This command can fail if the requested identity is not within reach of the
authenticated user.  The complete sequence will then fail.  This will not
happen when the `arpa2_alterego` is not provided, or if it is an empty
string; this should lower its bar of entry.  Furthermore, switching to the
same identity as the one for which login was successful will also not fail;
this should take away potential problems of not knowing what to put there.

The normal order for this command is after `login`, though it may also work on
an externally supplied `arpa2_remote_user`.  Its effect, when successful, is
to change the value in `arpa2_remote_user` to the new value.  This is often
useful for subsequent processing in the `access` command.

**See:** `login`, `access`.

### Command `access`

Determine what Access Control to apply to the given identity.  This may be done
as a step after `login`, or otherwise a variable `arpa2_remote_user` must be
provided as a substitute for a previously established identity that the client
trusts.

It is a good idea to enable the user to `switch` to another identity if they
know it and have set it, for instance because they want to view a website from
the perspective of a group member, or under a pseudonym.

The output of Access Control is generally a set of flags, written as a percent
sign followed by uppercase letters, for instance `%RWD` usually represents the
rights to read, write and delete, though in general the service decides on such
interpretations.  This string is output via a variable `arpa2_access_rights`.

**See:** `alter`, `login`.

### Commands `anon`, `pseudo`, `guest` and `visitor`

These commands are usually placed after the `login` and `access` commands,
if any.  They are all different, but their functions overlap.

The `anon` command drops the value in the `arpa2_remote_user` variable so
that it will not be delivered to the requesting party.  This provides
anonymity, usually after `login` and perhaps `access` have done their work.

The `pseudo` command replaces the value in the `arpa2_remote_user` variable
with a pseudonym that grants an identity that cannot be traced to the user's
real identity, but it will be the same on future encounters.  As input to
this scrambling process the service must provide an `arpa2_userid_salt`
value that will be hashed with the user identity and a secret that the service
does not need to know.

The `guest` command is normally used without prior `login` and `access`.
It places a temporarily usable identity in `arpa2_remote_user` and delivers
`arpa2_identity_timeout` alongside it.  The identity follows a pattern
`+guest+XXX@domain.name` and is likely to be granted low rights in the
Access Control rules, with default `%V` for visitor rights only.

The `visitor` command may be placed anywhere in the sequence, but it is most
useful before the `login` command.  It modifies a failure by resorting to a
`guest` identity instead.  This provides users with a fallback option, even
if just a `+visitor+XXX@domain.name` identity.

**See:** `login`, `access`.

### Commands `domain`, `local`, `internetwide`

These set the scope for upcoming commands, replacing any preceding command
from the set:

  - `domain` restricts further commands to the service domain set in
    `arpa2_domain`, although identities will still be of the
    `user@domain.name` form;
  - `local` restricts operations to the domains locally setup on the
    server, which usually (but not necessarily) includes `arpa2_domain`;
  - `internetwide` is the default, and it will use options installed to
    reach out to other domains on the Internet to establish identity.

Note that it makes sense that domain identities are established by the
domain under which the users reside.  The only thing required to do this
securely is to reach out to such a domain and be sure that an identity is
assured by the right server, and in response to the interaction with the
client talking to our services.

As for Access Control, this will not usually be decided elsewhere, but it
might be done; things like Communication Access are very much part of the
integral settings that a user wants to apply everywhere, and other things
may also be more conveniently configured from the user's own cockpit in
his domain.

**See:** `login`.

### Commands `sasl`, [future]

These set the `login` method to be used, replacing any preceding command
from the set:

  - `sasl` selectes SASL authentication, including challenge/response
    cycles.

**See:** `login`.

### Command `one`

This flag avoids any interactive behaviour, such as challenge/response
statements.  Note that this may lead to failure because no suitable
mechanisms can be found and `login` is nonetheless requested.

### Commands `success`, `failure`

These commands give trivial responses that are either positive or
negative.  They double to terminate a sequence, and may therefore be
used in an interactive flow, to stop processing and cleanup resources
on the identity service (which is nicer than waiting for a timeout
and risking a denial-of-service attack if many such requests stay
open for too long).


## Example: Web backend

The web can use a FastCGI backend that processes the inputs and produces the outputs.

  * The command sequence forms a path, which is evaluated from the start to the end;
  * The variables are setup in FastCGI variables;
  * Output data is sent in `X-ARPA2-*` headers, logically following the `arpa2_*`
    variable names, replacing `_` with `-` and mapping to uppercase any first letter
    after `-` and in the first position after `ARPA2-`;
  * Header data may be binary and is transmitted with HTTP-style `%` encoding.

An example path of commands that could be submitted over FastCGI is

```
arpa2_domain=example.com

/login/switch/access/anon  -->  200

arpa2_remote_user=john@example.com
arpa2_access_rights=%RW
```

This could be used to check on Access Control without revealing user identity.

Another option would be to perform an OTP check to confirm a special action,
usually after a `login` step that will be skipped when `arpa2_remote_user`
is meaningfully set,

```
arpa2_domain=example.com
arpa2_remote_user=john@example.com

/login/otp  -->  200

arpa2_access_rights=%RWD
```

As a last example, to synchornise a HOTP token it is often permissible to
search forward for a fair amount when two subsequent codes are provided,

```
arpa2_domain=example.com
arpa2_remote_user=john@example.com

/login/otp/otp  -->  200
```
