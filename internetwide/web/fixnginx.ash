#!/bin/ash
#
# Fix installation of Nginx
#
# From: Rick van Rein <rick@openfortress.nl>


chown 2002:2002 /var/lib/nginx
