#!/bin/ash
#
# rules.ash -- Setup the Rules DB under vardir
#
# CAREFUL, this script runs above the rootfs so it can reach the vardir.
#
# TODO: This script will probably be replaced with actual sharing
#       of the entire database later on, so it is DEPRECATED from
#       the onset (as well as current practice).
#
# From: Rick van Rein <rick@openfortress.nl>



touch      vardir/lib/arpa2/rules/data.mdb
touch      vardir/lib/arpa2/rules/lock.mdb
chmod 0444 vardir/lib/arpa2/rules/data.mdb
chmod 0666 vardir/lib/arpa2/rules/lock.mdb
