#!/bin/ash
#
# This script runs under FIX_BUILDDIR so be careful when changing it.
#
# Your root directory is metal and you can cut yourself seriously.
#
# From: Rick van Rein <rick@openfortress.nl>


#MKHERE# drwx------ 2 uuidd root 2 Jun 11 09:49 active
#MKHERE# drwx------ 2 uuidd root 2 Jun 11 09:49 bounce
#MKHERE# drwx------ 2 uuidd root 2 Jun 11 09:49 corrupt
#MKHERE# drwx------ 2 uuidd root 2 Jun 11 09:49 defer
#MKHERE# drwx------ 2 uuidd root 2 Jun 11 09:49 deferred
#MKHERE# drwxr-xr-x 2 root  root 2 Jun 30  2020 dev
#MKHERE# drwxr-xr-x 2 root  root 2 Jun 30  2020 etc
#MKHERE# drwx------ 2 uuidd root 2 Jun 11 09:49 flush
#MKHERE# drwx------ 2 uuidd root 2 Jun 11 09:49 incoming
#MKHERE# drwxr-xr-x 2 root  root 2 Jun 30  2020 lib
#MKHERE# drwx-wx--T 2 uuidd kube 2 Jun 11 09:49 maildrop
#MKHERE# drwxr-xr-x 2 root  root 2 Jun 11 09:49 pid
#MKHERE# drwx------ 2 uuidd root 2 Jun 11 09:49 private
#MKHERE# drwx--s--- 2 uuidd kube 2 Jun 11 09:49 public
#MKHERE# drwx------ 2 uuidd root 2 Jun 11 09:49 saved
#MKHERE# drwxr-xr-x 3 root  root 3 Jun 11 09:49 usr

#OLD# for SPOOLDIR in active bounce corrupt defer deferred flush hold incoming maildrop private public
#OLD# do
#OLD# 	mkdir -m 0750 -p "var/spool/postfix/$SPOOLDIR"
#OLD# 	chown postfix    "var/spool/postfix/$SPOOLDIR"
#OLD# done
#OLD# 
#OLD# for POSTDROP in maildrop public
#OLD# do
#OLD# 	chgrp postdrop   "var/spool/postfix/$POSTDROP"
#OLD# done
#OLD# 
#OLD# for CONFILE in dynamicmaps.cf   postfix-files   makedefs.out post-install postfix-script main.cf.proto master.cf.proto
#OLD# do
#OLD# 	touch    "etc/postfix/$CONFILE"
#OLD# done
#OLD# 
#OLD# for CONFDIR in dynamicmaps.cf.d postfix-files.d
#OLD# do
#OLD# 	mkdir -p "etc/postfix/$CONFDIR"
#OLD# done
#OLD# 
#OLD# chgrp postdrop /usr/sbin/postdrop /usr/sbin/postqueue
#OLD# chmod g+s      /usr/sbin/postdrop /usr/sbin/postqueue


#UNUSED# # Create dev/urandom but cleanup afterwards if our root status is fake
#UNUSED# #
#UNUSED# CLEANUP=""
#UNUSED# if [ ! -c dev/urandom ]
#UNUSED# then
#UNUSED#	rm -f dev/urandom
#UNUSED# 	mknod dev/urandom c 1 9
#UNUSED# 	[ $? -ne 0 ] && exit 1
#UNUSED# 	echo Created dev/urandom relative to $(pwd):
#UNUSED# 	stat dev/urandom
#UNUSED# 	if [ -n "$FAKEROOTKEY" ]
#UNUSED# 	then
#UNUSED# 		CLEANUP="dev/urandom"
#UNUSED# 		echo It will be cleaned up because it does not persist outside fakeroot
#UNUSED# 	fi
#UNUSED# fi


# The following is based on the installer script of the Postfix package in Debian:
#
# Debian Postfix postinst
# LaMont Jones <lamont@debian.org>
# Based on debconf code by Colin Walters <walters@cis.ohio-state.edu>,
# and John Goerzen <jgoerzen@progenylinux.com>.
#
# The script below is simplified because it starts from a less varied environment.


# Users and Groups as introduced in CMakeLists.txt
#
UID_root=0
GID_wheel=0
UID_postfix=2000
GID_postfix=2000
UID_postdrop=2001
GID_postdrop=2001
UID_postgrey=2002
GID_postgrey=2002
UID_nobody=65534
GID_nobody=65534

# All files are relative to the current/root directory;
# Variable directories are in ../vardir -- so we need NO_CHROOT
#
CHROOT=/vardir/spool/postfix
VARLIB=/vardir/lib/postfix
SBINDIR=/rootfs/usr/sbin
CFGDIR=/rootfs/etc/postfix

# Paths of directories and commands -- under NO_CHROOT
#
MAILDROP=${CHROOT}/maildrop
PUBQUEUE=${CHROOT}/public
PRIVATE=${CHROOT}/private
PIDDIR=${CHROOT}/pid
LIBDIR=${CHROOT}/lib
DEVDIR=${CHROOT}/dev
ETCDIR=${CHROOT}/etc
USRDIR=${CHROOT}/usr
VARDIR=${CHROOT}/var
POSTDROP=${SBINDIR}/postdrop
POSTQUEUE=${SBINDIR}/postqueue


# Extract from the Debian postinstall script -- mindful of NO_CHROOT
#
mkdir -p "$MAILDROP" "$PUBQUEUE" "$PRIVATE" "$PIDDIR" "$LIBDIR" "$DEVDIR" "$ETCDIR" "$USRDIR" "$VARDIR"
chown    $UID_root:$GID_postdrop "$POSTDROP" "$POSTQUEUE"
chown $UID_postfix:$GID_postdrop "$MAILDROP" "$PUBQUEUE"
chown $UID_postfix:$GID_wheel    "$CHROOT"   "$PRIVATE" #earlier setting of "$PUBQUEUE"
chown    $UID_root:$GID_wheel    "$PIDDIR"   "$CHROOT"  #earlier tried otherwise...
chown $UID_postfix:$GID_postfix  "$VARLIB"
chmod 02555 "$POSTDROP" "$POSTQUEUE"
chmod 02710 "$PUBQUEUE"
chmod  1730 "$MAILDROP"
chmod   700 "$PRIVATE"
chmod   755 "$PIDDIR" "$LIBDIR" "$DEVDIR" "$ETCDIR" "$USRDIR" #earlier setting of "$PUBQUEUE"


# Set a number of more general directories -- mindful of NO_CHROOT
#
for DIR in active bounce corrupt defer deferred flush hold incoming saved trace
do
	mkdir -p                      "${CHROOT}/${DIR}"
	chown $UID_postfix:$GID_wheel "${CHROOT}/${DIR}"
	chmod 700                     "${CHROOT}/${DIR}"
done


# Change the configuration -- mindful of NO_CHROOT
#
postconf() {
	/rootfs/usr/sbin/postconf -d "$CFGDIR" -e "$@";
}


# And stil we get trouble...
#
#SIGH# postfix/postfix-script: warning: not owned by group postdrop: /usr/sbin/postqueue
#SIGH# postfix/postfix-script: warning: not owned by group postdrop: /usr/sbin/postdrop
#SIGH# postfix/postfix-script: warning: not owned by group postdrop: /var/spool/postfix/public
#SIGH# postfix/postfix-script: warning: not owned by group postdrop: /var/spool/postfix/maildrop


#UNUSED# # Cleanup things that were only possible through fakeroot
#UNUSED# #
#UNUSED# [ -n "$CLEANUP" ] && rm -f $CLEANUP


# Create a local Postmaster mailbox (or leave it and just reassure its access rights)
#
mkdir -p                      /vardir/mail
chown $UID_postfix:$GID_wheel /vardir/mail
touch                         /vardir/mail/postmaster
chown $UID_root:$GID_wheel    /vardir/mail/postmaster
chmod go-rwx                  /vardir/mail/postmaster


# Create a directory for Postgrey
#
mkdir -p                          /vardir/lib/postgrey
chown $UID_postgrey:$GID_postgrey /vardir/lib/postgrey


# Assume a state of happy bliss
#
return 0
