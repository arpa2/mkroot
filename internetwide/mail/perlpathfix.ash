#!/bin/ash
#
# Perl has trouble finding information.  We fix it here.
#
#   root@mail0:~# postgrey
#   Can't locate Pod/Usage.pm in @INC (you may need to install the Pod::Usage module)
#   (@INC contains:
#       /etc/perl
#       /usr/local/lib/x86_64-linux-gnu/perl/5.32.1
#       /usr/local/share/perl/5.32.1
#       /usr/lib/x86_64-linux-gnu/perl5/5.32
#       /usr/share/perl5
#       /usr/lib/x86_64-linux-gnu/perl/5.32
#       /usr/share/perl/5.32
#       /usr/local/lib/site_perl
#       /usr/lib/x86_64-linux-gnu/perl-base
#   ) at /usr/sbin/postgrey line 12.
#   BEGIN failed--compilation aborted at /usr/sbin/postgrey line 12.
#
# The fix made here is to move content from unsought directories to ones sought:
#
#   /usr/share/perl/5.32.1/Pod/Usage.pm
#   /usr/lib/x86_64-linux-gnu/perl/5.32.1/Encode.pm
#
# Please correct me if there is a better/Perlier way of doing this properly!
#
# From: Rick van Rein <rick@openfortress.nl>


if [ -d /usr/share/perl/5.32.1 -a ! -r /usr/share/perl/5.32 ]
then
	ln -s 5.32.1 /usr/share/perl/5.32
fi

if [ -d /usr/lib/x86_64-linux-gnu/perl/5.32.1 -a ! -r /usr/lib/x86_64-linux-gnu/perl/5.32 ]
then
	ln -s 5.32.1 /usr/lib/x86_64-linux-gnu/perl/5.32
fi

