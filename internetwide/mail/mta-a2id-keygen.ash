#!bin/ash
#
# mta-a2id-keygen.ash  --  Generate an ARPA2 Identity key for Signed Identities on the MTA
#
# This is a NO_CHROOT script, because it writes to ../vardir
#
# From: Rick van Rein <rick@openfortress.nl>

TGTDIR="/vardir/lib/arpa2/identity/keys"
TGTKEY="$TGTDIR/mta-signed-arpa2-identities"


# Create /dev/urandom but cleanup afterwards if our root status is fake
#
CLEANUP=""
if [ ! -c dev/urandom ]
then
	rm -f dev/urandom
	mknod dev/urandom c 1 9
	[ $? -ne 0 ] && exit 1
	if [ -n "$FAKEROOTKEY" ]
	then
		CLEANUP="dev/urandom"
	fi
fi


# Create the $TGTKEY in $TGTDIR if it does not exist yet
#
if [ ! -r "$TGTKEY" ]
then
	echo Creating "$TGTDIR"
	mkdir -p "$TGTDIR"

	#TODO# The following is needed for /usr/local/lib, even under FIX_BUILDDIR
	LIBS=$(cat /rootfs/etc/ld.so.conf etc/ld.so.conf.d/* | sed -e '/^[^/]/d' -e "s+^+${FAKECHROOT_BASE:+$FAKECHROOT_BASE/}$(pwd)+")
	export LD_LIBRARY_PATH=${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}$(echo $LIBS | sed 's+ +:+g')
	# echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
	/rootfs/usr/local/bin/a2id-keygen "$TGTKEY"
	RETVAL=$?
else
	RETVAL=0
fi


# Cleanup things that were only possible through fakeroot
#
[ -n "$CLEANUP" ] && rm -f $CLEANUP


# Return the result from the key generation command
#
return $RETVAL
