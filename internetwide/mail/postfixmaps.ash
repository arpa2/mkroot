#!/bin/ash
#
# This script runs in the container, so postmap works properly.
#
# From: Rick van Rein <rick@openfortress.nl>


# Map the databases
#
LD_LIBRARY_PATH=/usr/lib/postfix/ /usr/sbin/postmap -i -r -c /etc/postfix aliases


# Assume a state of happy bliss
#
return 0
