#!bin/ash

mkdir -p ../vardir/lib/nginx ../vardir/log/nginx ../vardir/run

if [ -r etc/nginx/nginx.conf.new ]
then
	cp etc/nginx/nginx.conf.new etc/nginx/nginx.conf
fi

