#!/bin/ash

#TODO# This may not be necessary after "nonlocal" has run
export LD_LIBRARY_PATH=rootfs/lib:rootfs/usr/lib:rootfs/usr/local/lib:rootfs/lib/x86_64-linux-gnu:rootfs/usr/lib/x86_64-linux-gnu:rootfs/usr/local/lib/x86_64-linux-gnu

export KIP_VARDIR=/var/lib/arpa2/kip
export KIP_KEYTAB=/var/lib/arpa2/kip/master.keytab

KIP_VARDIR=/vardir/${KIP_VARDIR#/var/}
KIP_KEYTAB=/vardir/${KIP_KEYTAB#/var/}

ENCTYPE=aes256-cts-hmac-sha384-192

mkdir -p $KIP_VARDIR

/rootfs/usr/sbin/a2kip master create enctype $ENCTYPE service kip vardir "$KIP_VARDIR"

for ZONE in ${IWO_DOMAIN_ISP:-example.com} unicorn.demo.arpa2.org
do
	if [ ! -e "${KIP_VARDIR#/}/kip-vhost/$ZONE" ]
	then
		/rootfs/usr/sbin/a2kip virtual add domain "$ZONE" service kip
	fi
done

