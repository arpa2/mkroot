#!bin/ash
#
# Fix the exec rights on the FastCGI program for the ARPA2 LETSystem
#
# From: Rick van Rein <rick@openfortress.nl>


chmod ugo+x usr/local/libexec/arpa2lets/a2lets.fcgi
