#!bin/ash
#
# This is a FIX_BUILDDIR script to setup the ARPA2 LETS database.
# Nothing is done when a database already exists.
#
# CAREFUL, this runs on bare metal, so it can write in th vardir.
#
# From: Rick van Rein <rick@openfortress.nl>


DBVARDIR=vardir/lib/arpa2/lets/
DBVARFILE=$DBVARDIR/ARPA2LETSystem.sqlite3
DBNEWFILE=$DBVARDIR/ARPA2LETSystem.newdb
DBINITSCRIPT=rootfs/usr/local/libexec/arpa2lets/initdb.sql 

SQLITE3=rootfs/usr/bin/sqlite3
A2LETS=rootfs/usr/sbin/a2lets
CHME=rootfs/sbin/chme

# Do not touch the database if it already exists
#
[ -f "$DBVARFILE" ] && exit 0

# Remove any prior new database
#
rm -f "$DBNEWFILE"

# Be sure to have the target directory
#
mkdir -p "$DBVARDIR"

# Create the empty database
#
"$SQLITE3" -init "$DBINITSCRIPT" "$DBNEWFILE" .quit
RETVAL=$?
[ $RETVAL -ne 0 ] && exit $RETVAL

# Prepare the runtime environment for a2lets invocations
#
export ARPA2_LETS_DB="$DBNEWFILE"
export ARPA2_LETS_ADMIN_RULE="%A ~admin@${IWO_DOMAIN_ISP}"
export REMOTE_USER="admin@${IWO_DOMAIN_ISP}"

# Setup the ISP domain for the ARPA2 LETSystem
#
"$A2LETS" domain new domain ${IWO_DOMAIN_ISP}
RETVAL=$?
[ $RETVAL -ne 0 ] && exit $RETVAL

# Setup the public XES currency under the ISP domain
#
"$A2LETS" currency new domain ${IWO_DOMAIN_ISP} currency XES format "XES %d"
RETVAL=$?
[ $RETVAL -ne 0 ] && exit $RETVAL

# Stop others from seeing the SQLite3 databsae
#
chmod o-rwx "$DBNEWFILE"
RETVAL=$?
[ $RETVAL -ne 0 ] && exit $RETVAL

# Turn the SQLite3 database over to the "lets" user
#
chown 2000:2000 "$DBNEWFILE"
RETVAL=$?
[ $RETVAL -ne 0 ] && exit $RETVAL

# Activate the new database
#
mv "$DBNEWFILE" "$DBVARFILE"
RETVAL=$?
[ $RETVAL -ne 0 ] && exit $RETVAL

# Report the output from SQLite3
exit $RETVAL
