# InternetWide Architecture Component: Xchange
#
# The constructs a root file system for an exchange platform,
# covering payment systems, trading and a market with offers
# on other sites.
#
# From: Rick van Rein <rick@openfortress.nl>


add_rootfs(exchange)

rootfs_ospackages(
	busybox # bash
	krb5-user libkrb5-3 libk5crypto3
	libsasl2-2 libsasl2-modules libsasl2-modules-otp libsasl2-modules-db sasl2-bin
	libunbound8 unbound-anchor libunbound-dev dnsutils
	openssl
	libc-bin
	spawn-fcgi libfcgi-dev
	sqlite3
	DEBUG_SUPPORT ltrace strace gdb bash #pinentry-curses
)

rootfs_packages(
	arpa2cm
	arpa2common
	quickmem
	quickder
	quicksasl
	lets
	# freediameter
	# kip
	# arpa2shell	VERSION=1.2.3
	# twin		VERSION=v0.8.1
	#
	# chme, flash, picoget, picoput
	mkrootutils
)

rootfs_user (root GROUP wheel
	UID 0 GID 0
	HOME / SHELL /bin/ash
	NOTE "Container Administrator")

rootfs_user (lets GROUP lets
	UID 2000 GID 2000
	HOME / SHELL /bin/ash
	NOTE "ARPA2 LETSystem user")

rootfs_directories(
	/etc
	/dev
	/bin /sbin /usr/bin /usr/sbin
	/lib /usr/lib
	/tmp
	/proc /sys
	/var/log /var/run		#TODO# vardir
)

rootfs_files(
	# /etc/passwd
	# /etc/shadow
	# /etc/group
	/etc/nsswitch.conf
	/etc/ld.so.conf
	/etc/ld.so.conf.d/*
	README.MD
	etc/inittab
	etc/hosts
	etc/resolv.conf
	# etc/openssl.cnf
	# etc/sasl2/Hosted_Identity.conf
	# etc/loop-unbound-anchor
)

# rootfs_dnsrecord ("_kip._tcp" "SRV" 10 10 9876 "kip${IWO_SITEINST_ME}")
# rootfs_dnsrecord ("kip${IWO_SITEINST_ME}" "AAAA" "${IWO_dmz_PREFIX96}:a2:9876")
# rootfs_dnsrecord ("kip${IWO_SITEINST_ME}" "A" PORTFWD "${IWO_HOST_kip_TCP_9876}")

# The FastCGI service for the ARPA2 LETSystem as a web server plugin
rootfs_proxyweb ("/internetwide/exchange/lets/"
	FASTCGI
	SERVER "${IWO_bck_PREFIX96}:a2:6765"
	PORT 6765
	ARPA2LOGIN "ARPA2 Local Exchange Trading System for ${IWO_DOMAIN_ISP}")

rootfs_fixups(
	busybox.ash
	stripdev.ash
	ldconfig.ash
	fixfcgia2lets.ash
	nonlocal.ash
	FIX_BUILDDIR
	fixdbsetupa2lets.ash
)

use_bridge (DMZ_BRIDGE dmz)
use_bridge (CTL_BRIDGE ctl)
use_bridge (SVC_BRIDGE svc)
use_bridge (BCK_BRIDGE bck)

set (IETS_NETNS ${IWO_SITENAME}.iets${IWO_SITEINST_ME})
set (IETS_DMZ ${IWO_SITENAME}.iets${IWO_SITEINST_ME}.dmz${IWO_SITEINST_ME})
set (IETS_CTL ${IWO_SITENAME}.iets${IWO_SITEINST_ME}.ctl${IWO_SITEINST_ME})
set (IETS_SVC ${IWO_SITENAME}.iets${IWO_SITEINST_ME}.svc${IWO_SITEINST_ME})
set (IETS_BCK ${IWO_SITENAME}.iets${IWO_SITEINST_ME}.bck${IWO_SITEINST_ME})

rootfs_runkind(OCI_bundle
	config.json
	net4iets.sh
	# INSTANCES kip kipctl
	# INSTFILES config.json net4kip.sh
)

execute_process (COMMAND ${CMAKE_COMMAND}
		-E make_directory
			"${CMAKE_CURRENT_BINARY_DIR}/vardir"
			"${CMAKE_CURRENT_BINARY_DIR}/vardir/log"
			# "${CMAKE_CURRENT_BINARY_DIR}/vardir/lib/kip"
			# "${CMAKE_CURRENT_BINARY_DIR}/vardir/lib/unbound"
			"${CMAKE_CURRENT_BINARY_DIR}/tlsdir"
			"${CMAKE_CURRENT_BINARY_DIR}/tlsdir/reqs"
			"${CMAKE_CURRENT_BINARY_DIR}/tlsdir/cert"
			"${CMAKE_CURRENT_BINARY_DIR}/tlsdir/priv"
			"${CMAKE_BINARY_DIR}/log/iets${IWO_SITEINST_ME}"
)

