#!bin/ash
#
# Rights for /var/lib/acme directories
# https://github.com/hlandau/acmetool/blob/master/_doc/SCHEMA.md#permissions-posix
#
# TODO: Or invoke "acmetool conform"
#
# TODO: Errors when ../vardir/lib/acme/xxx/* lists an empty directory
#
# This is a FIX_BUILDDIR script; be careful.
#
# From: Rick van Rein <rick@openfortress.nl>

UID_acmetool=12345
GID_acmetool=12345
UID_root=0
GID_root=0

chown $UID_acmetool:$GID_acmetool /vardir/lib/acme/*
chown     $UID_root:$GID_root     /vardir/lib/acme/conf
chmod    0755 /vardir/lib/acme/*
chmod    0770 /vardir/lib/acme/accounts   /vardir/lib/acme/keys   /vardir/lib/acme/tmp
chmod -R 0660 /vardir/lib/acme/accounts/* /vardir/lib/acme/keys/*

