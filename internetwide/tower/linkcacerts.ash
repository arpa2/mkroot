#!/bin/ash
#
# Make all certificates for Mozilla available in /etc/ssl/certs
#
# From: Rick van Rein <rick@openfortress.nl>


CADIR="/usr/share/ca-certificates/mozilla/"
CPDIR="/etc/ssl/certs"
CPREL="../../.."

mkdir -p "$CPDIR"

for f in $(ls "$CADIR")
do
	if [ ! -r "$CPDIR/$f" ]
	then
		ln -s "$CPREL$CADIR/$f" "$CPDIR/$f"
	fi
done
