#!/bin/ash
#
# tlsall.ash -- Handler script for DN,LCS lifecycle operations
#
# Take a single step on the "tlsall" lifecycle in LDAP objects.
# These steps are perfect when they are:
#  - atomic -- so they do not leave a half-done state hanging
#  - idempotent -- so failure to update the workflow can rerun without pain
#
# The input reads DN,LCS pairs that present new work to do.
# The work is done, and upon success the focus moves to the next step.
#
# Continued failures just cause repeated hints to run the task, so any
# temporary setbacks will be overcome as long as no reports are made.
# Other than that, it is necessary to move forward.
#
# From: Rick van Rein <rick@openfortress.nl>


# Initialise the tlsall handler
#
PROGRAM=pulley/$(basename "$0")
PREFIX=tlsall
logger -s -p daemon.notice "$PROGRAM: Started"


# Get a new request and parse it.
#
# Read a DN,LCS pair of lines.  Never expect empty input.  Skip timers
# and variable assignments.  And detect what comes after all that.
# Handle broken input and several funny formats.  Derive variables.
#
# Return variables, with spaces to simplify later matching
#  DN, LCS   with the original input
#  LCS_STEP  with the steps (no "${PREFIX} " prefix) and an added space
#  LCS_DONE  with the steps already done and a prefixed space
#  LCS_TODO  with the steps to be taken and an added space
#  LCS_NEXT  with the next single step to take (with any @ time stripped)
#  LCS_REST  with the part of $LCS_TODO that follows after $LCS_NEXT
#  LCS_ORIG  with the original input, equals $LCS
#  LCS_CONT  with the continued value for if ${LCS_NEXT} succeeds
#  DN_HOST   with the host name from "dc=..."
#  DN_DOMAIN with the domain name from "accessDomain=..."
#  DN_FQDN   with the host.domain (compensating for dc=@)
#
get_request () {
	while true
	do
		#
		# Read DN, LCS and detect communication anomalies
		read DN
		if [ $? -ne 0 ]
		then
			logger -s -p daemon.notice "$PROGRAM: Graceful exit on empty input"
			exit 0
		fi
		read LCS
		if [ $? -ne 0 ]
		then
			logger -s -p daemon.err "$PROGRAM: Disrupted LCS download, exiting"
			exit 1
		fi
		if [ -z "$DN" -o -z "$LCS" ]
		then
			logger -s -p daemon.err "$PROGRAM: Empty DN/LCS input, exiting"
			exit 1
		fi
		if [ "${LCS#${PREFIX} }" = "$LCS" ]
		then
			logger -s -p daemon.notice "$PROGRAM: Ignoring unexpected LCS: $LCS"
			continue
		fi
		#
		# Parse the LCS line
		LCS_ORIG="${LCS}"
		LCS_STEP=" ${LCS#${PREFIX} } "
		LCS_DONE="${LCS_STEP% . *}"
		LCS_TODO="${LCS_STEP#* . }"
		while
			LCS_NEXT="${LCS_TODO%% *}"
			LCS_NEXT="${LCS_NEXT%@*}"
			LCS_REST="${LCS_TODO#* }"
			[ "${LCS_NEXT%[=?]*}" != "${LCS_NEXT}" ]
		do
echo "LCS_DONE=\"$LCS_DONE\", LCS_TODO=\"$LCS_TODO\", LCS_NEXT=\"$LCS_NEXT\" --->"
			LCS_DONE="${LCS_DONE} ${LCS_NEXT}"
			LCS_TODO="${LCS_TODO#${LCS_NEXT} }"
echo "LCS_DONE=\"$LCS_DONE\", LCS_TODO=\"$LCS_TODO\""
		done
		#TODO# Spaces not quite right in LCS_CONT
		NOW=$(date +%s)
		LCS_CONT="${PREFIX}${LCS_DONE} ${LCS_NEXT}@${NOW} . ${LCS_REST% }"
		LCS_CONT="${LCS_CONT% }"
		# if [ "$LCS_DONE . $LCS_TODO" != "$LCS_STEP" ]
		# then
		# 	logger -s -p daemon.err "$PROGRAM: Ignoring LCS with cursor ambiguity: $LCS"
		# 	continue
		# fi
		if [ -z "$LCS_NEXT" ]
		then
			logger -s -p daemon.notice "$PROGRAM: Ignoring finished LCS: $LCS"
			continue
		fi
		#
		# Parse the DN line
		DN_HOST="${DN#dc=}"
		DN_HOST="${DN_HOST%%,*}"
		DN_DOMAIN="${DN#*,accessDomain=}"
		DN_DOMAIN="${DN_DOMAIN%%,*}"
		if [ "${DN_HOST}" = "@" ]
		then
			DN_FQDN="${DN_DOMAIN}"
		else
			DN_FQDN="${DN_HOST}.${DN_DOMAIN}"
		fi
		logger -s -p daemon.debug "DEBUG: DN_HOST=${DN_HOST}, DN_DOMAIN=${DN_DOMAIN}, DN_FQDN=\"${DN_FQDN}\""
		case "$DN" in
		dc=$DN_HOST,accessDomain=$DN_DOMAIN,ou=IdentityHub,o=arpa2.net,ou=InternetWide)
			echo Got DN
			;;
		*)
			logger -s -p daemon.notice "$PROGRAM: Ignoring unsupported DN: $DN"
			continue
			;;
		esac
		#
		# Parse variables to set before invoking the subscript
		unset LCS_VARS
		for _V in $LCS_DONE
		do
			if [ "${_V%=*}" != "$_V" ]
			then
				LCS_VARS="${LCS_VARS:+$LCS_VARS }LIFECYCLE_${_V}"
			fi
		done
		LCS_VARS="${LCS_VARS:-}"
		#
		# We did not loop back up, so the work is done
		return
	done
}


# Main loop.  Read DN,LCS pairs and call the individual scripts for them.
#
( echo 'dc=rick,accessDomain=vanrein.org,ou=IdentityHub,o=arpa2.net,ou=InternetWide' ; echo "${PREFIX}"' . v=13 q=12 install@10 right up' ) |
while get_request
do
	logger -s -p daemon.debug "$PROGRAM: Got LCS=\"$LCS\" on DN=\"$DN\""
	echo "DONE=\"$LCS_DONE\""
	echo "TODO=\"$LCS_TODO\""
	echo "NEXT=\"$LCS_NEXT\""
	echo "REST=\"$LCS_REST\""
	echo "VARS=\"$LCS_VARS\""
	echo "CONT=\"$LCS_CONT\""
	SCRIPT="/usr/bin/$PREFIX-$LCS_NEXT.ash" 
	if [ -x "$SCRIPT" ]
	then
		echo "Got $SCRIPT"
		echo $DN_FQDN | eval $LCS_VARS $SCRIPT
		RETVAL=$?
		echo "RETVAL=$RETVAL"
		if [ $RETVAL -eq 0 ]
		then
			echo "LDAP modify:"
			echo "dn: $DN"
			echo "changetype: modify"
			echo "delete: lifecycleState"
			echo "lifecycleState: $LCS_ORIG"
			echo "-"
			echo "add: lifecycleState"
			echo "lifecycleState: $LCS_CONT"
			echo "-"
			echo ""
		fi
	else
		echo "Got no $SCRIPT"
	fi
done
