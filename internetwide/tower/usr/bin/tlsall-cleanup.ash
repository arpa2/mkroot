#!/bin/flash /var/run/acmetool.lock
#
# Cleanup the past certificate for a given hostname.
#
# This is called after the TTL on a transition from -live to -past.
# Its aim is to clean up the past certificate for a hostname, to
# allow for future upcycling from -next to -live, which is deferred
# until the -past certificate is gone.  No attempt is made to run
# that process, as it would be independently repeated.
#
# From: Rick van Rein <rick@openfortress.nl>


# [ ...check... ] || die "Out of here"
#
die () {
	logger -s -p ERROR "$*"
	exit 1
}


# Inform about a change
#
notice () {
	logger -p NOTICE "$*"
	echo "$*"
}


# Given hostname, user/group owner and kind in $1, $2 and $3, try
# to upcycle from -next to -live certificate.
#  $1 - WANTing host
#  $2 - HOSTname for which a certificate is released
#  $3 - USER or group labelled as certificate owner (not the source of UID:GID)
#  $4 - KIND of output, "pem" or "der" or...?
#
pastdrop_host () {
	WANT="$1"
	HOST="$2"
	USER="$3"
	KIND="$4"
	echo " - WANT=$WANT"
	echo " - HOST=$HOST"
	echo " - USER=$USER"
	echo " - KIND=$KIND"
	ORIG="/var/lib/acme/live/$HOST/cert"
	PRIV="/var/lib/acme/live/$HOST/privkey"
	WISH="/tlsall/$WANT/want/$HOST-$USER.$KIND"
	NEXT="/tlsall/$WANT/cert/$HOST-next.pem"
	LIVE="/tlsall/$WANT/cert/$HOST-live.pem"
	LIVD="/tlsall/$WANT/cert/$HOST-live.der"
	PAST="/tlsall/$WANT/cert/$HOST-past.pem"
	PRIL="/tlsall/$WANT/priv/$HOST-$USER.pem"
	PRID="/tlsall/$WANT/priv/$HOST-$USER.der"
	PRIW="/tlsall/$WANT/priv/$HOST-$USER.new"
	if [ -r "$PAST" ]
	then
		echo -n "Past cert:   "
		ls -l "$PAST"
		rm "$PAST" || die "Certificate cleanup failed for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
		notice "Certificate cleanup succeeded for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
	else
		notice "Certificate cleanup not necessary for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
	fi
	return 0
}


# Look for a place where a newly certified hostname is wanted
#  $1 - HOSTname
#
cleanup_host () {
	HOST="$1"
	for WANT in $(ls /tlsall)
	do
		echo "WANT=$WANT"
		for FILE in $(ls /tlsall/$WANT/want/$HOST-* 2>/dev/null)
		do
			echo " - FILE=$FILE"
			case "$FILE" in
			*.pem)
				KIND=pem
				;;
			*.der)
				KIND=der
				;;
			*)
				continue
				echo Skipping $FILE
				;;
			esac
			USER=${FILE#/tlsall/$WANT/want/$HOST-}
			USER=${USER%.$KIND}
			case "$USER" in
			*-*)
				# Zealous match, skip this file
				continue
				;;
			*)
				# Proper match, process this file
				pastdrop_host "$WANT" "$HOST" "$USER" "$KIND"
				[ $? -eq 0 ] || die "Certificate cleanup deferred for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
				;;
			esac
		done
	done
}


# Look for private keys that have no matching "want" entry (anymore)
#  $1 - HOSTname
#
cleanup_priv () {
	HOST="$1"
	for WANT in $(ls /tlsall)
	do
		for PRIV in $(cd /tlsall/$WANT/priv/ ; ls $HOST-* 2>/dev/null)
		do
			if [ ! -f /tlsall/$WANT/want/$PRIV ]
			then
				echo Cleanup of unwanted private key /tlsall/$WANT/priv/$PRIV
				rm /tlsall/$WANT/priv/$PRIV
			fi
		done
	done
}


# Main loop :- Read hostnames from stdin and process them
#
GOT_ANY=false
while read HOST
do
	GOT_ANY=true
	cleanup_host "$HOST"
	[ $? -eq 0 ] || logger -s -p ERROR "Certificate cleanup deferred for $HOST"
	cleanup_priv "$HOST"
done
$GOT_ANY || die "Certificate cleanup expects hostnames on stdin"

#TODO# Trigger DANE generation
#TODO# [ $? -eq 0 ] || die "Certificate held back due to DANE update failure for web0.econono.nl"

#TODO# Update LDAP to advance Life Cycle
#TODO# [ $? -eq 0 ] || die "LDAP failed to update for web0.econono.nl"

return 0

