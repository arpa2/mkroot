#!/bin/ash
#
# dyndns-hasdane -- Check that DANE is setup in all authoritative name servers
#
# Given a hostname, this script retrieves the TLSA records defined for it and
# verifies that it is completely present.  It first retrieves the set of
# authoritative name servers from the local name server and assures the fact
# on each of those.  As with dyndns-setdane, there may be more than one TLSA
# record due to variations in certificate, port and transport protocol.  All
# these are tested.
#
# This routine returns with exit code 0 on succcess, 1 on failure.  The idea
# is that it is called, ideally with exponential back-off, as long as it
# takes to establish the condition.  This is a barring step in a process for
# DANE management.  It is not more than that -- a single, barring atomic step.
#
# From: Rick van Rein <rick@openfortress.nl>



# [ ...check... ] || die "Out of here"
#
die () {
	logger -s -p ERROR "$*"
	exit 1
}


# Inform about a change
#
notice () {
	logger -p NOTICE "$*"
	echo "$*"
}


# The fixed TTL value, as assumed everywhere.  Do not change.
#
TTL=$((12*3600-300))


#
# Check arguments -- Expect a target hostname only
#
if [ $# -ne 1 ]
then
	echo >&2 "Usage: $0 host.domain.name"
	exit 1
fi
TARGETHOST="$1"
case "$TARGETHOST" in
*.*)
	# Looks a bit like an FQDN
	;;
*)
	echo >&2 "Provide a fully qualified host name for $TARGETHOST"
	exit 1
	;;
esac


#
# Determine the domain.  This is either the same as the
# hostname (common for web servers) or it has one extra
# label before it (common for mail servers).  Since the
# list of domains used is authoritative and since there
# are precautions against false certificate requests,
# we need not constrain to one level under the zone.
#
# This is not the place to choose whether a certificate
# will implement one or both forms.  That is up to the
# surrounding logic and requested setup.  This script is
# solely occupied with placing TLSA records online, or
# taking them offline.
#
ZONE=""
for DOMAIN in $(ls /var/lib/arpa2/domain)
do
	case "$TARGETHOST" in
	"$DOMAIN")
		ZONE="$DOMAIN"
		break
		;;
	*".$DOMAIN")
		if [ ${#ZONE} -lt ${#DOMAIN} ]
		then
			ZONE="$DOMAIN"
		fi
		;;
	esac
done


#
# Determine authoritative name servers for the zone
#
ZONE_NS=$(kdig +short "$ZONE" NS)
[ -n "$ZONE_NS" ] || die "DynDNS-hasDANE for $TARGETHOST found no name servers for $ZONE"


#
# NORMALISATION for comparison :-
#
# We collect a TLSA RRset, and produce one line each with
# the contents of the 4 fields; the integer fields 1-3 are
# in the shortest possible form; the hash field has no spaces
# and is in uppercase form.  Internal line breaks are kept
# by "quoting $variables" everywhere.  Note the absense of a
# line break at the end.  The first value is prefixed with
# ".$TTL" as a marker; current this value is considered
# fixed and not subjected to checking.
#
# Given that we know that the DNS owner name is the same,
# that we ignore the TTL and assume dealing with TLSA, and
# finally that the fields are sorted, we can compare these
# strings directly because they have a canonical form.
#
# Normalised forms for multilpe TLSA RRsets combine these
# values in the order of occurrence in tlsall-getdane.ash
# where the ".$TTL" value initiates the next entry.
#
# We therefore normalise the forms that we need to compare.
#



#
# Normalise a TLSA RRset from DNS, after asking:
#  $1 - The fully qualified owner name to look for
#  $2... - The name servers to ask
#
# Return the $NSNORMAL form only if all name servers
# agree, and then return code 0.  Otherwise, return 1
# and set an impossible form in $NSNORMAL.
#
normalise_kdug () {
	RETVAL=0
	FQDN="$1"
	shift
	FIRST=true
	NSNORMAL="NS_IMPOSSIBLE"
	for NS in "$@"
	do
		NEW_NSNORMAL=$(kdig +short @"$NS" "$FQDN" TLSA | sort)
		NEW_NSNORMAL=".$TTL $NEW_NSNORMAL"
		echo >&2 "DEBUG: $NS reports =$NEW_NSNORMAL="
		if $FIRST
		then
			NSNORMAL="$NEW_NSNORMAL"
			FIRST=false
			OLD_NS="$NS"
		else
			if [ "$NSNORMAL" != "$NEW_NSNORMAL" ]
			then
				notice "Different authoritative TLSA records for $FQDN on $OLD_NS and $NS"
				echo >&2 "DEBUG: had =$NSNORMAL= got =$NEW_NSNORMAL="
				NSNORMAL="NS_ERROR"
				RETVAL=1
			fi
		fi
	done
	return $RETVAL
}


#
# Normalise all TLSA RRsets from standard input, which
# is taken to be fed with the tlsall-.ash output.
#  $1 - The fully qualified host name
#
# Return the $CERTNORMAL form only if the process
# worked out, with 0 return code, and do accept
# empty $CERTNORMAL as valid.  Otherwise, return 1
# and set an impossible form in $CERTNORMAL, and a
# different impossible form than in $NSNORMAL.
#
# Name servers will yield these results per FQDN
# through separate queries.  To facilitate that,
# produce an additional $CERTNAMES with the owner
# names to query, in the same order as $CERTNORMAL.
# When returning 1 for error, the $CERTNORMAL value
# is an empty string.
# 
normalise_tlsall () {
	unset CERTNAMES
	CERTNORMAL=""
	OWNER='   '
	while read NEW_OWNER NEW_TTL NEW_CLASS NEW_RRTYPE NEW_RDATA
	do
		[ "$NEW_CLASS"  = "IN"   ] || continue
		[ "$NEW_RRTYPE" = "TLSA" ] || continue
		NEW_OWNER=${NEW_OWNER%.}.
		if [ "$NEW_OWNER" != "$OWNER" ]
		then
			OWNER="$NEW_OWNER"
			PREFIX=".$TTL "
			CERTNAMES="${CERTNAMES:+$CERTNAMES }$NEW_OWNER"
		else
			PREFIX=""
		fi
		CERTNORMAL=$( [ -n "$CERTNORMAL" ] && echo "$CERTNORMAL" ; echo "$PREFIX$NEW_RDATA" | tr a-z A-Z )
	done
	return 0
}


#
# Make a "tlsall-getdane.ash" run of the target host, and derive
# a $CERTNORMAL value along with $CERTNAMES.  Use the latter to
# iterate over name servers and produce the $NSNORMAL for that.
#
# Finally, compare the two values and return with exit code 0
# when they match, or exit code 1 if not.  Processing errors in
# preceding phases also result in an early exit with code 1.
# This script exit code is produced in the last pipeline stage.
#
echo "$TARGETHOST" | tlsall-getdane.ash | (
	#
	# Derive $CERTNORMAL and $CERTNAMES
	normalise_tlsall || die "DynDNS-hasDANE for host $TARGETGHOST failed to derive normal form"
	notice "DynDNS-hasDANE for host $TARGETHOST collected names $CERTNAMES"
	#
	# Derive $NSNORMAL_ALL in an iterative process
	unset NSNORMAL_ALL
	for FQDN in $CERTNAMES
	do
		normalise_kdug "$FQDN" $ZONE_NS || die "DynDNS-hasDANE for host $TARGETHOST failed to normalise name server form"
		NSNORMAL_ALL=$( [ -n "$NSNORMAL_ALL" ] && echo "$NSNORMAL_ALL" ; echo "$NSNORMAL" )
	done
	#
	# Compare the two forms
	notice "DynDNS-hasDANE for host $TARGETHOST ist  $NSNORMAL_ALL"
	notice "DynDNS-hasDANE for host $TARGETHOST soll $CERTNORMAL"
	[ "$NSNORMAL_ALL" = "$CERTNORMAL" ] || die "DynDNS-hasDANE for host $TARGETHOST is not published on all authoritatives"
)

