#!/bin/ash
#
# appctl-newcert -- inform an application about a new certificate
#
# This is done through picoput, or pipe command put, using a pipe
# shared with the application.  Delivery is best-effort, but an
# error code allows later corrections.
#
# From: Rick van Rein <rick@openfortress.nl>


OK=true
while read HOST
do
	logger -s -p daemon.notice "appctl-newcert: Sending certificate notifications for $HOST"
	for _CERTPICO in $(ls /pico/*/certctl.pico.out 2>/dev/null)
	do
		/bin/picoput "$_CERTPICO" HOST="$HOST" newcert
		if [ $? -ne 0 ]
		then
			logger -s -p daemon.warn "appctl-newcert: Failed to send newcert for $HOST via $_CERTPICO"
			OK=false
		fi
	done
done


