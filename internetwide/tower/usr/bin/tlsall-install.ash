#!/bin/flash /var/run/acmetool.lock
#
# Install a named host to the places that want it
#
# From: Rick van Rein <rick@openfortress.nl>


# [ ...check... ] || die "Out of here"
#
die () {
	logger -s -p ERROR "$*"
	exit 1
}


# Inform about a change
#
notice () {
	logger -p NOTICE "$*"
	echo "$*"
}


# Copy a certificate, possibly altering the KIND
#  $1 - pem2pem | pem2der
#  $2 - INFILE
#  $3 - OUTFILE
#
copy_cert () {
	KIND="$1"
	INFILE="$2"
	OUTFILE="$3"
	TMPFILE="$3.tmp"
	case "$KIND" in
	pem2pem)
		cp -f "$INFILE" "$TMPFILE"
		;;
	pem2der)
		openssl x509 -inform pem -in "$INFILE" -outform der -out "$TMPFILE"
		;;
	*)
		die "Certificate kind $KIND is not supported"
		;;
	esac
	mv -f "$TMPFILE" "$OUTFILE"
}


# Given hostname, user/group owner and kind in $1, $2 and $3, try
# to install it as a -next certificate
# Try to install a certificate as a -next [and -live if none yet]
#  $1 - WANTing host
#  $2 - HOSTname for which a certificate is released
#  $3 - USER or group labelled as certificate owner (not the source of UID:GID)
#  $4 - KIND of output, "pem" or "der" or...?
#
install_nextcert () {
	WANT="$1"
	HOST="$2"
	USER="$3"
	KIND="$4"
	echo " - WANT=$WANT"
	echo " - HOST=$HOST"
	echo " - USER=$USER"
	echo " - KIND=$KIND"
	ORIG="/var/lib/acme/live/$HOST/cert"
	PRIV="/var/lib/acme/live/$HOST/privkey"
	WISH="/tlsall/$WANT/want/$HOST-$USER.$KIND"
	NEXT="/tlsall/$WANT/cert/$HOST-next.pem"
	LIVE="/tlsall/$WANT/cert/$HOST-live.pem"
	LIVD="/tlsall/$WANT/cert/$HOST-live.der"
	PAST="/tlsall/$WANT/cert/$HOST-past.pem"
	PRIL="/tlsall/$WANT/priv/$HOST-$USER.pem"
	PRID="/tlsall/$WANT/priv/$HOST-$USER.der"
	PRIW="/tlsall/$WANT/priv/$HOST-$USER.new"
	#
	# Install the live certificate into the -next state
	copy_cert pem2pem "$ORIG" "$NEXT"
	echo -n "Next cert:   "
	ls -l "$NEXT"
	notice "Certificate installation started for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
	#
	# If there is no -live certificate yet, upcycle right now
	if [ ! -r "$LIVE" ]
	then
		copy_cert pem2pem "$NEXT" "$LIVE"
		copy_cert pem2der "$NEXT" "$LIVD"
		echo -n "Certificate: "
		ls -l "$LIVE"
		echo -n "Certificate: "
		ls -l "$LIVD"
		cp -f -p "$WISH" "$PRIW"
		chmod 0222 "$PRIW"
		cat "$PRIV" > "$PRIW"
		chmod 0440 "$PRIW"
		mv -f "$PRIW" "$PRIL"
		echo -n "Private key: "
		ls -l "$PRIL"
		notice "Certificate immediately upcycled for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
	fi
	return 0
}


# Look for a place where a newly certified hostname is wanted
#  $1 - HOSTname
#
install_host () {
	HOST="$1"
	for WANT in $(ls /tlsall)
	do
		echo "WANT=$WANT"
		for FILE in $(ls /tlsall/$WANT/want/$HOST-* 2>/dev/null)
		do
			echo " - FILE=$FILE"
			case "$FILE" in
			*.pem)
				KIND=pem
				;;
			*.der)
				KIND=der
				;;
			*)
				continue
				echo Skipping $FILE
				;;
			esac
			USER=${FILE#/tlsall/$WANT/want/$HOST-}
			USER=${USER%.$KIND}
			case "$USER" in
			*-*)
				#
				# Zealous match, skip this file
				continue
				;;
			*)
				#
				# Proper match, process this file
				install_nextcert "$WANT" "$HOST" "$USER" "$KIND"
				[ $? -eq 0 ] || die "Certificate installation deferred for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
				;;
			esac
		done
	done

}


# Main loop :- Read hostnames from stdin and process them
#
GOT_ANY=false
while read HOST
do
	GOT_ANY=true
	install_host "$HOST"
	[ $? -eq 0 ] || logger -s -p ERROR "Certificate installation deferred for $HOST"
done
$GOT_ANY || die "Certificate installation expects hostnames on stdin"
#TODO# Trigger DANE generation
#TODO# [ $? -eq 0 ] || die "Certificate held back due to DANE update failure for web0.econono.nl"

#TODO# Update LDAP to advance Life Cycle
#TODO# [ $? -eq 0 ] || die "LDAP failed to update for web0.econono.nl"

return 0

