#!/bin/flash /var/run/acmetool.lock
#
# For a given host name, output the DANE records based on all occurrences
# and all versions -- live, next, past.
#
# From: Rick van Rein <rick@openfortress.nl>


# [ ...check... ] || die "Out of here"
#
die () {
	logger -s -p ERROR "$*"
	exit 1
}


# Inform about a change
#
notice () {
	logger -p NOTICE "$*"
	echo "$*"
}


# A filename pattern that matches next|live|past
# and a bit more, but always 4 letters and not what
# an asterisk would do.
#
# Matching $HOST-$NEXT_LIVE_PAST.pem instead of $HOST-*.pem
# avoids problems with $HOST mixups with and without a dash.
#
NEXT_LIVE_PAST=[nlp][eia][xvs][tet]


# Output a DANE record line for a given hostname.
# Transport protocol and port are derived from tran/port listings
# in the "want" directory.  This file may be empty to indicate no
# need for DANE records, but it may also lines like "tcp/443" to
# indicate that the requested certificate for the intended owner
# will use it on one or more tran/port which may be verified with
# DANE by any remote peers.
#
#  $1 - HOSTname on the certificate
#
make_dane () {
	HOST="$1"
	#
	# DANE time-to-live, set to 12 hours or an office varied working day
	# reduced by 300 seconds to allow some extra propagation delay
	TTL=42900
	#
	# DANE usage 1, service endpoint certificate
	USAGE=1
	#
	# DANE selector 0, full certificate verification
	SELECTOR=0
	#
	# DANE matching 1, SHA-256 hash
	MATCHING=1
	#
	# Iterate over active PEM certificates -- avoid matching zeal as with $HOST-*.pem
	for CERT in $(ls /tlsall/*/cert/$HOST-$NEXT_LIVE_PAST.pem 2>/dev/null)
	do
		#
		# Derive the WANT for this certificate
		WANT=$(echo $CERT | sed -e 's+^/tlsall/++' -e 's+/.*++')
		#
		# Iterate over files for this WANT
		for FILE in $(ls /tlsall/$WANT/want/$HOST-*.pem 2>/dev/null)
		do
			case "$FILE" in
			/tlsall/$WANT/want/$HOST-*-*.pem)
				#
				# Skip this zealous match
				continue
				;;
			*)
				#
				# Iterate any tran/port lines that call for DANE
				cat "$FILE" | \
				while read TRAN_PORT
				do
					#
					# Split out the proto/port
					case "$TRAN_PORT" in
					tcp/*)
						TRAN=tcp
						PORT=${TRAN_PORT#tcp/}
						;;
					udp/*)
						TRAN=udp
						PORT=${TRAN_PORT#udp/}
						;;
					sctp/*)
						TRAN=sctp
						PORT=${TRAN_PORT#sctp/}
						;;
					*)
						# Ignore silly/other type of line
						continue
						;;
					esac
					#
					# DANE association data, the SHA-256 hash in hexadecimal
					ASSOCDATA=$(openssl x509 -in "$CERT" -outform der -out - | openssl sha256 -hex | sed 's/^.*= *//')
					echo "_$PORT._$TRAN.$HOST.  $TTL IN TLSA  $USAGE $SELECTOR $MATCHING $ASSOCDATA"
				done
				;;
			esac
		done
	done | sort -u
}


# Main loop :- Read hostnames from stdin and process them
#
GOT_ANY=false
while read HOST
do
	GOT_ANY=true
	make_dane "$HOST"
	[ $? -eq 0 ] || logger -s -p ERROR "DANE retrieval deferred for $HOST"
done
$GOT_ANY || die "DANE retrieval expects hostnames on stdin"
