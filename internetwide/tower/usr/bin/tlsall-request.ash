#!/bin/ash
#
# Iterate over /tlsall/*/want/* to learn about required hostnames
#
# From: Rick van Rein <rick@openfortress.nl>

uniq () {
	awk '{if(!seen[$ARGV0]++)print}'
}

want2host () {
	sed -e 's+^.*/++' -e 's+-[^-/]*[.][a-z]*$++'
}

for HOST in $( ls /tlsall/*/want/* | want2host | uniq )
do
	#TODO# For now, just echo what ACMETOOL should do -- to get that stable first
	echo acmetool want "$HOST"
done

