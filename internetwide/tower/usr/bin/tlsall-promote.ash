#!/bin/flash /var/run/acmetool.lock
#
# Make the next certificate got live for the given hostname.
#
# This is called after the TTL on a newly added host has expired.
# Its aim is to make the next certificate for a hostname go live.
# It may however be deferred until the -past certificate is gone.
# If it is deferred, it should be called again sometime later.
# The cleanup of the old certificate is separately timed.
#
# From: Rick van Rein <rick@openfortress.nl>


# [ ...check... ] || die "Out of here"
#
die () {
	logger -s -p ERROR "$*"
	exit 1
}


# Inform about a change
#
notice () {
	logger -p NOTICE "$*"
	echo "$*"
}


# Copy a certificate, possibly altering the KIND
#  $1 - pem2pem | pem2der
#  $2 - INFILE
#  $3 - OUTFILE
#
copy_cert () {
	KIND="$1"
	INFILE="$2"
	OUTFILE="$3"
	TMPFILE="$3.tmp"
	case "$KIND" in
	pem2pem)
		cp -f "$INFILE" "$TMPFILE"
		;;
	pem2der)
		openssl x509 -inform pem -in "$INFILE" -outform der -out "$TMPFILE"
		;;
	*)
		die "Certificate kind $KIND is not supported"
		;;
	esac
	mv -f "$TMPFILE" "$OUTFILE"
}


# Given hostname, user/group owner and kind in $1, $2 and $3, try
# to promote from -next to -live certificate.
#  $1 - WANTing host
#  $2 - HOSTname for which a certificate is released
#  $3 - USER or group labelled as certificate owner (not the source of UID:GID)
#  $4 - KIND of output, "pem" or "der" or...?
#
next2live_host () {
	WANT="$1"
	HOST="$2"
	USER="$3"
	KIND="$4"
	echo " - WANT=$WANT"
	echo " - HOST=$HOST"
	echo " - USER=$USER"
	echo " - KIND=$KIND"
	ORIG="/var/lib/acme/live/$HOST/cert"
	FULL="/var/lib/acme/live/$HOST/fullchain"
	PRIV="/var/lib/acme/live/$HOST/privkey"
	WISH="/tlsall/$WANT/want/$HOST-$USER.$KIND"
	NEXT="/tlsall/$WANT/cert/$HOST-next.pem"
	LIVE="/tlsall/$WANT/cert/$HOST-live.pem"
	LIVD="/tlsall/$WANT/cert/$HOST-live.der"
	PAST="/tlsall/$WANT/cert/$HOST-past.pem"
	PRIL="/tlsall/$WANT/priv/$HOST-$USER.pem"
	PRID="/tlsall/$WANT/priv/$HOST-$USER.der"
	PRIW="/tlsall/$WANT/priv/$HOST-$USER.new"
	AUTH="/tlsall/$WANT/cert/ca.pem"
	DHPM="/tlsall/$WANT/cert/dhparam.pem"
	#
	# Checks
	[ ! -r "$PAST" ] || die "Certificate promotion haunted by past for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
	if [ ! -r "$NEXT" ]
	then
		notice "Certificate promotion was already completed for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
		return 0
	fi
	cmp -s "$ORIG" "$NEXT" || die "Certificate promotion overtaken by future for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
	if [ cmp -s "$NEXT" "$LIVE" ]
	then
		notice "Certificate promotion unnecessary for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
		return 0
	fi
	#
	# Certificate
	echo -n "Next cert:   "
	ls -l "$NEXT"
	if [ -r "$LIVE" ]
	then
		copy_cert pem2pem "$LIVE" "$PAST"
		echo -n "Past cert:   "
		ls -l "$PAST"
	fi
	copy_cert pem2pem "$NEXT" "$LIVE"
	copy_cert pem2der "$NEXT" "$LIVD"
	echo -n "Certificate: "
	ls -l "$LIVE"
	echo -n "Certificate: "
	ls -l "$LIVD"
	#
	# Private key
	cp -f -p "$WISH" "$PRIW"
	chmod 0222 "$PRIW"
	cat "$PRIV" > "$PRIW"
	chmod 0440 "$PRIW"
	mv -f "$PRIW" "$PRIL"
	echo -n "Private key: "
	ls -l "$PRIL"
	rm -f "$NEXT"
	#
	# CA
	diff "$ORIG" "$FULL" | sed -e '/^+/!d' -e 's/^+//' -e '/^-----BEGIN/,/^-----END/!d' > "$AUTH.tmp"
	mv -f "$AUTH.tmp" "$AUTH"
	#
	# DH params
	if [ ! -r "$DHPM" ]
	then
		openssl dhparam -out "$DHPM" 4096
	fi
	#
	# Report success
	notice "Certificate promotion succeeded for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
	return 0
}


# Look for a place where a newly certified hostname is wanted
#  $1 - HOSTname
#
promote_host () {
	HOST="$1"
	for WANT in $(ls /tlsall)
	do
		echo "WANT=$WANT"
		for FILE in $(ls /tlsall/$WANT/want/$HOST-* 2>/dev/null)
		do
			echo " - FILE=$FILE"
			case "$FILE" in
			*.pem)
				KIND=pem
				;;
			*.der)
				KIND=der
				;;
			*)
				continue
				echo Skipping $FILE
				;;
			esac
			USER=${FILE#/tlsall/$WANT/want/$HOST-}
			USER=${USER%.$KIND}
			case "$USER" in
			*-*)
				#
				# Zealous match, skip this file
				continue
				;;
			*)
				#
				# Proper match, process this file
				next2live_host "$WANT" "$HOST" "$USER" "$KIND"
				[ $? -eq 0 ] || die "Certificate promotion deferred for WANT=$WANT HOST=$HOST USER=$USER KIND=$KIND"
				;;
			esac
		done
	done

}


# Main loop :- Read hostnames from stdin and process them
#
GOT_ANY=false
while read HOST
do
	GOT_ANY=true
	promote_host "$HOST"
	[ $? -eq 0 ] || logger -s -p ERROR "Certificate promotion deferred for $HOST"
done
$GOT_ANY || die "Certificate promotion expects hostnames on stdin"

#TODO# Trigger DANE generation
#TODO# [ $? -eq 0 ] || die "Certificate held back due to DANE update failure for web0.econono.nl"

#TODO# Trigger DANE generation
#TODO# [ $? -eq 0 ] || die "Certificate held back due to DANE update failure for web0.econono.nl"

#TODO# Update LDAP to advance Life Cycle
#TODO# [ $? -eq 0 ] || die "LDAP failed to update for web0.econono.nl"

return 0

