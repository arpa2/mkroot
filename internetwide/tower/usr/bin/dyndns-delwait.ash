#!/bin/ash
#
# Assure that at least the TTL has been passed since the
# last tlsall-promote was done.  This assures that the
# old certificate has lingering in DANE records for long
# enough to be generally forgotten.  This is a useful
# test to perform before tlsall-cleanup is applied.
#
# The same host may appear in multiple places, and we
# like to be cautious, so we check them all.
#
# From: Rick van Rein <rick@openfortress.nl>



# [ ...check... ] || die "Out of here"
#
die () {
	logger -s -p ERROR "$*"
	exit 1
}


# Inform about a change
#
notice () {
	logger -p NOTICE "$*"
	echo "$*"
}


# Expect one argument, namely the target hostname.
#
if [ $# -ne 1 ]
then
	die "Usage: $0 host.domain.name"
fi
TARGETHOST="$1"
case "$TARGETHOST" in
*.*)
	# Looks a bit like an FQDN
	;;
*)
	die "Provide a fully qualified host name for $TARGETHOST"
	;;
esac


# The fixed TTL value, as explained above.  Do not change.
#
TTL=$((12*3600-300))


# Iterate over the files with the target hostname to
# find the youngest.  If we found nothing then cleanup
# is trivially permitted, the wait is over.
#
YOUNGEST=0
for TIMESTAMP in $(stat -c %Y /tlsall/*/cert/$TARGETHOST-past.pem 2>/dev/null)
do
	if [ $TIMESTAMP -gt $YOUNGEST ]
	then
		YOUNGEST="$TIMESTAMP"
	fi
done
#
if [ $YOUNGEST -le 0 ]
then
	notice "DynDNS-delwait for $TARGETHOST ends -- no such certificate"
	exit 0
fi


# Check if the TTL has passed since the youngest certificate.
#
CLOCKTIME=$(date +%s)
THRESHOLD=$(($YOUNGEST+$TTL))
REMAINDER=$(($THRESHOLD-$CLOCKTIME))


# Return with a 0 exit code only when we're done
#
if [ $REMAINDER -ge 0 ]
then
	notice "DynDNS-delwait called $REMAINDER seconds early"
	exit 1
else
	exit 0
fi
