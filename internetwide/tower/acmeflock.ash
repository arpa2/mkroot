#!/bin/ash
#
# Move acmetool to acmetool.real and wrap it in a flock.
#
# From: Rick van Rein <rick@openfortress.nl>


if [ ! -h /usr/bin/acmetool ]
then
	mv -f /usr/bin/acmetool /usr/bin/acmetool.real
	ln -s acmetool.locked /usr/bin/acmetool
fi

