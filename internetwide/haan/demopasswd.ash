#!/bin/ash
#
# Setup demo@arpa2.net demonstration password
# Setup {john,mary}@example.com demonstration passwords
#
# From: Rick van Rein <rick@openfortress.nl>


echo sekreet    | /usr/sbin/saslpasswd2 -c -f /etc/sasldb2 -u ${IWO_DOMAIN_ISP:-example.com} demo
echo sekreet1   | /usr/sbin/saslpasswd2 -c -f /etc/sasldb2 -u ${IWO_DOMAIN_ISP:-example.com} demo1
echo sekreet2   | /usr/sbin/saslpasswd2 -c -f /etc/sasldb2 -u ${IWO_DOMAIN_ISP:-example.com} demo2
echo sekreet3   | /usr/sbin/saslpasswd2 -c -f /etc/sasldb2 -u ${IWO_DOMAIN_ISP:-example.com} demo3
echo sekreet123 | /usr/sbin/saslpasswd2 -c -f /etc/sasldb2 -u ${IWO_DOMAIN_ISP:-example.com} john
echo sekreet456 | /usr/sbin/saslpasswd2 -c -f /etc/sasldb2 -u ${IWO_DOMAIN_ISP:-example.com} mary
