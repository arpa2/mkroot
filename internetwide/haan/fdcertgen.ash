#!/bin/ash
#
# Setup certificates for freeDiameter
#
# This uses an internal CA, so it has limited use as it stands.
#
# THIS KIND OF SCRIPTING IS OBSOLETED BY rootfs_certificate()


#TODO# Migrate towards a mounted directory (but not in fixups) --> "mkroot" issue #28
mkdir -p /etc/tls /etc/tls/req /etc/tls/private /etc/tls/certs

cd "/etc/tls" || exit 1


#
# Construct a local Certificate Authority.
#
# This is slightly better than self-signed certs, but does
# not scale to the entire Internet, so work remains.
#
if [ ! -d ca ]
then
	# CA infrastructure
	mkdir ca ca/certs ca/crl && \
	echo 01 > ca/serial && \
	touch ca/index.txt \
	|| exit 1
	# CA self certificate
	openssl req -batch -nodes \
		-config /etc/openssl.cnf \
		-new -x509 \
		-days 3650 \
		-newkey rsa:4064 \
		-subj /CN=kip-haan-ca.${IWO_DOMAIN_ISP:-example.com}/OU=HelmArbitraryAccessNode/O=${IWO_DOMAIN_ISP:-example.com}/OU=InternetWide \
		-keyout ca/cakey.pem \
		-out ca/cacert.pem \
	|| exit 1
	cp ca/cacert.pem certs/cacert.pem
	# Restrict access (somewhat)
	chmod go-rwx ca/*
fi


#
# Construct a private key and certificate for SASL over freeDiameter.
# This includes HAAN traffic (but KIP has its own protocol).
#
# Use the internal CA.
#
if [ ! -r certs/diameter-sasl.pem ]
then
	rm -f private/diameter-sasl.pem req/diameter-sasl.pem certs/diameter-sasl.pem
	openssl genrsa \
		-out private/diameter-sasl.pem \
		1664 && \
	openssl req -batch -nodes \
		-config /etc/openssl.cnf \
		-new \
		-subj /CN=haan${IWO_SITEINST_ME}.${IWO_DOMAIN_ISP:-example.com}/OU=HelmArbitraryAccessNode/O=${IWO_DOMAIN_ISP:-example.com}/OU=InternetWide \
		-key private/diameter-sasl.pem \
		-out req/diameter-sasl.pem && \
	openssl ca -batch \
		-config /etc/openssl.cnf \
		-cert ca/cacert.pem \
		-keyfile ca/cakey.pem \
		-in req/diameter-sasl.pem \
		-out certs/diameter-sasl.pem \
	|| exit 1
fi


#
# Report success
#
return 0
