#!/bin/ash

if [ -x usr/bin/pinentry-curses -a ! -x usr/bin/pinentry ]
then
	ln -s pinentry-curses usr/bin/pinentry
fi

