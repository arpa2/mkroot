#!/bin/sh

#TODO# Is this still needed after "nonlocal" has run?
export LD_LIBRARY_PATH=rootfs/lib:rootfs/usr/lib:rootfs/usr/local/lib:rootfs/lib/x86_64-linux-gnu:rootfs/usr/lib/x86_64-linux-gnu:rootfs/usr/local/lib/x86_64-linux-gnu

export KIP_VARDIR=/var/lib/kip
export KIP_KEYTAB=/var/lib/kip/master.keytab
export KIP_REALM=unicorn.demo.arpa2.org

KIP_VARDIR=../vardir/${KIP_VARDIR#/var/}
KIP_KEYTAB=../vardir/${KIP_KEYTAB#/var/}

mkdir -p "$KIP_VARDIR"

ENCTYPE=aes256-cts-hmac-sha384-192

mkdir -p $KIP_VARDIR

/rootfs/usr/sbin/a2kip master create enctype $ENCTYPE service haan vardir "$KIP_VARDIR"

for ZONE in ${IWO_DOMAIN_ISP:-example.com} unicorn.demo.arpa2.org
do
	if [ ! -e "${KIP_VARDIR#/}/haan-vhost/$ZONE" ]
	then
		/rootfs/usr/sbin/a2kip virtual add domain "$ZONE" service haan
	fi
done

