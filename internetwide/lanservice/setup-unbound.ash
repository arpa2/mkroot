#!/bin/ash

echo ADDING USER/GROUP UNBOUND
groupadd -g 1000 unbound
useradd -g 1000 -u 1000 unbound
chown -R unbound:unbound vardir/lib/unbound
chmod ug+w vardir/lib/unbound
echo DONE
