{
	"ociVersion": "1.0.1",
	"process": {
		"terminal": true,
		"user": {
			"uid": 0,
			"gid": 0
		},
		"args": [
			"/bin/init"
		],
		"env": [
			"PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin",
			"PS1=\\[\\e[01;32m\\]\\u@\\h\\[\\e[00m\\]:\\[\\e[01;34m\\]\\w\\[\\e[00m\\]\\$\\ ",
			"LS_OPTIONS=--color=auto",
			"LESS=R",
			"TERM=ansi",
			"KIP_VARDIR=/var/lib/kip",
			"KIP_KEYTAB=/var/lib/kip/master.keytab",
			"KIP_REALM=@IWO_DOMAIN_ISP@",
			"KIPVHS_HAVE=@KIP_VIRTUAL_HOSTS@",
			"KIPSERVICE_CLIENT_REALM=@IWO_DOMAIN_ISP@",
			"KIPSERVICE_CLIENTUSER_LOGIN=demo",
			"KIPSERVICE_CLIENTUSER_ACL=demo+ali",
			"SASL_CONF_PATH=/etc/freeDiameter",
			"UNBOUND_CONFIG=/etc/unbound/unbound.conf.d/arpa2demo.conf"
		],
		"cwd": "/",
		"capabilities": {
			"bounding": [
				"CAP_SETUID",
				"CAP_SETGID",
				"CAP_CHOWN",
				"CAP_NET_ADMIN",
				"CAP_NET_RAW",
				"CAP_SETUID",
				"CAP_NET_BIND_SERVICE",
				"CAP_AUDIT_WRITE",
				"CAP_KILL",
				"CAP_NET_BIND_SERVICE"
			],
			"effective": [
				"CAP_SETUID",
				"CAP_SETGID",
				"CAP_CHOWN",
				"CAP_NET_ADMIN",
				"CAP_NET_RAW",
				"CAP_SETUID",
				"CAP_NET_BIND_SERVICE",
				"CAP_AUDIT_WRITE",
				"CAP_KILL",
				"CAP_NET_BIND_SERVICE"
			],
			"inheritable": [
				"CAP_SETUID",
				"CAP_SETGID",
				"CAP_CHOWN",
				"CAP_NET_ADMIN",
				"CAP_NET_RAW",
				"CAP_SETUID",
				"CAP_NET_BIND_SERVICE",
				"CAP_AUDIT_WRITE",
				"CAP_KILL",
				"CAP_NET_BIND_SERVICE"
			],
			"permitted": [
				"CAP_SETUID",
				"CAP_SETGID",
				"CAP_CHOWN",
				"CAP_NET_ADMIN",
				"CAP_NET_RAW",
				"CAP_SETUID",
				"CAP_NET_BIND_SERVICE",
				"CAP_AUDIT_WRITE",
				"CAP_KILL",
				"CAP_NET_BIND_SERVICE"
			],
			"ambient": [
				"CAP_SETUID",
				"CAP_SETGID",
				"CAP_CHOWN",
				"CAP_NET_ADMIN",
				"CAP_NET_RAW",
				"CAP_SETUID",
				"CAP_NET_BIND_SERVICE",
				"CAP_AUDIT_WRITE",
				"CAP_KILL",
				"CAP_NET_BIND_SERVICE"
			]
		},
		"rlimits": [
			{
				"type": "RLIMIT_NOFILE",
				"hard": 5000,
				"soft": 5000
			}
		],
		"noNewPrivileges": true
	},
	"root": {
		"path": "rootfs",
		"readonly": true
	},
	"hostname": "svc@IWO_SITEINST_ME@",
	"mounts": [
		{
			"destination": "/proc",
			"type": "proc",
			"source": "proc"
		},
		{
			"destination": "/dev",
			"type": "tmpfs",
			"source": "tmpfs",
			"options": [
				"nosuid",
				"strictatime",
				"mode=755",
				"size=65536k"
			]
		},
		{
			"destination": "/dev/pts",
			"type": "devpts",
			"source": "devpts",
			"options": [
				"nosuid",
				"noexec",
				"newinstance",
				"ptmxmode=0666",
				"mode=0620",
				"gid=5"
			]
		},
		{
			"destination": "/dev/shm",
			"type": "tmpfs",
			"source": "shm",
			"options": [
				"nosuid",
				"noexec",
				"nodev",
				"mode=1777",
				"size=65536k"
			]
		},
		{
			"destination": "/dev/mqueue",
			"type": "mqueue",
			"source": "mqueue",
			"options": [
				"nosuid",
				"noexec",
				"nodev"
			]
		},
		{
			"destination": "/sys",
			"type": "sysfs",
			"source": "sysfs",
			"options": [
				"nosuid",
				"noexec",
				"nodev",
				"ro"
			]
		},
		{
			"destination": "/sys/fs/cgroup",
			"type": "cgroup",
			"source": "cgroup",
			"options": [
				"nosuid",
				"noexec",
				"nodev",
				"relatime",
				"ro"
			]
		},
		{
			"destination": "/tmp",
			"type": "tmpfs",
			"source": "tmpfs",
			"options": [
				"noexec",
				"nodev",
				"nosuid",
				"strictatime",
				"mode=1777",
				"size=65536k",
				"rw"
			]
		},
		{
			"destination": "/etc/tls",
			"source": "@TLS_DIRTOMOUNT@",
			"options": [
				"bind",
				"nosuid",
				"noexec",
				"nodev",
				"relatime",
				"ro"
			]
		},
		{
			"destination": "/var",
			"source": "@CMAKE_CURRENT_BINARY_DIR@/vardir",
			"options": [
				"bind",
				"nosuid",
				"noexec",
				"nodev",
				"relatime",
				"rw"
			]
		},
		{
			"destination": "/var/log",
			"source": "@CMAKE_BINARY_DIR@/log/svc@IWO_SITEINST_ME@",
			"options": [
				"bind",
				"nosuid",
				"noexec",
				"nodev",
				"relatime",
				"rw"
			]
		}
	],
	"linux": {
		"resources": {
			"devices": [
				{
					"allow": false,
					"access": "rwm"
				}
			]
		},
		"namespaces": [
			{
				"type": "pid"
			},
			{
				"type": "network",
				"path": "/var/run/netns/@SVC_NETNS@"
			},
			{
				"type": "ipc"
			},
			{
				"type": "uts"
			},
			{
				"type": "mount"
			}
		],
		"maskedPaths": [
			"/proc/kcore",
			"/proc/latency_stats",
			"/proc/timer_list",
			"/proc/timer_stats",
			"/proc/sched_debug",
			"/sys/firmware",
			"/proc/scsi"
		],
		"readonlyPaths": [
			"/proc/asound",
			"/proc/bus",
			"/proc/fs",
			"/proc/irq",
			"/proc/sys",
			"/proc/sysrq-trigger"
		]
	},
	"hooks": {
		"prestart": [
			{
				"path": "@CMAKE_CURRENT_BINARY_DIR@/net4svc.sh",
				"args": [ "net4svc.sh", "up" ],
				"timeout": 10
			}
		],
		"poststop": [
			{
				"path": "@CMAKE_CURRENT_BINARY_DIR@/net4svc.sh",
				"args": [ "net4svc.sh", "down" ],
				"timeout": 10
			}
		]
	}
}
