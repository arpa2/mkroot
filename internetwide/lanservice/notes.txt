 - Unbound and DNSmasq require CAP_SETUID and CAP_SETGID to drop privileges
 - Unbound requires a very high rlimit for RLIMIT_NOFILES, like soft/hard 5000
 - DNSmasq requires CAP_NET_ADMIN, CAP_NET_RAW, CAP_SETUID, CAP_NET_BIND_SERVICE for DHCP/ARP

