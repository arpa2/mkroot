#!/bin/ash

groupadd -g 0 adm
useradd -g 0 -u 0 root

sed -i \
	-e 's/^\(^[^#]*load="imklog"\)/#NOTHERE# \1/' \
	-e 's/^#\(.*\)"\(imtcp\|imudp\)"/\1"\2"/' \
	/etc/rsyslog.conf
