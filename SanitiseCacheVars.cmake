# Sanitise Cache Variables
#
# This script will be used to update variables as used in caches.
# It may need to ask the user to do things explicitly.
# Documentation is found below, prefixed with "Update <Month> <YEAR>:"
#
# From: Rick van Rein <rick@openfortress.nl>


# Initially assume we will not run into trouble
#
set (_INSANITY FALSE)
macro (report _MSG)
	message (STATUS "Error at line ${CMAKE_CURRENT_LIST_LINE}: ${_MSG}")
	set (_INSANITY TRUE)
endmacro (report)


# Utilities:
#  - delcache(_VAR _OKVAL) to remove cache variable _VAR if its value is _OKVAL
#  - movcache(_OLD _NEW _TYPE _DESCR) to rename cache variable _OLD to _NEW
#
function (delcache _VAR _OKVAL)
	if (DEFINED ${_VAR})
		if (NOT "${${_VAR}}" STREQUAL "${_OKVAL}")
			report ("Please stop relying on ${_VAR} and set it to \"${_OKVAL}\"")
		endif ()
		unset (${_VAR} CACHE)
	endif ()
endfunction (delcache)
#
function (movcache _OLD _NEW _TYPE _DESCR)
	if (DEFINED ${_OLD})
		if (NOT DEFINED ${_NEW})
			set (${_NEW} "${${_OLD}}" CACHE ${_TYPE} ${_DESCR})
			unset (${_OLD} CACHE)
		else ()
			report ("You cannot have old cachevar ${_OLD} and new ${_NEW}")
		endif ()
	endif ()
endfunction (movcache)




# Update April 2022: Removal of name settings for bridges and network namespaces
#
# We used to have IWO_xxx_BRIDGE and IWO_xxx_NETNS symbols.
# They were never used and will be removed.
#
foreach (_NAME IN ITEMS iwo0adm iwo0bck iwo0ctl iwo0dmz iwo0plg iwo0pub iwo0sit iwo0svc iwo0ngb)
	delcache (IWO_${_NAME}_BRIDGE "${_NAME}")
	delcache (IWO_${_NAME}_NETNS  "")
endforeach ()


# Update April 2022: Rename IWO_HOST_ portfwd settings to NAT_PORTFWD_
#
# We used IWO_HOST_xxx0_proto_port and will sort them better as NAT_PORTFWD_xxx_xxx_proto_port
#
foreach (_NAME IN ITEMS chat0_TCP_194 chat0_TCP_5222 chat0_TCP_5269 chat0_TCP_6667 haan0_SCTP_5868 haan0_TCP_443 id0_SCTP_5868 id0_TCP_464 id0_TCP_749 id0_TCP_88 id0_UDP_464 id0_UDP_749 id0_UDP_88 kip0_SCTP_5868 kip0_TCP_443 kip0_TCP_5868 kip0_TCP_9876 mail0_TCP_25 mail0_TCP_587 name0_TCP_53 name0_UDP_53 sched0_TCP_443 sched0_TCP_80 svc0_SCTP_5868 web0_TCP_443 web0_TCP_80 zone0_TCP_53 zone0_UDP_53)
	string (REPLACE "0_" "_" _NEWNAME "${_NAME}")
	movcache (IWO_HOST_${_NAME} NAT_PORTFWD_internetwide_${_NEWNAME} STRING "NAT port forwarding from IPv4")
endforeach ()
foreach (_NAME IN ITEMS bind0_TCP_53 bind0_UDP_53 identity0_TCP_464 identity0_TCP_749 identity0_TCP_88 identity0_UDP_464 identity0_UDP_749 identity0_UDP_88 nginx0_TCP_80 sip0_TCP_5060)
	delcache (IWO_HOST_${_NAME} "")
endforeach ()


# Update April 2022: Remove _VERSION_ and _VARIANT_ and _FLAVOUR_ unchanged settings
#
# We generated extensive lists, but they are only used exceptionally.
#
foreach (_HOST IN ITEMS arpa2dns chat haan identity kip kip_haan kiphaan lanservice mail pub2dmz schedule tower web zonemaster arpa2dns chat haan identity kip kip_haan kiphaan lanservice mail pub2dmz schedule tower web zonemaster)
	foreach (_PKG IN ITEMS 6bed4 acmetool apachemod arpa2cm arpa2common arpa2shell axesmtp freediameter freediameter_sasl gdb kip knot kxover mkrootutils pavlov pulleyback qpid_dispatch qpid_proton quickder quickmem quicksasl steamworks strace tlspool twin)
		delcache (${_HOST}_VERSION_${_PKG} "(no override)")
		delcache (${_HOST}_VARIANT_${_PKG} "(no override)")
		delcache (${_HOST}_FLAVOUR_${_PKG} "(no override)")
	endforeach ()
endforeach ()


# Update April 2022: Remove IWO_HOST_xxx_IFACE_IDENT if not altered.
#
# We have not overridden those yet, so we will assume they are empty.
#
delcache (IWO_HOST_bind0_IFACE_IDENT "::a2:53a")
delcache (IWO_HOST_chat0_IFACE_IDENT "::a2:194")
delcache (IWO_HOST_ctl0_IFACE_IDENT "::a2:161")
delcache (IWO_HOST_haan0_IFACE_IDENT "::a2:9877")
delcache (IWO_HOST_id0_IFACE_IDENT "::a2:88")
delcache (IWO_HOST_identity0_IFACE_IDENT "::a2:88")
delcache (IWO_HOST_kip0_IFACE_IDENT "::a2:9876")
delcache (IWO_HOST_mail0_IFACE_IDENT "::a2:25")
delcache (IWO_HOST_name0_IFACE_IDENT "::a2:53")
delcache (IWO_HOST_nginx0_IFACE_IDENT "::a2:80a")
delcache (IWO_HOST_sched0_IFACE_IDENT "::a2:5546")
delcache (IWO_HOST_sip0_IFACE_IDENT "::a2:5060")
delcache (IWO_HOST_svc0_IFACE_IDENT "::a2:802")
delcache (IWO_HOST_web0_IFACE_IDENT "::a2:443")
delcache (IWO_HOST_zone0_IFACE_IDENT "::a2:53")


# Update April 22: Move from a /64 site prefix to a /96 prefix
#
# This requires manual work, because it has a lot of impact
# that the operator needs to be aware of.
#
if (DEFINED IWO_PUBLIC_PREFIX64)
	report ("\nBIG CHANGE TO NETWORKING:\n\nWe moved from a /64 prefix per site to a /96\n\nPlease pdate IWO_PUBLIC_PREFIX64=\"${IWO_PUBLIC_PREFIX64}\" to IWO_PUBLIC_PREFIX96=...\n\nBE CAREFUL ABOUT NETWORK CHANGES AFTERWARDS; probably rebuild it\n\n")
endif ()


# Update April 22: Make sure that the IWO_xxx0_PREFIX96 have no trailing "::"
#
# The IWO_PUBLIC_PREFIX64 has trailing :: to please the user.  The rest
# exists to please scripts (and can do without).  Checkk that this has
# indeed arrived in the cache.
#
foreach (_BRIDGE IN ITEMS adm bck ctl dmz plg pub sit svc ngb)
	if (DEFINED IWO_${IWO_SITENAME}.${_BRIDGE}0_PREFIX96)
		if (IWO_${IWO_SITENAME}.${_BRIDGE}0_PREFIX96 MATCHES "^(fd[0-9a-fA-F]+:[0-9a-fA-F]+:[0-9a-fA-F]+:[0-9a-fA-F]+:[0-9a-fA-F]+:[0-9a-fA-F]+)::$")
			unset (IWO_${IWO_SITENAME}.${_BRIDGE}0_PREFIX96 CACHE)
		endif ()
	endif ()
endforeach ()


# Update May 1: Remove the tunnel facilities
#
delcache (IWO_UPLINK_PREFIX96 "native_routed")


# Update May 16, 2022: Remove zealous settings for DNS
#
# See commit f054a560c0956bdf000bdf782671dfec6278682e
# Note that we are removing support for static configuration of extra zones!
#
foreach (_VAR in ITEMS
			DNS_PRIMARY_ZONES
			DNS_PRIMARY_SLAVES DNS_PRIMARY_SLAVES_IPV4
			DNS_PRIMARY_SLAVES_TSIG_NAME)
		delcache (${_VAR} "")
endforeach ()
foreach (_VAR IN ITEMS
			DNS_PRIMARY_SLAVES_TSIG_ALG
			DNS_INTERNAL_TSIG_ALG)
		delcache (${_VAR} "hmac-sha256")
endforeach ()
delcache (DNS_PRIMARY_SLAVES_TSIG_NAME "name1.zone0.tsig.mkroot")
delcache (DNS_INTERNAL_TSIG_NAME "name0.zone0.tsig.mkroot")



# Finally, stop fatally if we ran into trouble
#
if (${_INSANITY})
	message (FATAL_ERROR "Please fix the above before retrying.  Read ${CMAKE_CURRENT_LIST_FILE} for details")
endif ()

