# Contribution "mkroot" container for Gitea

> *This container offers Git service through the Gitea daemon.*

Gitea is a relatively simple service to run, it centers around
one binary.  It offers a web interface and access control.
The intention is to integrate much of this access control with
the "mkroot" mechanisms for user management.

We will probably migrate our project Git service to this and
host it ourselves.  The ongoing popularity of free offerings
to open source developers seems to lead to ongoing scrutiny
in the hosted facilities, and make them less comfortable for
everyday development.

