#!/bin/ash
#
# Gitea wrapper for running it inside a container
#
# From: Rick van Rein <rick@openfortress.nl>


export HOME=/var/home/gitea
cd "$HOME"

/usr/local/sbin/gitea web -c /etc/gitea/app.ini
RETVAL=$?

sleep 5

exit $?
