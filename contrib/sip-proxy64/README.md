# README for contrib/sip-proxy64

> *You want to use IPv6-only services, especially for peer-to-peer
> protocols.  But your SIP phones lag behind, and are stuck to IPv4.*

The SIPproxy64 software is a simple, almost stateless translation
program between IPv4 and IPv6.  Almost stateless, because it will
not just pass SIP but also translate the RTP media flows that it
sets up.

Use this software to accommodate SIP devices that cannot work with
IPv6 yet, but that are surrounded by a network that can.  Run this
container (or the contained software) on the LAN that holds those
SIP phones, so you never need to cross NAT.  The translation can be
done before that, and IPv6 leaves and enters your network much more
easily.


## Here's Why

There is a great benefit in doing this.  Here's the cascade of
benefits spelled out:

  * Your SIP Phone appears with an IPv6 address/port to the outside
  * As a result, the outside transparantly knows where to find your phone
  * No NAT traversal is required, and no media endpoints need to be patched
  * Piercing through a firewall is easy (when both sides allow it, as for media)
  * Media can be sent directly between peers, without intermediate party relaying it
  * You can encrypt without being stopped by SDP modification in media relays
  * You can open the SIP port to your phone's IPv6 address if you like (or you could send keepalives)
  * Given that, anyone can reach you over the Internet (or perhaps just your SIP Trunk)
  * People may reach you through your domain line, using SIP without telco
  * If you liked, you could use ENUM to declare a bypass for your phone number
  * The latter is a free alternative to free access numbers (paid by the receiver)

In short, **SIP over IPv6 sets telephony free** and makes it a true
Internet protocol, as it was originally designed.  IPv4 in contrast,
brings about the need to handle NAT and gets this freedom bogged down.


## Here's How

In broad terms, this container has an interface over which it can be
reached with an (internal) IPv4 address of your choosing.  Allocate a
fixed value in your router or pick an address that will not be
assigned to hosts via DHCP.  This will be available over `sip4to6`, a
virtual network interface that tunnels to `sip6to4` inside this
container.

The container receives your SIP and RTP traffic and translates IPv4
address/port combinations to IPv6 address/port combinations.  These
are then tunneled like other IPv6 traffic over `dmz0` as part of
your "mkroot" infrastructure.

Outside the container, you need to connect the `sip4to6` interface
to your SIP network.  It is common for SIP networks to use a VLAN
(or you can start doing that now) with a number like 123, and you
could peel that off from a real network interface like `eth0` and
then insert both into a bridge like `sip4` with:

```
ip link add sip4vlan type vlan id 123
ip link set up dev sip4vlan
brctl addbr sip4
ip link set up dev sip4
brctl addif sip4 sip4to6
brctl addif sip4 sip4vlan
```

Debian could automate the setup with its `interfaces(5)` setup,
in response to the container coming up and going down:

```
auto sip4
iface sip4 inet manual
	pre-up    ip link add link eth0 name sip4vlan type vlan id 123
	pre-up    ip link set up dev sip4vlan
	pre-up    brctl addbr sip4
	up        ip link set up   dev sip4
	up        brctl addif sip4 sip4vlan
	down      brctl delif sip4 sip4vlan
	down      ip link set down dev sip4
	post-down brctl delbr sip4
	post-down ip link set down dev sip4vlan
	post-down ip link del sip4vlan

allow-hotplug sip4to6
iface sip4to6 inet manual
	pre-up    ifup sip4
	up        brctl addif sip4 sip4to6
	down      brctl delif sip4 sip4to6
	post-down ifdown sip4
```

Note that the bridge `sip4` receives no address, nor does the
VLAN interface `sip4vlan` need it.  The container receives any
queries for the MAC address belonging to its IPv4 address.

You can now continue setting up your phones with (the VLAN and)
the IPv4 address that you configured in this container.  A good
place would be as a proxy address.


## Configuration Details

SIPproxy64 needs some configuration.  You will find what you need
when you start configuring your phones.  We explain what to do in
terms of the use case of IPv4-only SIP Phones and an IPv6 uplink
such as a SIP phone service (like the `contrib/sip-trunk` container).
You may also configure an IPv4-only phone switch.

  * **Internal IPv4 Address** is the IPv4 address to which the
    SIPproxy64 will be listening on its `sip6to4` side of the
    tunnel into your SIP network or VLAN.

    Configuration variable `CONTRIB_SIPPROXY64_V4LISTEN` is the
    IPv4 address allocated for SIPproxy64 and serving as the
    outbound proxy for IPv4 SIP phones in the same network.

    There is no IPv6 counterpart to this setting, because it can
    be derived from the general "mkroot" configuration.

  * **IPv4 Phone Mappings** are the SIP Phones stuck in IPv4 that
    you want to unleash on the IPv6 network.  They are supposed to
    have fixed IPv4 address and port settings, and they map to the
    IPv6 address of this container with another port.

    Configuration variable `CONTRIB_SIPPROXY64_V4ONLY` is a
    list of `v4addr:port=[v6addr]:port` mappings.  Add one such
    string for each phone that you want to map.  If you have a
    phone that uses multiple ports, then add those as independent
    phone mappings.

    *TODO:* The `:port` notation is not supported (yet) in
    SIPproxy64.  It just maps addresses.  Since we assume only
    one address on each end, this implies that only one phone is
    supported (or one switch, or one ...)

  * **IPv6 Telecom Network** is the uplink that is used when phones
    connect to the IPv4 address of the container.  In such cases,
    SIPproxy64 translates the traffic to IPv6 and relays it to this
    default SIP proxy.

    Configuration variable `CONTRIB_SIPPROXY64_V6UPLINK` may be set
    to an IPv6 address.  This is often in another network, but it
    does not have to be.  Remember to open firewalls for any of
    the ports that should be reachable.

  * **IPv4 Phone Switch** can optionally be set to a phone switch
    that serves inside the same IPv4 network alongside SIP phones.
    If traffic is sent to the IPv6 address of this container, then
    SIPproxy64 will translate to IPv4 and relay to this address.

    Configuration variable `CONTRIB_SIPPROXY64_V4UPLINK` may be
    left empty or, if it is used, set to an IPv4 address in the
    same IPv4 network as the IPv4 phones and the `sip4to6`
    interface.

  * **IPv6 Phone Mappings** are not usually needed, but they may
    be useful in the event of IPv6 phones that need to address
    an IPv4 phone switch, or possibly other IPv4 phones.  Even
    if an IPv6-ready phone can usually do IPv4 as well, then it
    may still be desirable to use IPv6 to get to a different
    network dedicated to IPv4 telephony.  You probably don't
    need it; but when you do, it's ready and waiting so you can
    have the smoothest IPv6-transition possible.

    Configuration variable `CONTRIB_SIPPROXY64_V6ONLY` may be
    set to a list of `[v6addr]:port=v4addr:port` mappings.  Add
    one such string for each phone that you want to map.  If you
    have a phone that uses multiple ports, then add those as
    independent phone mappings.

    *TODO:* The `:port` notation is not supported (yet) in
    SIPproxy64.  It just maps addresses.  Since we assume only
    one address on each end, this implies that only one phone is
    supported (or one switch, or one ...)

The SIPproxy64 configuration is generated from these settings,
and will run a simple and rock-solid translation between SIP
over IPv4, sent into `sip4to6`, translated to SIP over IPv6
and passed out via `dmz0` like any other "mkroot" container.

