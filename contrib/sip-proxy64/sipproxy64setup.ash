#!/bin/ash
#
# sipproxy64setup.ash -- Configure a SIPproxy64 process
#
# Save this one man page before it is removed.
#
# From: Rick van Rein <rick@openfortress.nl>


MANPAGE=usr/share/man/man8/sipproxy64.8.gz 

if [ -r $MANPAGE ]
then
	#NO.NROFF# gunzip < $MANPAGE | nroff -man > SIPproxy64.txt
	gunzip < $MANPAGE > SIPproxy64.man
fi

