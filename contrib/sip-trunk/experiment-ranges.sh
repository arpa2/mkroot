#!/bin/bash
#
# Download phone number ranges and map them to ranges for string comparison.
#
# From: Rick van Rein <rick@openfortress.nl>

ZIPFILE=/tmp/nummers.csv.zip
CSVFILE=/tmp/nummers.csv
RNGFILE=/tmp/nummerreeksen.csv

rm -f "$ZIPFILE" "$CSVFILE" "$RNGFILE"

wget -O "$ZIPFILE" https://www.acm.nl/download/registers/nummers_csv.zip

unzip -d $(dirname "$CSVFILE") "$ZIPFILE"

sed -E < "$CSVFILE" > "$RNGFILE" \
	-e 's/^[^;]*;[^;]*;[^;]*;[^;]*;[^;]*;[^;]*;[^;]*;[^;]*;([^;]*);([^;]*);[^;]*;([^;]*);.*$/\2;\1;\3/' \
	-e 's/([0-9])-([0-9])/\1\2/g' \
	-e 's/^0/+31/' \
	-e 's/^([^;]*);0/\1;+31/' \
	-e 's/^([^;]*);([^;]*);(.*)$/"\1","\2","\3"/'

rm "$ZIPFILE" "$CSVFILE"

