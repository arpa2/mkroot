#!/bin/ash
#
# Setup Kamailio databases
#
# This is a FIX_BUILDDIR script because it needs to see the vardir
#
# From: Rick van Rein <rick@openfortress.nl>


# Clone the auth_db to a trunk_auth_db
#
sed 's/\<subscriber/trunkauth/g' \
	< rootfs/usr/share/kamailio/db_sqlite/auth_db-create.sql \
	> rootfs/usr/share/kamailio/db_sqlite/trunk_auth_db-create.sql


# Clone the alias_db to a trunk_alias_db
#
sed 's/\<dbaliases/trunkaliases/g' \
	< rootfs/usr/share/kamailio/db_sqlite/alias_db-create.sql \
	> rootfs/usr/share/kamailio/db_sqlite/trunk_alias_db-create.sql

# Ensure trunk_auth_db among the EXTRA_MODULES
#
grep -q 'trunk_auth_db' rootfs/etc/kamailio/kamctlrc
if [ $? -ne 0 ]
then
	echo 'EXTRA_MODULES="$EXTRA_MODULES trunk_auth_db"' \
		>> rootfs/etc/kamailio/kamctlrc
fi

# Ensure trunk_alias_db among the EXTRA_MODULES
#
grep -q 'trunk_alias_db' rootfs/etc/kamailio/kamctlrc
if [ $? -ne 0 ]
then
	echo 'EXTRA_MODULES="$EXTRA_MODULES trunk_alias_db"' \
		>> rootfs/etc/kamailio/kamctlrc
fi

# Create database, including EXTRA_MODULES
#
if [ ! -r vardir/lib/kamailio/kamailio.sqlite ]
then
	yes | kamdbctl create vardir/lib/kamailio/kamailio.sqlite
fi
