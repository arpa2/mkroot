#!/bin/bash
#
# net4pstn.sh -- hook script for up/down operations
#
# For the proper hooks, see prestart and poststop in
# https://github.com/opencontainers/runtime-spec/blob/master/config.md
#
# A neat document explaining network namespaces is on
# https://blog.scottlowe.org/2013/09/04/introducing-linux-network-namespaces/
#
# From: Rick van Rein <rick@openfortress.nl>


case "$1" in

create)
	#TODO# Throuh documented, the "createContainer" hook does not call here
	# You can call this manually before bringing up the container
	ip netns add @TRUNK_NETNS@
	;;

up)
	IP6PREFIX_DMZ='@IWO_dmz_PREFIX96@'
	IP6PREFIX_CTL='@IWO_ctl_PREFIX96@'
	IP6PREFIX_SVC='@IWO_svc_PREFIX96@'
	IP6PREFIX_BCK='@IWO_bck_PREFIX96@'
	ip link add @TRUNK_DMZ@ @DMZ_BRIDGE_NETNS@ type veth peer name dmz@IWO_SITEINST_ME@ netns @TRUNK_NETNS@ &&
	ip link add @TRUNK_CTL@ @CTL_BRIDGE_NETNS@ type veth peer name ctl@IWO_SITEINST_ME@ netns @TRUNK_NETNS@ &&
	ip link add @TRUNK_SVC@ @SVC_BRIDGE_NETNS@ type veth peer name svc@IWO_SITEINST_ME@ netns @TRUNK_NETNS@ &&
	ip link add @TRUNK_BCK@ @BCK_BRIDGE_NETNS@ type veth peer name bck@IWO_SITEINST_ME@ netns @TRUNK_NETNS@ &&
	ip netns exec @TRUNK_NETNS@ ip addr add ${IP6PREFIX_DMZ%::}:a3:5060/96 dev dmz@IWO_SITEINST_ME@ &&
	ip netns exec @TRUNK_NETNS@ ip addr add ${IP6PREFIX_CTL%::}:a3:5060/96 dev ctl@IWO_SITEINST_ME@ &&
	ip netns exec @TRUNK_NETNS@ ip addr add ${IP6PREFIX_SVC%::}:a3:5060/96 dev svc@IWO_SITEINST_ME@ &&
	ip netns exec @TRUNK_NETNS@ ip addr add ${IP6PREFIX_BCK%::}:a3:5060/96 dev bck@IWO_SITEINST_ME@ &&
	@DMZ_BRIDGE_NETNS_EXEC@ip link set mtu 1492 dev @TRUNK_DMZ@ &&
	@DMZ_BRIDGE_NETNS_EXEC@ip link set up dev @TRUNK_DMZ@ &&
	@CTL_BRIDGE_NETNS_EXEC@ip link set up dev @TRUNK_CTL@ &&
	@SVC_BRIDGE_NETNS_EXEC@ip link set up dev @TRUNK_SVC@ &&
	@BCK_BRIDGE_NETNS_EXEC@ip link set up dev @TRUNK_BCK@ &&
	@DMZ_BRIDGE_NETNS_EXEC@brctl addif @DMZ_BRIDGE@ @TRUNK_DMZ@ &&
	@CTL_BRIDGE_NETNS_EXEC@brctl addif @CTL_BRIDGE@ @TRUNK_CTL@ &&
	@SVC_BRIDGE_NETNS_EXEC@brctl addif @SVC_BRIDGE@ @TRUNK_SVC@ &&
	@BCK_BRIDGE_NETNS_EXEC@brctl addif @BCK_BRIDGE@ @TRUNK_BCK@ &&
	ip netns exec @TRUNK_NETNS@ ip link set up dev dmz@IWO_SITEINST_ME@ &&
	ip netns exec @TRUNK_NETNS@ ip link set up dev ctl@IWO_SITEINST_ME@ &&
	ip netns exec @TRUNK_NETNS@ ip link set up dev svc@IWO_SITEINST_ME@ &&
	ip netns exec @TRUNK_NETNS@ ip link set up dev bck@IWO_SITEINST_ME@ &&
	ip netns exec @TRUNK_NETNS@ ip link set up dev lo   &&
	ip netns exec @TRUNK_NETNS@ ip route add default via @IWO_dmz_OUTROUTE_fe80@ dev dmz@IWO_SITEINST_ME@
	;;

down)
	@DMZ_BRIDGE_NETNS_EXEC@brctl delif @DMZ_BRIDGE@ @TRUNK_DMZ@
	@CTL_BRIDGE_NETNS_EXEC@brctl delif @CTL_BRIDGE@ @TRUNK_CTL@
	@SVC_BRIDGE_NETNS_EXEC@brctl delif @SVC_BRIDGE@ @TRUNK_SVC@
	@BCK_BRIDGE_NETNS_EXEC@brctl delif @BCK_BRIDGE@ @TRUNK_BCK@
	@DMZ_BRIDGE_NETNS_EXEC@ip link del @TRUNK_DMZ@
	@CTL_BRIDGE_NETNS_EXEC@ip link del @TRUNK_CTL@
	@SVC_BRIDGE_NETNS_EXEC@ip link del @TRUNK_SVC@
	@BCK_BRIDGE_NETNS_EXEC@ip link del @TRUNK_BCK@
	;;

destroy)
	# You can call this manually after bringing down the container
	ip netns del @TRUNK_NETNS@
	;;

*)
	echo >&2 "Usage: $0 create|up|down|destroy"
	exit 1
	;;

esac
