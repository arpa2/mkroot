# InternetWide Architecture Contribution: Jitsi Meet.
#
# The constructs a root file system for videoconferencing via
# Jitsi Meet.  Locations for conferencing can be restricted to
# user and group names, and subject to Access Control.  This
# would be automatically configured.
#
# From: Rick van Rein <rick@openfortress.nl>


add_rootfs(jitsi)

rootfs_ospackages(
	busybox
	# findutils
	libc-bin
	# netbase
	# libicu67
	# libdb5.3
	# libssl1.1
	# ssl-cert
	#
	# SASL for mechanisms anywhere
	# sasl2-bin libsasl2-2 libsasl2-modules
	# libsasl2-modules-gssapi-mit libsasl2-modules-otp libsasl2-modules-db
	#
	# OpenJDK 8 or OpenJDK 11
	openjdk-11-jre-headless
	#
	# Nginx forms the web frontend
	nginx-light
	#
	# Control Tower access to the ARPA2 Rules DB
	dropbear libtomcrypt1 libtommath1
	#
	# Debug support
	DEBUG_SUPPORT strace gdb
)

rootfs_packages(
	mkrootutils
	arpa2cm
	#
	# ARPA2 Identity, Rules DB, Communication Access
	arpa2common
	#
	# Quick MEM pools are used for connection storage
	# quickmem
	#
	# Quick DER is used to encode the DiaSASL protocol
	# quickder
	#
	# Quick DiaSASL is used as the C API to DiaSASL
	# quicksasl
	#
	# TLS Pool protects connections in another process
	#NON-ROOT-TODO# Cannot locate pulleybacksimu_tlspool.so
	# tlspool
	#
	# Jitsi Meet comes as "foreign" Debian packages, so we
	# simply download from https:// links and see no extra
	# value in checking the package signatures.  Dynamic
	# setup in "mkhere" might get confused or biased.
	foreigndeb "FLAVOUR=https://download.jitsi.org/stable/jibri_8.0-93-g51fe7a2-1_all.deb https://download.jitsi.org/stable/jicofo_1.0-756-1_all.deb https://download.jitsi.org/stable/jigasi_1.1-38-g8f3c241-1_amd64.deb https://download.jitsi.org/stable/jitsi-meet-prosody_1.0.5056-1_all.deb https://download.jitsi.org/stable/jitsi-meet-tokens_1.0.5056-1_all.deb https://download.jitsi.org/stable/jitsi-meet-turnserver_1.0.5056-1_all.deb https://download.jitsi.org/stable/jitsi-meet-web-config_1.0.5056-1_all.deb https://download.jitsi.org/stable/jitsi-meet-web_1.0.5056-1_all.deb https://download.jitsi.org/stable/jitsi-meet_2.0.5963-1_all.deb https://download.jitsi.org/stable/jitsi-upload-integrations_0.15.15-1_all.deb https://download.jitsi.org/stable/jitsi-videobridge2_2.1-508-gb24f756c-1_all.deb https://download.jitsi.org/stable/jitsi-videobridge_995-1_amd64.deb"
)

rootfs_user (root GROUP wheel
	UID 0 GID 0
	HOME / SHELL /bin/ash
	NOTE "Container Administrator")

rootfs_user (systemd-journal  GROUP systemd-journal  UID 100 GID 100)
rootfs_user (systemd-timesync GROUP systemd-timesync UID 101 GID 101)
rootfs_user (systemd-network  GROUP systemd-network  UID 102 GID 102)
rootfs_user (systemd-resolve  GROUP systemd-resolve  UID 103 GID 103)
rootfs_user (systemd-coredump GROUP systemd-coredump UID 104 GID 104)

rootfs_user (jitsi GROUP jitsi
	UID 2000 GID 2000
	HOME /var/lib/jitsi
	SHELL /usr/sbin/nologin
	NOTE "Jitsi Meet Server")

rootfs_user (nobody  GROUP nobody
	UID 65534 GID 65534
	HOME /nonexistent SHELL /usr/sbin/nologin
	NOTE "Ultimately Powerless User")

rootfs_secret (machine-id BITS 128 SEED "642dcb07-2786-40bc-9dbd-ff3ad55f0405")

rootfs_directories(
	/etc
	/dev
	/bin /sbin /usr/bin /usr/sbin
	/lib /usr/lib
	/tmp
	/proc /sys
	/var/log
	/var/lib
	/run /run/lock
)

rootfs_files(
	# /etc/passwd
	# /etc/shadow
	# /etc/group
	/etc/nsswitch.conf
	/etc/ld.so.conf
	/etc/ld.so.conf.d/*
	README.MD
	etc/inittab
	etc/rcS
	etc/resolv.conf
	etc/hosts
	etc/machine-id
	#TODO# etc/dropbear/cmd_a2rule.ash
)

rootfs_certificate (PEM
	SERVER jitsi0.${IWO_DOMAIN_ISP})

rootfs_certificate (PEM
	SERVER localhost)

# Port 443/tcp for Jitsi Meet [80/tcp is mentioned for Let's Encrypt]
# Port 10000/udp and 5349/tcp for audio/video streaming
# Port 3478/udp for STUN
# https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-quickstart
rootfs_ports (jitsi0
	IFACE_IDENT ::a3:194a
	TCP_IN 443
	TCP 5349
	UDP_IN 3478
	UDP 10000
)

# Best is IPv6; IPv4 requires mappings for all of 443/tcp, 5349/tcp, 3478/udp and 10000/udp
rootfs_dnsrecord ("jitsi0" "AAAA" "${IWO_dmz0_PREFIX96}:a3:194a")
rootfs_dnsrecord ("jitsi0" "A" PORTFWD "${IWO_HOST_jitsi0_TCP_433}" "${IWO_HOST_jitsi0_UDP_10000}" "${IWO_HOST_jitsi0_TCP_5349}" "${IWO_HOST_jitsi0_UDP_3478}")

rootfs_fixups(
	busybox.ash
	stripdev.ash
	ldconfig.ash
	dropbearkey.ash
	actualise.ash
	stripdocs.ash
	# perlpathfix.ash
	# striplocale.ash
	# FIX_BUILDDIR
)

rootfs_downloads(
	# "https://cdn.conversejs.org/7.0.5/dist/converse.min.js"
	# "https://cdn.conversejs.org/7.0.5/dist/converse.min.css"
	# DESTDIR /opt/converse/
)

use_bridge (DMZ_BRIDGE dmz)
use_bridge (BCK_BRIDGE bck)
use_bridge (CTL_BRIDGE ctl)
use_bridge (SVC_BRIDGE svc)

set (JITSI_NETNS ${IWO_SITENAME}.jitsi${IWO_SITEINST_ME})
set (JITSI_DMZ   ${IWO_SITENAME}.jitsi${IWO_SITEINST_ME}.dmz${IWO_SITEINST_ME})
set (JITSI_BCK   ${IWO_SITENAME}.jitsi${IWO_SITEINST_ME}.bck${IWO_SITEINST_ME})
set (JITSI_CTL   ${IWO_SITENAME}.jitsi${IWO_SITEINST_ME}.ctl${IWO_SITEINST_ME})
set (JITSI_SVC   ${IWO_SITENAME}.jitsi${IWO_SITEINST_ME}.svc${IWO_SITEINST_ME})

rootfs_runkind(OCI_bundle
	config.json
	net4jitsi.sh
	# INSTANCES kip kipctl
	# INSTFILES config.json net4kip.sh
)

execute_process (COMMAND ${CMAKE_COMMAND}
		-E make_directory
			"${CMAKE_CURRENT_BINARY_DIR}/vardir"
			"${CMAKE_CURRENT_BINARY_DIR}/vardir/log"
			"${CMAKE_CURRENT_BINARY_DIR}/vardir/tls"
)


