# Central Logfiles

> *The containers in "mkroot" log in a central directory.*

Under the build directory, the `log` subdirectory holds an entry for each
container, basically named like its host.  Note that multiple containers
may be started, such as `zone0` and `name0` for `internetwide/zonemaster`;
these have their own logging directory.

These directories mount in the container as `/var/log` and anything
stored there ends in these directories.  Programs are directed to log
there via either the container's `syslogd` or because they write in their
own subdirectory.

**Backups.**
When [making backups](backups-from-containers.md),
the `/var` directories are usually found, but the `/var/log` is
explicitly exempted from backup.   This is usually a good choice,
because logs can have implications for privacy and security and may
not be good to mix with a container's live data.  It also makes
more sense to have all logfiles together, which is what happens
when making a backup of the entire `log` directory holding all
(desired) hosts.

**Log rotation.**
Some containers build up a lot of logging, and it makes sense
to rotate that.  This is easier to control from the outside,
and may be integrated with the normal `logrotate` cycle, by
programming it into `/etc/logrotate.d/` and signalling the
various containers to switch to the new file (with the old
name) using

```
postrotate
   runc exec <container> /usr/bin/killall -HUP syslogd
endscript
```

to restart the container's `syslogd`.  Note that it is not
a problem when the container is not currently running; a
restart of `syslogd` will make it do what we intended.  Not
missing log cycles on containers that are down is one good
reason to do log rotation outside of the containers.

