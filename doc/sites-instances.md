# Sites and Instances on "mkroot"

> *For optimal reliability, it helps to run a service multiple
> times.  The less overlap, the better.  Different physical
> locations help, but independent management is also useful.*

Reliability can be vital to commercial service providers.
But it may also be helpful if you run services at home,
perhaps on a Raspberry Pi, and want to swap support with a
friend who has another uplink to the Internet.  Your service
could be multi-homed at a very low budget.  And we provide
the structures to do it.

**TODO:** Instances are planned, but not implemented everywhere yet; Routing and DNS does work.


## Concepts: Site and Instance

There are a few concepts that are sort-of implicit, but that can
be great help with reliable services.

*Sites want to be different.  Instances want to be the same.*

Every **Site** is a set of online services managed to appear as
one whole.  Think of *website* if you like.

Are you running a service with users?  Then you might find it
useful to have different sites for `live` and `test`, so
you can safely fiddle with the latter and, when it finally works,
promote the setup to the `live` environment.

    Note: Site names should not exceed 4 characters because of
    a length constraint on interface names in Linux.

Do you want to develop on the "mkroot" software?  Then you
could consider to define a `developer` site.

Do you want to give demonstrations of Realm Crossover?  Then
you might define sites for `desktop`, `clidom`, `srvdom` or,
if you follow our naming habits, `

Every **Instance** of a Site runs a copy of the service, but
with its own address range.  You could run two mail servers and,
provided they are both configured with MX records in DNS,
would be alternative inputs to send you email.

As a general principe, instance number 0 has the highest priority
in places where this can be configured (as is the case in MX
records but not in AAAA records, for instance).


## Configuration of Sites and Instances

Instances of the same Site each have their own build directory,
but their configuration must overlap:

  * `IWO_SCRAMBLER` must be the same, because it generates the concealed
    addresses as well as the base secrets stored in
  * `IWO_DOMAIN_ISP` delivers the same domain name (TODO: what about backups?)
  * `IWO_SITENAME` must be the same (TODO: nobody publishes these, so why?)
  * `IWO_SITEINST_PREFIXES` must be setup with the prefixes of all
    Instances of the Site, separated by semicolons.  Every Instance must use
    the same order so the numbering of Instances will be the same everywhere,
    as well as derived host/bridge numbering and link-level addresses.

    *Tip:* If something between semicolons is obviously not an IPv6 address,
    then it will be skipped silently.  Instance numbering increments, but
    no actual instance is assumed.  You can use this to remove Instances
    from a Site without disturbing numbering everywhere.
  * `${CMAKE_BINARY_DIR}/.base_secret`, so the file `.base_secret` under
    the build directory.
  * `${CMAKE_BINARY_DIR}/ca` may or may not be shared; if not, be sure
    to setup `TLS_ROOTCERT_FILES` with references to other CA roots.
    They must have the same list of addresses, in the same order.
  * They must use the same numbers for the same interfaces and hosts.
    This makes network addresses in different instances match.


## Running under Sites and Instances

Sites are shown as prefixes to container names, interface names and
network namespaces.
Instances add a number to hostnames and interface names.

  * The same machine can be home to bridges `live.dmz0` and `test.dmz0`
    to keep the Sites separated.

    *Tip:* Use short names to avoid running into name size restrictions.
  * The same machine can be home to containers `live.id0` and `test.id0`
    to keep the Sites separated.  They would both be named `id0` inside
    the container, and know nothing about other Sites.
  * The same machine can be home to network namespaces `live.id0` and
    `test.id0` to keep their respective `id0` containers separate.
  * The same machine may be home to bridges `live.dmz0` and `live.dmz1`
    so the Instances of the `live` Site are separately named.  They may
    however be considered part of the same Site, and will all show up.
  * The same machine may be home to containers `live.id0` and `live.id1`
    to keep the Instances of the `live` Site separately named.  They
    are internally named `id0` and `id1` so they are well aware of their
    distinct Instances.  Their network interfaces would also be named
    differently, namely`dmz0` and `dmz1`.
  * The same machine may be home to network namespaces `live.id0` and
    `live.id1` to keep the Instances of the `live` Site separately
    networked.
  * It is possible for a crossover machine to connect to `svc` and
    `bck` bridges from other sites.  As an example, `bck0` and `bck1`
    would link to Instance 0 and 1 of the same Site.  This is not
    supported for `dmz` and `ctl` interfaces, which are always local
    to the Instance of a Site.


## Mixing Sites on one Machine

You cannot mix Sites.  Their interfaces, containers and network namespaces
should not connect, except when they use each other's service.  This is
considered crossing over to a separate administrative domain.

Note that Realm Crossover, the big idea in the InternetWide Architecture,
enables you to connect easily to foreign services, and other Sites are
just examples of that use case.

Given that they don't mix at all, it is perfectly transparant to run
different Sites on the same machine.  You supply a different /96 prefix
to each of them, and plug them into any uplink bridge, be it the same or
a different one.  They will run side to side without interference.
Just what you need to have `live` and `test` roll-outs!

Another use of running independent sites is to help a friend control
a backup Site on your machine, without interference from your changes
to your own Site.


## Mixing Instances on one Machine

It can be helpful to run multiple instances on the same machine.  Each
would have its own /96 prefix, but they could be backups that perhaps
store data differently, or use another uplink if your machine is
multi-homed (which may be possible in data centers).

Even when they are run on different machines, it can make sense to
connect the `bck` and `svc` bridges between Instances of the same
Site.  This might be done with a tunnel, with or without encryption.
A remote `mkroot.bck0` and `mkroot.svc0` might then appear next to
your local `mkroot.bck1`, `mkroot.svc1`, `mkroot.dmz1` and
`mkroot.ctl1`.  The latter two should not be mirrorred.

Note that every host is aware of its instance number and, where possible,
will first try to reach remotes over the same instance number before
falling back on others.  This means that traffic is kept local when it
can be, and made remote when it must be.  Your performance may degrade
but your service would not break.  This kind of crossover between
mutually dependent systems is a standard recipe for resilient network
services.


## Distributing Instances across Machines

Sites can be trivially distributed across Machines, because they are
independent.  Instances are different, they may like to be connected.

A facility that we are looking into for this is a VxLAN or Virtually
eXtended LAN interface.  This is designed to efficiently mimic a
local network with distributed endpoints.  It has trickery to learn
whole sets of MACs that are at home at other nodes, so it knows how
to relay things without broadcasting to all nodes attached.

Properties required here are encryption and authentication (especially
for the trusted `bck` and `svc` networks).  We may well discover that
secure point-to-point links like WireGuard are easier or better.  A
good key sharing mechanism between Instances of the same Site can be
automated in "mkroot".  Your ideas and experience are welcomed.


## Multi-Homing

Multi-homing is the property of an Instance to be visible over multiple
addresses.  The ability of remote Instances is more general and it may
not help to add this layer of complexity.  On the other hand, it is
not completely dissimilar to servers that have interfaces for more than
one instance.

This *would* give a purpose for having `dmz0` as well as `dmz1` on an
Instance, which would be silly otherwise.  We will keep an open eye on
multi-homing, but as yet are not planning to integrate it.  Talk to us
if you would like to show us that this is a mistake.


