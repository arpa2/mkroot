# Making Backups from Open Containers

> *Extraction of backup files is easy to automate.*

The various containers define a `config.json` file that
defines mount points.  Many of these are uninteresting
to backup, because they are generated and therefore
contain program code and generated configuration data.
Others contain user data collected over time.

## Configuration Backups

Configuration information that you will want to backup
is mostly contained in `CMakeCache.txt` in your "mkroot"
build directory.

A useful idea might be to also backup the Git location
for your build tree, and/or any local changes.  This
can be obtained with commands in your "mkhere" source
directory such as

```
# The branch on which we are working
git branch

# The first log entry, including commit hash
git log -n 1 HEAD

# Local changes, both added and not, reletive the this commit
git diff HEAD
```

## User Data Backups

These reside in directories mounted with `"rw"` and not
of type `"tmpfs"`.  Note that this usually includes log
files and may include private files, so be mindful about
security and privacy.

The tool `jq` is one way of extracting this data on a
command line, with

```
jq '
	.hostname as $host |
	.mounts |
	map (select (.options != null)) |
	map (select (.type != "tmpfs")) |
	map (select (.type != "devpts")) |
	map (select (.destination != "/var/log")) |
	map (select (.options |
		map (. == "rw") |
		any)) |
	map (select (.options |
		map (. == "bind") |
		any)) |
	map ( {"hostname":$host, "mountdir":.destination, "metaldir":.source} )
' config.json
```

or, if you want a raw table to work with,

```
jq -r '
	.hostname as $host |
	.mounts |
	map (select (.options != null)) |
	map (select (.type != "tmpfs")) |
	map (select (.type != "devpts")) |
	map (select (.destination != "/var/log")) |
	map (select (.options |
		map (. == "rw") |
		any)) |
	map (select (.options |
		map (. == "bind") |
		any)) |
        map ( [ $host, .destination, .source] | join ("\t") ) |
	join ("\n")
' config.json
```

You could run this to backup an individual host, or you might
start from your "mkroot" build directory to crawl for the
files with

You could run this to backup an individual host, or you might
start from your "mkroot" build directory to crawl for the
files with

```
jq ...FILTER... $(find . -name config.json)
```

The output for `jq` on each individual file looks like an
array of objects that mention the host name and the directories
on the metal/host and inside container, like

```
[
  {
    "hostname": "mail0",
    "mountdir": "/var",
    "metaldir": "/my/mkroot-build/internetwide/mail/vardir"
  },
  {
    "hostname": "mail0",
    "mountdir": "/etc/postfix",
    "metaldir": "/my/mkroot-build/internetwide/mail/rootfs/etc/postfix"
  }
]
[
  {
    "hostname": "chat0",
    "mountdir": "/var",
    "metaldir": "/my/mkroot-build/internetwide/chat/vardir"
  },
  {
    "hostname": "chat0",
    "mountdir": "/var/www/converse",
    "metaldir": "/my/mkroot-build/internetwide/web/rootfs/var/www/econono.nl/converse/"
  }
]
```

This is not proper JSON, because there is no comma between the two lists.
You might want to use the table format instead, which uses newlines and tabs
as separators and produces

```
mail0	/var	/my/mkroot-build/internetwide/mail/vardir
mail0	/etc/postfix	/my/mkroot-build/internetwide/mail/rootfs/etc/postfix
chat0	/var	/my/mkroot-build/internetwide/chat/vardir
chat0	/var/www/converse	/my/mkroot-build/internetwide/web/rootfs/var/www/econono.nl/converse/
```

This should be relatively easy to turn into a tarball, or whichever
format you want.  The benefit of deriving this directly from the
`config.json` files is that it follows changes in "mkroot" without
further action.  Do keep an old version is safe storage when you
upgrade your "mkroot" environment, so you do not loose any.


## Tell us what you do!

This is a first stab at backup processing.  It would be interesting to learn
about more clever ways of doing this.  When they are generally useful, please
share your tested approach via an Issue or as a Merge Request to this file!
