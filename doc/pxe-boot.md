# PXE Booting from a "mkroot" RootFS

> *The idea behind "mkroot" is that it produces a form that is
> widely useful; we describe how to use it with PXE.*

There is not much PXELINUX-specific about the installation,
except for the output targeting the simple menu system.

## CMake Variables

There are a few general settings of interest to PXE setup,
namely

  * `PXELINUX_TFTP_ROOTDIR` is the root directory of your TFTP
    server, as seen by the building platform.  It defaults to
    `/tftpboot` and must not end in a slash.  This is where the
    "mkroot" files are installed.

  * `PXELINUX_TFTP_SUBDIR` is the subdirectory underneath the
    TFTP server root directory that stores the output from the
    current "mkroot" build tree.  You might have variations for
    different build trees (for different platforms, perhaps)
    but the default is just `mkroot` and it must neither start
    nor end with a slash.

The installation is done in the subdirectory plus the path of
the "mkroot" component.

## Support in CMakeLists.txt

There are a few formulations that are needed in a "mkroot"
component's CMakeLists.txt file.

**Kernel.** You will need a kernel and usually some modules:

```
rootfs_kernel (
	psmouse
	serport serio_raw
)
```

You should not forget to run `depmod -a` on the result, for
which a generic fixup script is available:

```
rootfs_fixups (
	depmod.ash
)
```

**Runkind.** You need to generate the PXE specifics in a Runkind:

```
rootfs_runkind (PXE
	install-pxelinux.sh
)
```

An example of the latter script as a `install-pxelinux.sh.in` file
is

```
#!/bin/sh

TFTPDIR=@PXELINUX_TFTP_ROOTDIR@/@PXELINUX_TFTP_SUBDIR@/demo/twin-tty

mkdir -p "$TFTPDIR"

( cd rootfs ; find * | cpio --quiet -H newc -o ) | gzip -9 -n > "$TFTPDIR/initrd.gz"

cp rootfs/boot/vmlinuz* "$TFTPDIR/vmlinuz"

cat <<-EOF >"$TFTPDIR/index.menu"

        MENU BEGIN twin-tty

        MENU TITLE TWINtty :- TextWINdows work environment

        MENU SAVE

        TEXT HELP
        The TWIN user interface offers a character-based "graphical" interface
        for managing terminals that run textual applications.  Included here
        are SSH, MOSH, W3M and F-IRC, as well as MINICOM and LRZSZ.
        ENDTEXT

        LABEL lores
         MENU LABEL Safe default resolution
         LINUX ::@PXELINUX_TFTP_SUBDIR@/demo/twin-tty/vmlinuz
         APPEND initrd=::@PXELINUX_TFTP_SUBDIR@/demo/twin-tty/initrd.gz

        LABEL 1280x1024
         MENU LABEL VGA resolution 1280x1024
         LINUX ::@PXELINUX_TFTP_SUBDIR@/demo/twin-tty/vmlinuz
         APPEND initrd=::@PXELINUX_TFTP_SUBDIR@/demo/twin-tty/initrd.gz vga=0x328

        MENU END

        EOF
```

The generated `index.menu` file can be loaded in your PXELINUX configuration,
usually in `pxelinux.cfg/default`,

```
INCLUDE ::path/to/index.menu
```

for instance, with the working example for the default settings,

```
INCLUDE ::mkroot/demo/twin-tty/index.menu
```

This will show up as a submenu that is entered upon... enter.

**Terminals.** If you want to show output, you need a few terminfo files,

```
rootfs_terminfo (
	ansi
	linux
	xterm*
	vt*
)
```

It is possible to also add a convenient interface for
[Textual WINdowing](https://github.com/cosmos72/twin/)
which can be built up to something that
[looks rather nice](https://a.fsdn.com/con/app/proj/twin/screenshots/321949.jpg/max/max/1),

```
rootfs_ospackages (
	gpm
	dialog
)

rootfs_packages (
	twin VERSION=v0.8.1
)
```

## Bootstrapping Code

In an Open Container, a networking script takes care of configuration, as a hook
from the `config.json` file.  This is not the case under PXE.  However, once a
kernel has setup its initial ramfs, it will run the `/init` program in it.  This
is usually a script that sets up the system and ends up calling `exec /bin/init`
or something along those lines.


## Working Example

Have a look at `demo/twin-tty` for a working demonstration of PXE generation
and booting.


## More to Come

There are a few more plans with this:

  * Kernels are currently stored under `rootfs/boot` and packaged along with
    the rootfs.  This is wasteful.

  * Kernel modules are current stored under `rootfs/lib/modules` where they
    can also be seen during other uses.  If this is ever considered harmful,
    we will exploit the idea that a `cpio` and even a `cpio.gz` format offers
    concatenation (after peeling off the `cpio` trailer, like it is done in
    "mkhere" too).  The kernel modules (and the kernel) then end up in a
    separate `kernfs` or something along those lines.

  * There is no support for mounting external file systems.  This limits the
    use cases, and several options can be considered to more in line with
    what `config.json` does for Open Containers.

  * When bridges are declared, they should translate to a VLAN on top of the
    primary interface.  Default VLAN numbers can be derived when bridges are
    declared as part of the "mkroot" setup for InternetWide.

