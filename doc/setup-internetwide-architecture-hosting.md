# Setup the InternetWide Architecture on your Hosting Platform

> *The InternetWide Architecture defines new Internet technology that
> is implemented in specific ARPA2 projects.  Together, these form a
> domain/user hosting platform.  It can run for bulk providers, but
> parts or all of it can also be privately run.*

**This document gets you started running the platform:**

  - It explains requirements for running the system
  - It gives some guidance to your public network interfacing
  - It explains how to build software packages
  - It explains how to build open containers
  - It explains how to setup the internal network
  - It explains how to setup the containers

Our primary target platform is Debian Linux, and we track its stable
releases as we go.  We stay general as much as possible, but there
may be parts where you need to patch for other choices.  Your merge
requests may help us improve this software and if they are generally
useful they would not be a concern for you in the future.


## End Result

After these instructions, you will have a platform for hosting the
InternetWide Architecture.  You can run service containers in a
virtual network like this one:

![Setup with Router and Nginx Container](images/setup-hosting-network.png)

**Quick tour:**

  * Arrows indicate the initiative of traffic flow.
  * Your host has a network interface, named `eth0` in this.
  * Yellow rounded rectangles are bridges that interconnect containers.
  * The dashed gray boxes are containers named by their build path.
  * Purple rectangles are bits of software inside a container.

**Outline:**

You shall have to guide traffic from the `eth0` interface to the
`iwo0pub` interface.  We provide suggestions below, but what works
depends on your hosting environment.  In general, you guide some IPv6
and IPv4 traffic into the `iwo0pub` interface, from where the
containers pickup.

The task of `internetwide/pub2dmz` is to translate all traffic into
IPv6, so the containers only need to be configured for that address
family, *and* so they are not hampered by the limited space in the
IPv4 protocol.  Thanks to `pub2dmz`, visitors on IPv4 will never
notice; they pass through NAT64 and port forwarding, all static
translations.  Only for peer-to-peer behaviour is 6bed4 a better
choice, because it supports the address transparency of IPv6 and
does not translate addresses and ports.


## Building with "mkhere" and "mkroot"

We deliver root file systems for containers.  Alongside we can generate
configuration files, which we currently mostly do for the
[Open Container Initiative](https://opencontainers.org/).
This should allow you to run and manage containers with
[RunC](https://github.com/opencontainers/runc)
or any other/future/higher implementation of the standard.
Merge requests with integration code and documentation are welcomed.

Root file systems can also be used for PXE and ISOLINUX, which can be
used for booting a container over a network, from USB or CDROM.  For
these, annotation scripts can easily be required.  Finally, NFS exports
may be useful for systems that boot without local storage, including
a virtualisation system.  We know this works in theory, but would love
to receive merge requests including documentation to make it practical.

We have found ZFS to be a pleasant place for the various "mkhere" and
"mkroot" file systems, but will show `/opt` in stored examples below.
ZFS has builtin support for file system export over NFS, and it is
good at snapshotting versions that run in various places, perhaps
while building on the next version on the live file system.  Most
containers have a variable directory to carry data across runs.

This may help to explain why we focus on building root file systems
with various forms of descriptive metadata to go along with them.
The "mkroot" system builds such root file systems.  The layer below
this is "mkhere", which installs and builds our software and
dependencies in its own environment; this may involve crossover to
a foreign architecture; this enables such things as easy building
for Raspberry Pi or 32-bit Intel.


## Server Requirements for the InternetWide Architecture

To run components from the InternetWide Architecture, you need:

  * An IPv6 prefix of /96 or shorter, with global routing (tunnels no problem)
  * An IPv4 address or subnet (backward compatibility to IPv4-only visitors)
  * For now, Linux is required to run it; we develop on Debian stable

The metal host makes extensive use of this software:

  * The [iproute2](https://www.lartc.org/howto/lartc.iproute2.html) and [brctl](https://linux.die.net/man/8/brctl) and [nftables](https://netfilter.org/projects/nftables/index.html) packages
  * The [RunC](https://github.com/opencontainers/runc) container software
  * Portable meta-maker [cmake](https://cmake.org/cmake/help/v3.13/manual/cmake.1.html) and its [TUI](https://cmake.org/cmake/help/v3.13/manual/ccmake.1.html) or [GUI](https://cmake.org/cmake/help/v3.13/manual/cmake-gui.1.html)
  * Advanced [bash](https://tiswww.case.edu/php/chet/bash/bashtop.html) scripting

Note how the tooling is quite portable; the parts that currently tie us to Linux
are fairly well-known and we are interested in patches to relax this:

  * The networking scripts rely on advanced `iproute2` and `brctl` functionality
      - One might simulate bridges with VLANs between simulated hardware machines
      - Firewall scripts usually have an analogon on other platforms
      - Tunnel devices are quite portable; they might simulate special-purpose tunnels

  * The containers run with RunC
      - Root file systems plus mounts are also useful for PXE, ISOLINUX and NFS
      - RunC is just a first stab at the [movement towads Open Containers](https://opencontainers.org/)
      - Simulated hardware may treat it as a disk, perhaps over NFS or PXE or ISOLINUX
      - The [Windows Container Platform](https://docs.microsoft.com/en-us/virtualization/windowscontainers/deploy-containers/containerd) has a similar tool `runhcs`
      - Root file systems can import to Docker, which runs in most places
      - Consoles or displays are usually available for simulated hardware

  * The software is often built for the Linux kernel
      - We use Busybox as a compact convenience that focusses us on container modesty
      - The "mkhere" system is designed to support emulation layers

  * We currently develop for Debian stable
      - The "mkhere" system abstracts away the operating system
      - Your operating system may only need package name translation

  * We are not focussing on compact images
      - The "mkhere" system prepares variants and flavours to reduce packages
      - The "mkroot" system can specify versions, variants and flavours
      - For continued development, the focus should not be compactness
      - For deployment, the focus on compactness is supported


## Setup "mkhere" and Build Packages

[https://gitlab.com/arpa2/mkhere](https://gitlab.com/arpa2/mkhere)

Your first step is to setup a "mkhere" environment.  You should consider
a separate one for each platform for which you intend to build the
software.  This would be your *target platform*, which is basically a
distribution installed as a root filesystem in a subdirectory of the
*build platform*.

**Build platform:**

  * Developed on Debian Linux, suite stable, architecture x86_64
  * Requires `bash` with a few common shell utilities
  * Uses `chroot` to switch to the target platform

**Target platform:**

  * Developed for Debian Linux, suite stable, architecture x86_64
  * Benefits from `qemu-user-static` and `qemu-user-binfmt` for crossover
  * No cross-compilers but actual target emulation
  * Tends to be stable for other Debian Linux architectures
  * Regularly tried for 32-bit Intel and Raspbian armv7l
  * Initial successes with Alpine Linux
  * Going beyond `chroot` is open for future BSD and Windows support

To setup "mkhere", first setup your target operating system

```
#CROSSOVER# apt-get install qemu-user-static qemu-user-binfmt
mkdir /opt/tgt123
cdebootstrap --flavour=build --arch=tgt123 /opt/tgt123
```

You should now be able to enter it with `chroot /opt/tgt123` and play around.
If this works, continue to setup the "mkhere" build system,

```
git clone https://gitlab.com/arpa2/mkhere.git /opt/tgt123
/opt/tgt123/mkhere/ospackages.sh ossetup
```

You now have a basic setup for building software.

The further interaction happens over scripts that each represent a
single software package.  The usualy build instructions are drilled
down to retrieval, build and extraction.  The latter is new; the
"mkhere" environment is just for building but may be too limited to
run the software.  You would export a file hierarchy to addto a
root directory on the target.

A standard flow including all the necessary steps in a single command
would be:

```
/opt/tgt123/mkhere/6bed4.sh update depend osdepend touch build list
```

This forces fresh download and build; if you want to skip double
work you would instead do

```
/opt/tgt123/mkhere/6bed4.sh have depend osdepend build list
```

The `list` command extracts just the file names that would be
exported; you can actually export with `cpio` or `cpiogz` or `tar`
or `targz`; or you could install in the calling directory (so not
on the build system!) with `install`.

You should now be able to build more of the packages, and you will
find some that we built and others that we felt like adding.

You can include a symbolic link to the "mkhere" system from anywhere
you need it, and then follow the link to build and install the
exports:

```
mkdir /tmp/mybuild
cd    /tmp/mybuild
ln -s /opt/tgt123/mkhere
./mkhere/6bed4.sh have depend osdepend build cpio oslibcpio install
```

This is enough to install the work in the current directory.
It includes any libraries referenced from the operating system,
which means that you have referential integrity &mdash; except for
plugin libaries.

You can also extract operating system packages, albeit without any
generated files or package management data,

```
FLAVOUR_ospackages=bash ./mkhere/ospackages.sh list
```

Now, as a preview to the "mkroot" system, try this:

```
mkdir internetwide/pub2dmz
cd    internetwide/pub2dmz
../../mkhere/6bed4.sh have depend osdepend build install
```

You just installed the `6bed4` package installed in the subdirectory.
At its heart, this is how "mkroot" builds on top of "mkhere".

If you want to support more than one target platform, you can
create a separate root directory for each, with a separate
checkout of "mkhere" on each.  The trick of "mkhere" is that
it links to a complete build environment which acts like the
target as a build environment.  You will never even see if you
crossed over to a funny target architecture!  All you might
detect is a slow-down caused by emulation.


## Setup "mkroot" and Build Root Filesystems

[https://gitlab.com/arpa2/mkroot](https://gitlab.com/arpa2/mkroot)

Given one or more "mkhere" target build systems, you can now use
the next layer, and start building containers or, more precisely,
root filesystems with annotation files.

One "mkhere" system can have build directories for various target
platforms, each referencing their own "mkhere" system.  The build
directories then end up each containing their own hierarchy.

**Properties:**

  * The "mkroot" system is built with `cmake` and `bash`
  * A build directory should have a "mkhere" symbolic link
  * Experimental support for bootstrapping a "mkhere" directory exists (ill-advised)
  * Use a configuration front-end to `cmake` (like `ccmake` on a terminal)
  * Configure the desired network setup before generating rootfs
  * Adds and overwrites data when rerun
  * Skips redundant steps when it can
  * Good support for minimised root file systems

Basically, to checkout a build environment:

```
mkdir /opt/mkroot
git clone https://gitlab.com/arpa2/mkroot.git /opt/mkroot
cd /opt/mkroot
mkdir build.tgt123
cd    build.tgt123
ln -s /opt/tgt123/mkhere
ccmake ..
make -C internetwide/pub2dmz
```

This builds the container `internetwide/pub2dmz` that translates public IPv4 and
IPv6 traffic into the
[IPv6-only internal network](http://internetwide.org/blog/2020/05/31/net-1-ipv4-backward-compat.html)
assumed by our components.  It uses `tayga` for NAT64 in support of IPv4-only
clients and `6bed4` to additionally support peer-to-peer interactions (which tend
to benefit from IPv6).  On top of that, it sets up configuration files that
adhere to your local configuration.

A simply component running on this system would be a webserver, though that
requires an explicit enabling flag:

```
cd /opt/mkroot/build.tgt123
cmake -D CONTRIB_WEB_NGINX=ON .
make -C contrib/web-nginx
```

The following assumes you have built these two components and explains how to
run them on your system.


## Network Configuration for the InternetWide Architecture

The first thing to do is to setup a network.  This requires a series of bridges
in the metal host.  Containers connect to those that they need through the
network scripts:

  * `mkroot.pub` is the interface to the metal host.  The /96 prefix IPv6 and
     any number of IPv4 addresses are routed into this interface.  This is
     the least defined part of this instruction, because not all environments
     are equal.  We provide some hints below.

  * `mkroot.dmz` is an internal bridge to which public services attach; they use
    only IPv6 for reasons of simplicity without being constrained by the
    limited numbering spaces of IPv4; the `internetwide/pub2dmz` container makes
    the network IPv6-only, using translation for IPv4 clients.  Addresses on
    this bus are part of the /96 with static mappings to IPv4, so these can
    appear in DNS as A and AAAA records.

  * `mkroot.svc` is an internal star-shaped network, simulated through a bridge
    with firewall rules; the `internetwide/lanservice` offers services like
    DNS that cannot be overtaken by other containers; addresses are not publicly
    routable.

  * `mkroot.bck` is a bridge that serves as interconnect between containers;
    addresses are not publicly routable.

  * `mkroot.ctl` is a star-shaped bridge for control and monitoring; addresses
    are not publicly routable.

  * `mkroot.adm` will probably allow administrative access.

  * `mkroot.ngb` will probably connect same services across sites.

  * `mkroot.sit` will probably connect to remote sites.

  * `mkroot.plg` will probably connect to plugin services from other parties.

You should setup these bridges in your hosting environment.  Individual
containers define interfaces with names like `dmz0` that pair with an
interface named with the component in shorthand, such as `id0dmz` for
the `internetwide/identity` component's plugin to `iwo0dmz`.

**Further reading:**

  * Details of [pub2dmz routing](http://internetwide.org/blog/2020/05/31/net-1-ipv4-backward-compat.html) between `iwo0pub` and `iwo0dmz`.
  * Details of [6bed4 peering](http://internetwide.org/blog/2020/06/26/net-2-6bed4-routing.html).


### Witchcraft without Subnets

If you have a subnet that you can dedicate to the InternetWide Architecture,
then you can simply route it into the `iwo0pub` interface and you should
be done.  If you are in a less fortunate position, for IPv4 and/or IPv6,
you may need to be a bit clever.  We'll try to get you going, though...

For IPv6 we really assume the /96 prefix size, and even a /64 is easy to get.
You would be able to split off one of more /96 prefixes
to one or more sites.  For IPv4 you would get by with a single one, though
with fairly limited port forwarding options.

If you need addresses for your own use, you might consider binding to the
`iwo0dmz` bridge yourself.  You could add an address to this bridge or create
your own `ip link add your0dmz type veth peer name your0` virtual ethernet
pair, and `brctl addif iwo0dmz your0dmz` to link it into the bridge.  You
can then `ip add addr ... dev your0` like you normally would have with
your external interface.  An example is the `dev0dmz` interface for which
we generate a recipe that can be brought up when administration calls for
it.

You can now pass your entire network into `iwo0dmz`, and a common technique
for doing that is to create a bridge into which you shift your interface
`iwo0pub` along with the physical interface.  You do not set an address
on this bridge or on its constituent interfaces.  If you do this remotely,
please be sure that you can recover -- even if just by running `shutdown -r +15`
in the background for automatic reboot in 15 minutes and `shutdown -c`
to stop this after success.  Most remote hosting providers support an
automated reset option, however.

In support of routing setups, we number devices in IPv4 and IPv6 as follows:

  * `${PREFIX96}::1/128` or `10.64.64.1` for the metal side physical interface
  * `${PREFIX96}::2/126` or `10.64.64.2` for the metal side interface `iwo0pub`
  * `${PREFIX96}::3/126` or `10.64.64.3` for its `pub2dmz` container conterpart `pub0`
  * `${PREFIX96}::4/96` and no IPv4 address for `pub2dmz` container interface `dmz0`
  * `${PREFIX96}::/64` for the `iwo0dmz`

Note how the `/126` prefixes are more specific than `/96`, causing routes to these
addresses to move to the outside, and the remainder of the prefix to move towards
`iwo0dmz`.  Default routes from containers are directed to `${PREFIX96}::/96` and
then moved out via `pub0` unless 6bed4 or NAT64 lures the destination address
into their nets and relays it as IPv4, which then also move out over `pub0`.


### Representing IPv4 addresses as IPv6

The `internetwide/pub2dmz` container uses NAT64 to translate IPv4 addresses to
IPv6.  For this action, all IPv4 that enters is routed to address `10.64.64.64`
of a tunnel implemented by [Tayga](http://www.litech.org/tayga/).  The IPv6
side represents the same addresses with 96 bits prefixed, namely `64:ff9b::/96`
as is standardised.  You can treat these as normaly IPv6 addresses.

Following Tayga, we have NAPT to translate destination addresses and ports and
map them onto IPv6 counterparts.  It sounds funny, NAPT in IPv6, but remember
that the only thing changed is the look of the addresses, they have not become
more abundent.  In the vast IPv6 space there are stil very few IPv4 addresses
that can be used, and so we still need to do port forwarding.  On the sunny
side however: the translation is static and so you should not fear for timeouts
or other NAPT trouble.  Reaching out to IPv4 is a different matter, however:
it requires dynamic mapping and so... timeouts.

How does an IPv6 host get the idea to connect to an IPv4 service port?  This
is what DNS64 arranges, and this comes integrated into `internetwide/lanservice`.
After validating DNSSEC, it rewrites A records in DNS to AAAA records.  It will
of course prefer AAAA records when they are available.  Please understand that
this can only be as sound as your IPv6 routing.  We really do rely on IPv6,
and a poor tunnel will reflect on your server stability.  Go for native or
something that is closely compitable; avoid Teredo and 6to4 for that matter;
but 6in4 and 6rd tend to be quite solid tunnel options.


### Recipes for Creating your Bridges

The Debian style for bridge construction centers around `/etc/network/interfaces`
and its `.d` variants.  The corresponding file is written to the build
output file `internetwide/pub2dmz/debian.interface`.

The input to this file is part of your configuration, below.  This involves
setting up your /96 prefix for IPv6, a forwarded subnet (possibly a /32) for
IPv4 and a scramler that is used to generate random prefixes for internal
bridges, to make it more difficult for attackers to guess their addresses.

A place to get free IPv6 tunnel with a /64 prefix is
[Hurricane Electric's tunnel broker service](https://tunnelbroker.net/);
and this should work fine if you route it into your "mkroot" configuration.

You should therefore pick a scrambler that you can reproduce, and store it
in a moderately safe place; this is obfuscation, no strong security, but it
comes for free.  It is also useful because it avoids clashes when you decide
to connect to other sites and then decide to route these bridges.

This pattern can be easily extended to accommodate other systems.  We warmly
welcome your merge requests!


## Server Configuration for the InternetWide Architecture

This is how to setup the "mkroot" structures for your server:

```
git clone https://gitlab.com/arpa2/mkroot.git /opt/mkroot
cd /opt/mkroot
mkdir build
cd build
ccmake .. || cmake-gui ..
```

You should now `C`onfigure and you will be told that you forgot to setup
the "mkhere" link.  Oops.  `E`xit then `Q`uit and:

```
ln -s /opt/tgt123/mkhere
ccmake .. || cmake-gui ..
```

Again, `C`onfigure.  You will be informed that a new `IWO_SCRAMBLER` was
generated at random, but you can set it to another string now if you want.
This is used to scatter randomness, but it has no security implications.
You may have to reproduce it later if you want to reproduce addresses.

In general, you will be asked in a number of `C`onfigure iterations to
build up some information.  Some settings of interest are:

  * `IWO_PUBLIC_PREFIX96` is the /96 prefix in IPv6 that you reserved for
    the InternetWide Architecture.  Do not add `/96` at the end, but simply
    end in `::` so we can attach the remainder in our scripts.  When you
    setup a fresh site, it will be set to `2001:db8:9:10::` under the test
    prefix `2001:db8::/32`, but this prefix will not be routed globally.
  * `IWO_PUBLIC_BRIDGE` is a bridge into which the "mkroot" interface will plug
    its public router interface.  It is assumed to have the uplink route,
    covering at least the /96 prefix setup under `IWO_PUBLIC_PREFIX96`.
  * `IWO_PUBLIC_IPV4_SUBNET` is the IPv4 subnet, with `/dd` size attached,
    that is routed into the `iwo0pub` bridge.  This space is no longer
    available on your interface, except through the extensions suggested
    before.  You might get away with redirecting only certain traffic in here.
  * `IWO_SCRAMBLER` helps to scatter the random prefixes assigned to internal
    networks.  These prefixes look like `fdXX:XXXX:XXXX:XXXX:XXXX:XXXX::/96` and they
    may help to avoid clashes when you connect sites, as well as guesses by
    an attacker if one breaks into a front door and wants to continue to your
    backend.  Set the same scrambler and receive the same addresses, is
    pretty much the idea.
  * `IWO_DOMAIN_ISP` is the domain of the InternetWide Service Provider,
    which defaults to `example.com` until you specify your own domain.
    (You may need to cleanup your CA root certificate after changes.)
    If all you want is to self-host a single domain name, then you can use
    this single fixed setting.  Extended uses with bulk domain handling and
    virtual hosting with dynamic updates generally rely on this domain
    being the static part; in that case it really functions as the domain
    of an InternetWide Service Provider.
  * `IWO_SITENAME` defaults to `iwo0` which you should not change for now.
    It may be useful in future releases when we start
    [distinguishing sites](naming-numbering.md),
    and generate interface names accordingly.
  * `IWO_6BED4_PREFIX` is provided to external 6bed4 clients; the default
    setting `fc64::/16` allows them to access your network as well as to
    connect directly, but you cannot providing IPv6 native routing to them.
    You cannot simply use your /64 for it if you also use it for other
    purposes, such as for the InternetWide Architecture; but if you have
    an extra /64 lying around (perhaps you got a /48 from your provider)
    then you might allocate one for 6bed4 and offer native routing on it.
    We may allocate a globally routable TBD1::/32 in the future.
  * `IWO_NAT64_PREFIX` is prefixed to 32-bit IPv4 address to form 128-bit
    IPv6 addresses; the default setting `64:ff9b::/96` is standardised and
    should work well enough.  A common alternative is to allocate a /96 in
    your own network address space.
  * `IWO_PUBLIC_IPLIST_6BED4` and `IWO_PUBLIC_IPLIST_NAT64` define the
    IPv4 address for each of the IPv4/IPv6 crossover services.  List them
    as full IPv4 addresses separated by semicolons.  There may be overlap;
    6bed4 only uses a single UDP port and the rest will forward to NAT64
    for conversion to IPv6; the only result of this overlap is that you
    influence whether the 6bed4 UDP port could be port forwarded after
    NAT64.
  * `CONTRIB_xxx` are additional packages that might be built, and for
    the example below we assume `CONTRIB_WEB_NGINX=ON`, which is not
    standard (contribs are `OFF` by default).
  * `IWO_HOST_nginx0_TCP_80` holds an IPv4 address and port to connect
    to the TCP port 80 for host `nginx0` with our web server.  Its format
    is like `123.45.67.89:80`.

You may have to press `C`onfigure a few times halfway to see all the options.
Play with it, new options appear on top with a clear mark.

Once you setup what you think looks good, you `C`onfigure a last time and
finally `G`enerate the configuration.  Your settings end up in `CMakeCache.txt`
as always with CMake, and *these settings are local to this build directory*,
as is the "mkhere" link.  Feel free to build variations in other directories!

You can now build all with `make` or be more directed; two approaches in one:

```
make -C contrib/web-nginx
cd internetwide/pub2dmz
make
```

Your first run make take a while.  It needs to download software, build some
of it from source, and begin setting up your root file systems.  But there
are caches:

  * The "mkhere" system caches builds and is mindful of versions, variants and
    flavours so you should feel free to use the same linked build environment
    in many places;
  * The `last-updated` file is tagged with a timestamp of last change; older
    files will not be installed again.

You should keep this in mind when rebuilding and especially if you want
things removed from the `rootfs/` but other than that, quick updates can
be processed with statements like

```
cmake ../..
make
```

After building, you should find a few resources that are used by RunC (and
your suspicion that you could easily extend it with similar patterns to use
for other tooling is correct):

  * `rootfs/` is a directory with the root file system
  * `config.json` or `*/config.json` is/are configuration files for the Open Container
  * `net4xxx.sh` and `*/net4xxx.sh` are network setup/teardown scripts.
  * `vardir/` et al may be mounted as persistent data directories 

Generally, you run `net4xxx.sh create` once.  This will enhance the existing
bridges with whatever else is needed; usually, a network namespace or two.
To cleanup, use `net4xxx.sh destroy`.  The subcommands `up` and `down` are
used by RunC, but might be called by you too.
If you have instance directories, the `up` and `down` are not called here,
but on each peer.  They have their own `net4xxxyy.sh` scripts, and you should
also use `create` and `destroy` on those.

Assuming that you have `iwo0pub` and `iwo0dmz` bridges up and running,
you can start them with:

```
./net4pub.sh create
runc run mkroot.pub0
# console access, possibly detach
...
# back on the metal host
runc kill mkroot.pub0
./net4pub.sh destroy
```

Note that the `runc run` will deliver a console, including any daemon
output.  A useful approach is to dump this into either `systemd` or a
manually handled `tmux` or `dtach` session.  We also tried `conserver`
but stumbled on access rights when it needs to start the container,
while RunC can hand off terminals but
[is limited in doing so](https://github.com/opencontainers/runc/issues/2110)
and `conserver`
[cannot accept such TTYs](https://github.com/bstansell/conserver/issues/45),
[was not tried on IPv6](https://github.com/bstansell/conserver/issues/46) and
[lacks dynamic TTY acceptance](https://github.com/bstansell/conserver/issues/47).

Building the web requires `WEB_CONTRIB_NGINX` to be enabled before building:

```
cd /opt/mkroot/build.tgt123
cmake -D CONTRIB_WEB_NGINX=ON .
cd contrib/web-nginx
make
./net4nginx.sh create
runc run mkroot.ngx0
# console access, possibly detach
...
# back on the metal host
runc kill ngx0
./net4nginx.sh destroy
```

While both are running, you should be able to access the server on your
prefix followed by `::a2:80a` at port 80.  Nothing fancy, no SSL.  The
webserver is `www.example.com` or, if you setup a domain in
`IWO_DOMAIN_ISP`, then at `www` underneath that domain name.

**Note:** The metal host is not the best place to connect; the funny
game with IPv6 prefixes if you have only one /96 can get in the way.
However, there is a way out if you installed the optional `dev0dmz`
interface:

```
ifup dev0dmz
nc -C -6 -s ${PREFIX%::}::a2:666 ${PREFIX%::}::a2:80a 80
GET / HTTP/1.1
Host: www.example.com

HTTP/1.1 200 OK
...
</html>
^C
ifdown dev0dmz
```
