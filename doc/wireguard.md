# Wireguard for Robust Crossover

> *The "mkroot" container configuration system uses Wireguard to protect
> crossover connections between containers in different Instances of a Site.
> It may also be used locally.*

Wireguard is a modern IP-level encryption and authentication mechanism.
It internally routes traffic based on destination IP prefix and uses a
public key assigned to that.  The public key serves as an identity for
a peer, so every different remote must have a different public key.


## Creating subnets

We introduce a `rootfs_subnet()`facility that spans hosts, as long as
they each specify the same information.  The traffic is carried over
the `dmz0` route, so directly between containers, and resolved to a
Wireguard interface within that container.  Setup is done before the
container is started, so key material is not available from within,
but the network namespace for the container holds bot the configured
Wireguard interface and its UDP socket for encrypted exchange.  In
other words, Wireguard-protected traffic is direct traffic between
containers.  The `dmz` interface is public, and indeed, Wireguard
traffic may be passed over public channels, be it in the same Instance
or between different Instances of the same Site.

The identification for a `rootfs_subnet()` are the port number and
a salt used to scramble the traffic beyond mere public-key crypto,
so it is protected from attacks by quantum computers.  The salt is
public, and will be further scrambled with the `base_secret` shared
among Site Instances, so knowledge of the salt alone is not helpful
to an attacker.  On the other hand, it avoids reversal of the
underlying public key in case of a quantum computer attack, so choose
a long one.  **Do not put off salt generation, because that laziness
can turn out costly.** If you create a new subnet, you can setup a
salt easily with a one-time statement like

```
hexdump /dev/urandom | head | openssl sha256
```

Use the same port and salt in all containers that need to connect;
the "mkroot" infrastructure makes the settings span across Instances,
but different kinds of containers may also want to exchange data.

You can describe the same subnet more than once, but with different
`LOCAL` and `PEERS` parameters, and possibly a different firewall.

The purpose of `LOCAL` is to mention the containers' host names
that locally offer Wireshark connectivity.  The `PEERS` indicate
what remote host names are reachable.  The list of `PEERS` always
spans all remote peers.  There may be overlap between `LOCAL` and
`PEERS to indicate connections between a container and its
occurrences in other Site Instances.

Firewalls are not supperted yet, use `IP_ANY` to continue to have
open connections instead of complete clogging when they are added
in a future version.  This also helps to locate firewalls that are
yet to be added.


## Keys per Container

Since Wireshark indexes `PEERS` by their public key, all potential
peers must have a different public key, within a subnet.  It is
not directly problematic to use the same public key for a peer in
the same subnet, since they only distinguish IP addresses and will
only communicate over channels that are actually configured with
the acceptable `PEERS` containers.

So, we decide on a model with one Wireshark public key per container
and pass those public keys over to other Site Instances.  When no
public key is available yet, the connection will not be setup.

The private key for the local Site Instance are stored in
`wg/priv` and named by the container name.  For `name` and `zone`
you would find `wg/priv/name.key` and `wg/priv/zone.key`
private key files.

The public keys are stored in a directory dedicated to a peer's
Site Instance, like `wg/pub3` for instance 3.  If the local
instance number is 0, then the local public keys are in
`wg/pub0`.  This directory, by that name, can be copied to
the other Site Instances, because they are all supposed to have
the same numbering scheme for Instances in their Site.  So, to
exchange keys you would copy that directory to the other site.

The key directories are useful targets for tools like `aide` or
`tripwire`, which regularly validate unchanged values for files.
You will also want to be highly protective from writing in any
of the `wg` subdirectories, and also on reading from the
`wg/priv` subdirectory.


## Key Derivation (alternative approach)

*This is not what we implemented in "mkroot", for little added value.*

The key pairs in Wireguard are Elliptic Curve in the Curve25519 group.
Since the application is Diffie-Hellman key agreement, this use case
is also referred to as X25519.

Diffie-Hellman keys allow derivation of subkeys, if we multiply both
public and private key by the same factor (in modular-exponentiation
terms, that would be raising them to the same power).  This is however
an operation to apply in the right order; we cannot just multiply the
random private key with a factor.

A private key `a` is combined with a given generator `G` to form the
public key `G*a`.  Another party generates random private key `b` and
derives public key `G*a`; the parties compute `G*a*b` and `G*b*a`,
which has the same value.  The reason is that they express the same
value `G*(a*b)` or `G*(b*a)`.  On the right side, as in the exponent
of a modular-exponentiation form, the multiplication is commutative.
That is because it is just a number that expresses the number of
terms `G` in a summing operation, so the property is inherited from
plain number theory.  Not so with the `G*a` and `G*b` operations,
which cannot be treated as `a*G` or `b*G`.  The reason is that `G`
is an entry point into a cycle of numbers (hence its name "generator"
of the curve).

So, if we want to derive the key with a modifier `m` we can modify
the generator to `G*m`.  For a public key `G*m*a` equals `G*a*m` so
we might add `m` to a given public key.  However, for private keys
this is not so easy.

What is worth noting however, is that the space of the private key
is a cyclic space, so operations can be considered to occur modulo
the order of the cycle starting from `G`.  This is a
[static order](https://en.wikipedia.org/wiki/Curve25519)
for the given curve, so we might compute `a*m` modulo this number.

This is a fair amount of work, and tools to do this are not common.
At least not in the minimalistic realm that "mkroot" uses, which
involves mostly CMake, bash and OpenSSL on the build/host platform.
So let's not go there; the only benefit is reduced keys to exchange.
(A more real benefit is that later addition of a host does not mean
yet another key exchange.  For these purposes, it may still be
possible to derive keys with this mechanism, as a deliberate choice
by an admin.)

