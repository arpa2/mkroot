# Routing behind a Fritz!Box may not be possible

> *The Fritz!Box is all but open to traffic, and it is difficult
> to find its behaviour.  In the end, you will have to peel off
> another subnet from the prefix you ISP gave you.  This is only
> possible if you have a /48 or /56 or, generally a prefix shorter
> than a full /64 prefix.*

When contacted with `ping6` from the outside, the Fritz!Box respondes with

  * `Administratively prohibited` when the firewall kicks in;
  * `Address unreachable` when it cannot find the MAC address, so when Neigbor Discovery fails;
  * *No response* when Neighbor Discovery succeeds but nothing is returned;
  * *Response timing* when everything works.

This is why the standard /64 shared with LAN hosts is unsuitable:

  * It should be possible to relay a /96 subnet to an internal node.  Ignoring advertisements
    by `radvd` makes sense for a router, but the Fritz!Box does not allow explicit
    configuration of a /96 below the primary /64.  As a result, it will use the default logic
    of delivering to a shared network, not to a router.

    *The mistake:*  Core routers refuse taking in very long prefixes, simply because their
    routing tables could be flooded.  Prefixes accepted in core routers range from /32 to /48.
    The idea of not going beyond /64 is arbitrary for a home router.  It has no bearing on
    the amount of storage, given the much larger amount of memory used for firewalls and
    neighbor cache.

  * Delivering to a shared network instead of to a router means that every single host on
    a subnet is asked for its MAC address, using ICMPv6 Neighbor Discovery.  This is not a
    problem per se, just more work than strictly necessary.

    *Cannot fixate MAC.**  The Fritz!Box has a few places where the MAC address of an
    internal host can be set, but this does not seem to work for IPv6 routes.  Setting the
    `fe80::/64` address is the mechanism for IPv6, but this too does not remedy anything.

    *Linux is not helping.*  This should have been solved by inserting the `mkroot.pub0`
    interface into a bridge that also receives the Neighbor Discovery requests.  Somehow,
    and certainly something we would like solved, these queries do not land on the link
    to the `internetwide/pub2dmz` container which would relay it.  We tried `ndppd` to get
    this done, but failed.

You therefore need to delegate another /64 prefix than the one you normally use for your
laptops, servers and so on.  This is because you require it to be *routed* to you,
rather than sent from a router to *neighbours* on the same network.

You generally need to:

  * Delegate an IPv6 subnet of /64 size, using either `wide-dhcpv6-client` (the `dhcp6c`
    program) or setup a Static IPv6 Route.  Be sure to tick off `open firewall for
    delegated IPv6 prefixes of this device` while configuring Permit Access for the
    MAC address of the router.  If your router uses a bridge, be sure to specify the
    MAC of the actual interface that handles that branch of the bridge.  Setup an
    Interface ID that matches the MAC in the usual manner but care less about the IPv4
    address, which need not even exist.

  * To allow incoming traffic, you need to expose the host that serves as the router.
    This is an `fe80::/64` link-local address.  You do not need to expose it for IPv4,
    unless you want to route some of that in too.  Without this, you will see the
    *Administratively Prohibited* message.

  * To allow outgoing traffic, you need to tell the Fritz!Box to accept prefixes from
    other routers on the network.

  * You need to setup routing in the internal network.  Generally, when you setup a
    /96 and do not setup an `IWO_PUBLIC_BRIDGE`, the right thing should happen.

      - The /96 is part of the entire /64 delegated to you.  Traffic for this
        prefix is sent into this interface, via the `fe80::/64` link-local
        address on the other side of the link.

      - The `fe80::/64` address on the `mkroot.pub0` interface (assuming your
        `IWO_SITENAME` is set to `mkroot`) is used from the interior of the
        `mkroot.xxx` host structures to sends traffic out.

      - You can setup as many Sites or Instances, each with their own /96 prefix,
        as you like &mdash; namely, as many as there are IPv4 addresses for the
        entire Internet, a whole 32-bit space.  You can create those side by side,
        each of these will have its own /96 prefix routed in and out.

*Experience gathered with Fritz!OS 07.16*
