# Open Issues

> *A wild bundle of thoughts and ideas.*


## Multicast over VEth devices?

Poor documentation.  Maybe better,
https://linux-blog.anracom.com/2017/10/30/fun-with-veth-devices-in-unnamed-linux-network-namespaces-i/
https://linux-blog.anracom.com/2017/11/12/fun-with-veth-devices-linux-bridges-and-vlans-in-unnamed-linux-network-namespaces-ii/
https://linux-blog.anracom.com/2017/11/14/fun-with-veth-devices-linux-bridges-and-vlans-in-unnamed-linux-network-namespaces-iii/
https://linux-blog.anracom.com/2017/11/20/fun-with-veth-devices-linux-bridges-and-vlans-in-unnamed-linux-network-namespaces-iv/
https://linux-blog.anracom.com/2017/11/21/fun-with-veth-devices-linux-bridges-and-vlans-in-unnamed-linux-network-namespaces-v/
https://linux-blog.anracom.com/2017/11/28/fun-with-veth-devices-linux-bridges-and-vlans-in-unnamed-linux-network-namespaces-vi/
https://linux-blog.anracom.com/2017/12/30/fun-with-veth-devices-linux-bridges-and-vlans-in-unnamed-linux-network-namespaces-vii/
https://linux-blog.anracom.com/2018/01/05/fun-with-veth-devices-linux-bridges-and-vlans-in-unnamed-linux-network-namespaces-viii/


## Sites and Instances

The idea of Sites is clear, and trivially implemented.
With the /96 prefixes, it is even trivial on one machine.

The idea of Instances is now clear as well, but not implemented.
Instances each have their own /96 prefix, but they collaborate.
Instance numbers may be derived from these /96 prefixes, by
looking up a list index number (possibly after sorting).

Instances share their `IWO_SITENAME`, `IWO_SCRAMBLER` and
`.base_secret` so they harmonise.  That is, they derive the
same `rootfs_secret()` values, the same /96 prefixes for
the backend and service networks and, as a result, may share
those for crossing over in redundant schemes.


## Multihoming

The use of multiple IPv6 prefixes in one container is possible
with multiple `dmz` interfaces.  It makes sense for some of the
protocols, like those that use SCTP (though not for crossover).

Multihoming is probably an abandoned idea, in the interest of
replicated services that are mostly stateless.  But they seem
to follow the same line of thought as Instances, so they may be
an easy gain (and it hints that we should allow `dmz` sharing).


## How to import a remote system's bridge and make it appear locally?

Maybe we should use a VxLAN, specified in RFC 7348.

It remembers what node is home to a MAC address, and directs traffic
straight to it, instead of searching around.

The Linux kernel documentation on VxLAN,
https://www.kernel.org/doc/html/latest/networking/vxlan.html

A gentle introduction, demonstrating how to define VxLAN interfaces
and how they may be used in local bridge ports,
https://programmer.help/blogs/practice-vxlan-under-linux.html

VxLAN is an overlay network that runs on UDP port 4789 (IANA assigned).
The VxLAN Tunnel EndPoints are called VTEPs.

It goes up to 16 million overlay networks, also spanning distances (or at least IP ranges).
Is there a use for WireGuard in securing remote links?  Secret keys are easy in "mkroot".


## OpenVSwitch instead of veth/brctl?

Nice article comparing infrastructures,
https://www.opencloudblog.com/?p=66

Continuation about performance, where OpenVSwitch wins on throughput,
https://www.opencloudblog.com/?p=96

Use OpenVSwitch instead of VEth/BrCtl,
https://www.opencloudblog.com/?p=386


## SystemD integration

It should be simple enough to produce systemd scripting for our
containers.  They might capture the consoles and let us use
`runc exec` to land in a terminal.  Although `ip netns exec` is
often more useful, having full access to the tools on the host.

Each container could have its own service definition, and the
set of all enabled services could be combined into one target.
It would then be possible to operate systemd as it is intended.

Sites and Instances should be distinct, so the scripts would use
service names may look like container names (`mkroot.live.kip0`),
with the shared target looking like the Site with its Instance
number (`mkroot.live.0`).

It probably makes sense to rely on the interfaces being up, too.
Let's not get overboard though, and assume that is taken care of
by the networking code.  It might be useful to have separate
targets for the network name spaces of containers, because they
run longer lives but also form a true dependency.

*If you think SystemD is fun, then feel free to jump at this bait.*


## Consistent MAC Addresses

Random MAC addresses are generated upon plugin of a module.
To allow faster moves, it may help if these are consistent.
To that end, `ip link add` can use `address xx:xx:xx:xx:xx:xx`
where the bytes would be generated.

This should speed up the stop and start of a container with
the same IPv6 address, because Neighbor Discovery caches
will not have to relearn where other hosts are.

It is useful to have the same Site and Instance produce the
same value, but other Sites and other Instances all need to
be different.  So, we might use the `IWO_SCRAMBLER` and the
Site name and Instance number, along with the interface to
generate for.

This may be all that is needed to have exact replica's of
both Site and Instance, where one can take over from another.


## File Storage

The various `vardir` contain any application data worth
storing across runs.  They may be more useful when a storage
device can be assigned separately.  Note that this would
cause problems with fixup scripts that need to work on them.
Maybe a `chroot` class to enter specifically that directory
is a good solution.

A storage device might use `.../internetwide/identity/var/`
as its `vardir` analogon.  Or it might be possible to enter
the `rootfs` with that directory temporarily mounted.

Centralised file stores are very useful.  They might be
easily exported over NFS or a ClusterFS for quick take-over.
They might be easily backed up with `e2fsdump`, `rsync` or
`zfs send`.

