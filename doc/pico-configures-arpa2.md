# PICO configuration for ARPA2

> *These commmands are sent from Control Tower to
> change the set of identities to be hosted.*

Identities are domains and their collections of users and hosts.

## Have and Drop are Idempotent

The main commands are named to suggest idempotence:

  * `have` wants to have an item
  * `drop` wants to get rid of an item

If all actions occur in-order, then the opening and closing of resources should work predictably, including when they are dependent on each other.

A workflow-styled process might repeat a step if it sees no result, and this could cross; in that case, it is the same step in immediate succession, and that is the case where idempotency is helpful.

## Domain with User and Host

The vital parts of domain management for ARPA2

  * `domain` is a domain name as registered anywhere
  * `user` adds a user, group, and so on to a domain
  * `host` adds a service hostname to a domain
  * `cert` adds a certificate for a host

You normally do things in this order:

  * `have domain`
  * `have|drop owner|host|cert` as often as desired
  * `drop domain`

That's because `user` and `host` depend on each other, but there is cross-correlation.  As part of `drop domain`, there is an implied `drop` on its `user` and `host` elements.

Specifically, hosts do not have users; domains have users and hosts may try to authenticate users under their registering domains.

## Basic Variables

The commands are given a few bits of information through environment variables:

  * `ARPA2_DOMAIN` is the domain name at hand
  * `ARPA2_ID` (derived, not sent) is the full identity for a user (or service) ending in `@` and `ARPA2_DOMAIN`
  * `ARPA2_USER` is the base identity of a user (or +service) before `@` or the first `+` separator
  * `ARPA2_ALIASES` is the sequence of aliases (or service args) starting at the first `+` separator and before `@` (or, in theory, before `+<signature>+@` but signatures should not be used here).
  * `ARPA2_HOST` is the UTF-8 part before a dot and the `ARPA2_DOMAIN` to form a full hostname.
  * `ARPA2_FQDN` (derived, not sent) is the fully qualified host name in UTF-8.

A few useful invariants explain how the longest forms can be easily derived; their names are however advised for readability of scripts.

  * `${ARPA2_ID}` is the same as `${ARPA2_USER}${ARPA2_ALIASES}@${ARPA2_DOMAIN}`
  * `${ARPA2_FQDN}` is the same as `${ARPA2_HOST}.${ARPA2_DOMAIN}` for which it follows that we do not support the domain name itself as a host name.  If we ever support this hack, then we will probably redirect traffic from a fixed IP, much like a NAT router does.  Sad technology, really, to confuse hosts and domains, and only done to put up with ancient protocols that still have no SRV records defined for them.

Where they occur:

  * `ARPA2_DOMAIN` is always present
  * `ARPA2_USER` and `ARPA2_ALIASES` are used with `user`
  * `ARPA2_HOST` is used with `host` and with a `cert` that is not (just) meant for `ARPA2_DOMAIN`

## Specific Variables

Some calls may be sent extra information that others would not find useful (and would ignore if it was sent to them).

**Address information** may be useful in various situations.  Only IPv6 addresses make sense in the "mkroot" framework, because translation from IPv4 is done externally.

  * `ADDRESS_<name>` is a space-separated list of public IPv6 addresses for a host that offers service name `<name>`.  Servers can recognise their own to know that they are to support it.  It is also possible to pickup other services for the same domain or host from this.

**Service Names** are used to toggle a service on or off for the current domain and, if supplied, specifically for the current host.

  * `SERVICE_<name>` is set to `1` or `0` to reflect whether the service named `<name>` is enabled or disabled.  Service names are unique, but a service container may offer to serve any number of them.  Services are switched on or off for the current host and/or domain.  Absent `SERVICE_<name>` variables indicate that they are not changed, values that match the current situation are idempotent confirmations, but the behaviour for `0` will often be forcefully applied during `drop` operations, in such a way that cleanup follows a service-specific logical pattern during the removal of identities.

**Name servers** have their own infrastructure, which is not always public.

  * `ADDRESS_zone` is the zone management address, also known as the master name server.  Whether it is published in a zone or hidden remains undefined.
  * `ADDRESS_name` are the name server addresses, also known as the slave name servers.  These are usually published in a zone, but they need not always be; moreover, only IPv6 addresses are shown here.
  * `SERVICE_zone` and `SERVICE_name` are toggles for service.  These would always be `1` for local zones, but may be `0` for secondary zones.  Imagine a zone for which we only run a slave name service, or perhaps not DNS but only an SMTP relay.

## Script Patterns

A script for a given container is typically called as `/bin/arpa2control.ash` and starts with `#!/bin/picoget /pico/abc0.pico.in`.  It intends to setup a fixed set of services on the local machine by changing service configuration files and/or databases.

**Composites.**
Both the host and domain name are available; use the right one in the right places.  Likewise, the username and aliases each have their own uses.  If you need the full hostname or full ARPA2 Identity, construct it with

```
ARPA2_ID="$ARPA2_USER$ARPA2_ALIASES@$ARPA2_DOMAIN"
ARPA2_FQDN="$ARPA2_HOST.$ARPA2_DOMAIN"
```

**Logging.**
A few general ideas are writing to log files on errors or warnings, like

```
die () {
	logger -s user.err "$HOSTNAME: $*"
	exit 1
}

warn () {
	logger -s user.warning "$HOSTNAME: $*"
}

notice () {
	logger -s user.notive "$HOSTNAME: $*"
}

debug () {
	logger -s user.debug "$HOSTNAME: $*"
}
```

These can be used in statements like

```
[ -d "/etc/abc.conf.d" ] || die "ABC configuration directory evaporated"
```

**Acceptance.**
To avoid running silly commands, which may be meaningful in another container or another time/version, as well as to setup the remaining script with handy variables and toggles, a safe pattern is

```
case "$1 $2" in
"have domain")
	PLUS_ABC=12345
	MINUS_XYZ=
	;;
"drop domain")
	PLUS_ABC=
	MINUS_XYZ=13908
	;;
*)
	exit 0
	;;
esac
```

**Cleanup.**
To automatically remove a service during a `drop` command, a safe pattern is

```
case "$1 $2" in
"drop domain")
	SERVICE_abc=0
	;;
*)
	;;
esac
```

To automatically deselect when the local IP address `$MY_IPV6_ADDRESS`, is not in an `ADDRESS_<name>` list, a safe pattern is

```
case " $ADDRESS_abc " in
*" $MY_IPV6_ADDRESS "*)
	;;
*)
	SERVICE_abc=0
	;;
esac
```

**Idempotence.**
For each service, it is now relatively easy to idempotently switch a service on

```
_CHANGE_ABC=false
if [ ! -f "/etc/abc.conf.d/$ARPA2_DOMAIN.conf" -a "$SERVICE_abc" -eq 1 ]
then
	...prepare file...
	mv -f "/etc/abc.conf.d/$ARPA_DOMAIN.prep" "/etc/abc.conf.d/$ARPA2_DOMAIN.conf"
	_CHANGE_ABC=true
fi
```

Likewise, idempotent removal of a service is relatively easy to implement

```
if [ -f "/etc/abc.conf.d/$ARPA2_DOMAIN.conf" -a "$SERVICE_abc" -eq 0 ]
then
	rm -f "/etc/abc.conf.d/$ARPA2_DOMAIN.conf"
	_CHANGE_ABC=true
fi
```

When the enable and disable actions are similar, it is also possible to
check if the current state (or "ist") matches the desired state (or "soll"),
and though `$SERVICE_<name>` is like the latter, it may also be empty.
The form then is more like

```
_CHANGE_ABC=false
_IST_abc=...sample-current-state...
if [ "${SERVICE_abc:-$IST_abc}" != "$IST_abc" ]
then
	...go_from_IST_to_SOLL...
	_CHANGE_ABC=true
fi
```

At the end of the services, daemons may need to be updated

```
$_CHANGE_ABC && abc.update
```

**Atomicity.**
A boolean variable can be helpful to retain transaction state, so that (part of) the change is made either completely or not at all.  A pattern is

```
_TROK=true
$_TROK && make_a_step now please || _TROK=false
$_TROK && make_another_step fast || _TROK=false
...
$_TROK || cleanup_after_failed_steps
$_TROK || die "Failed to transactionalisificationismuss"
```

An alternative wrap-up with commit/rollback options would be the following, switching on `$_TROK` instead of  `[ $_TROK ]` which would not work in a pattern like

```
if $_TROK
then
	abc.commit
else
	abc.rollback
fi
```

While working on a transaction, it may be necessary to open resources that need to be closed later.  A safe pattern for this is

```
_DJBOPEN=false
$_TROK && djingboems open && _DJBOPEN=true || _TROK=false
...
$_DJBOPEN && djingboems close
...$_TROK...
```

