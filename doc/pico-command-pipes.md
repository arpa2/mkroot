# PICO for Command Pipes

> *The containers in "mkroot" can communicate simple commands
> using the pico system from mkroot-utils.  It is mostly used
> from the Control Tower to notify service containers about a
> configuration change.*

The idea of `pico` is quite simple:

  * Send commands in a non-blocking manner; fail if nobody
    is listening and let the environment retry later.

  * When a container is running, make it listen as much as
    possible, by respawning a listener in `/etc/inittab`.
    Only while processing a request would its handler be
    temporarily unavailable.  Processing should therefore
    be quick and not block-waiting.

  * Aside from command words, interpret the first few
    words as environment variables when they contain an `=`,
    but do not replace variabels from the context.

This can be used in many ways, just a few examples are:

  * Notify name servers about a new zone or one that is
    to be dropped.  The idemopotent variations on `add` and
    `del` would be `have` and `drop`, which suggests not
    raising an error when they are repeated.

  * Notify TLS services about a new certificate for a
    hostname that they might be interested in.

A [complete integration example](https://gitlab.com/arpa2/mkroot/-/commit/e14a52171ba4bab4fd75acb9d5cf5b460c219eff)
for `web0` and `mail0` notifications of new certificates
is probably helpful.  These receive a variable `$HOST` with
the certificate's hostname, and if this is locally used
then it may reload configuration fiels.

A [complex handler script](https://gitlab.com/arpa2/mkroot/-/blob/master/internetwide/zonemaster/usr/bin/zone-add-del.pico.in)
shows more advanced processing, where Knot DNS is setup with
another zone via its `knotc` command, its `keymgr` is setup
with key material and transactional state is managed to
be atomic in handling the change.  Extensions can easily be
imagined to do things like setting up master/slave connections,
adding/removing resource records and so on.

There already is a good degree of control integration, both for
[setting DANE records](https://gitlab.com/arpa2/mkroot/-/blob/master/internetwide/tower/usr/bin/dyndns-setdane.ash.in)
and for
[testing DANE records](https://gitlab.com/arpa2/mkroot/-/blob/master/internetwide/tower/usr/bin/dyndns-hasdane.ash)
but this uses Dynamic DNS updates, which are not generally
usable.  Had this not existed, then it too would have been
a candidate for `pico` automation.

