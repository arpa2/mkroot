# Port Forwarding for IPv4 Services

> *These containers are IPv6-only, but sit behind a translator for IPv4
> backward compatibility.  To allocate service ports in IPv4, you need
> to setup port forwarding.*

We publish services on the `mkroot.dmz` bridge, usually with an IPv6-only
`nftables` firewall per container.

## Why prefer IPv6 over IPv4?

The use of IPv6 is beneficial:

  * It is the generally accepted generation after IPv4;
  * The much larger address space offers configuration freedom in network setups;
  * We use it, for instance, to scramble our internal network prefixes;
  * IPv6 offers transparant addressing, so it can avoid NAPT;
  * This supports peer-to-peer protocols;
  * Peer-to-peer protocols make users less dependent on proxy services;
  * Proxy servers offer leverage to providers to control online behaviour.

So, it is a direct benefit for online freedom to use IPv6.  The reason we
still get pretty far with IPv4 are manyfold:

  * NAPT allows sharing a minute addressing space with many;
  * Technology to pierce through NAPT routers is common;
  * This all works smoothly for client-server protocols;
  * Peer-to-peer protocols are not in common use;
  * Catch-22, nobody asks for what they don't know they can/should have.

## Tunneling for full IPv6 benefits

You can run IPv4 and IPv6 together, so-called dual-stack approaches.
If you do this and connect to a remote server, then your computer
can use either.  You should prefer IPv6 for peer-to-peer software,
but the choice matters less when you are a strict client; in that
case, you should go for reliability.

By running an IPv6 tunnel like a `6bed4peer`, you can have the benefits
of IPv6, even if it routes over IPv4 or UDP/IPv4.  Tunnels do vary in
quality, `6to4` and especially `Teredo` are problematic; the latter
happens to be the default on Windows platforms.  In contrast, `6in4`
and `6rd` are solid, and our own `6bed4` should be too and even add
direct peering in many cases.

## Backward Compatible with IPv4

Not everyone will want to setup a tunnel.  Many still rely solely on
IPv4, and backward support is desirable.  As explained, for client-server
connectivity it makes complete sense to rely on IPv4.

There is a remaining problem of limited space; a network may have only
1 IPv4 address but commonly gets 2^64=18446744073709551616 or even
2^80=1208925819614629174706176 IPv6 addresses; this may even be routed
into a colocated server or home network, because that is seen as a
network; this common approaach is what we assume in support of our
networks of containers.

We use port forwarding to map IPv4 traffic onto IPv6 traffic.  We do
this after NAT64 which is a one-on-one address mapping, so what the
port forwarder does could be called NAPT66.

## Allocation of Service Addresses

The `mkroot.dmz` receives a /96 prefix delegated from the host network.
This is such a large space that we statically allocate (default)
the lower half of addresses, like `::a2:80a` where we use `a2` for
ARPA2 components and `80a` encodes HTTP (see the port number?) and
an instance (`a`, `b`, `c`, ...) &mdash; that is how spaceous IPv6
gets.  This notation expands to the 64 bits `0000:0000:00a2:080a`
and is postfixed to the delegated /96 prefix of the site.

Services specify their ports usage with statements like
```
rootfs_ports (nginx0
        IFACE_IDENT ::a2:80a
        TCP_IN 80 443
)
```
which states the static allocation of the lower half (the `IFACE_IDENT`,
short for interface identifier) and input ports 80 and 443.  This is
enough information to setup a container-level firewall but also to
know what port mapping options exist.

### Port Forwarding for IPv4 Service

The `rootfs_ports()` declaration also drives port forwarding for IPv4; note
that this is quite common in the IPv4 protocol, and that we advise IPv6 for
any peer-to-peer activite.  Port mapping for IPv4 is not an automatic
process.  In fact, it must be an administrative choice, because some taste
and policy is involved in squeezing services into a tiny IPv4 port/address
space, and perhaps to avoid adding some.  The CMake configuration adds
variables to allow per-site settings:

```
IWO_HOST_nginx0_IFACE_IDENT=::a2:80a
IWO_HOST_nginx0_TCP_80=123.45.67.89:800
```

The former is filled with the static default, but the latter is initially
empty.  As long as it remains empty, the service is IPv6-only.  This can
be useful for experimentation (the large address space even gives some
protection form port scanning, so this is a pleasant way to start a new
service).  Once ready to map the service onto IPv4, an IPv4 address and
port can be configured and an update run.

These settings generate the port forwarding definition in a file
`internetwide/pub2dmz/portfwd.d/nginx0.map` where it can be picked up
for processing in the `net4pub.sh` script.  The info is parsed when
bringing up the `pub2dmz` container, and it can be updated with

```
net4pub.sh portfwd
```

**TODO:** This procedure currently removes all NAPT mappings before adding new.  This can cause a glitch in the NAPT mappings.  The mapping is stateless, so connection states will not be lost.

The result of these procedures to setup port forwaring ends up in
the `pub0ns` name space for the `pub2dmz` instance in bidirectional
translation maps,

```
table ip6 raw {

	# Example TCP mapping 123.45.67.89:800 <--> [2001:db8:9:10::a2:80a]:80
	# using NAT64 mapping 123.45.67.89     <-->  64:ff9b::7b2d:4359

	# Map the NAT64-mapped IPv6 address to an mkroot.dmz IPv6 address
        map map-nat66-daddr-in {
                type ipv6_addr . inet_proto . inet_service : ipv6_addr
		elements = { 64:ff9b::7b2d:4359 . tcp. 800
		             : 2001:db8:9:10::a2:80a } ...
        }

	# Map the IPv4-side port to an mkroot.dmz port (for some protocol)
        map map-nat66-dport-in {
                type ipv6_addr . inet_proto . inet_service : inet_service
		elements = { 2001:db8:9:10::a2:80a . tcp . 800
		             : 80 } ...
        }

	# Map the mkroot.dmz port to an IPv4-side port (for some protocol)
        map map-nat66-sport-out {
                type ipv6_addr . inet_proto . inet_service : inet_service
		elements = { 2001:db8:9:10::a2:80a . tcp. 80
		             : 800 } ...
        }

	# Map the mkroot.dmz IPv6 address to a NAT64-mapped IPv6 address
        map map-nat66-saddr-out {
                type ipv6_addr . inet_proto . inet_service : ipv6_addr
		elements = { 2001:db8:9:10::a2:80a . tcp. 800
		             : 64:ff9b::7b2d:4359 } ...
        }

	# Map NAT64-mapped address:port to mkroot.dmz address:port
        chain prerouting {
		... map-nat66-daddr-in  ... map-nat66-dport-in  ...
	}

	# Map mkroot.dmz address:port to NAT64-mapped address:port
	chain postrouting {
		... map-nat66-sport-out ... map-nat66-saddr-out ...
	}

}
```

Port forwarding from IPv4 to IPv6 first translates the address based on the
IPv6 address after NAT64 and the unchanged protocol and port, to a target
IPv6 address; then with this address and the same protocol and port to a
target port, of course for the same protocol.  Reply traffic reverses this
by first mapping back the port, then the address.  The entire mapping is
stateless, and rests chiefly on map lookups.  Note that NAT64 also provides
a stateless IPv4/IPv6 address mapping, and does not alter protocol or port.

**Known problems:**
(0) Good about this procedure is that the mappings are generic and stateless.
(1) The current version of `nftables` does not allow the simultaneous mapping
of the IPv6 address and port, which
[can cause problems](https://www.spinics.net/lists/netfilter-devel/msg67543.html)
in corner cases.  By first mapping from the tight address space of NAT64-mapped
address to the /96 prefix on `mkroot.dmz` we reduce the risk of running into this,
but the mapping should really be improved as soon as `nftables` allows it.
(2) Note the
[missing support for ICMP](https://www.spinics.net/lists/netfilter-devel/msg67573.html)
in the current version, which means that IPv4 hosts may not see things like
failures to connect.
(3) The current implementation unfolds separate rules for TCP, UDP, SCTP and
DCCP, but they use the same patterns; it is *almost* possible to use the
protocol as a variable, and certainly the future intention of both `nftables`
and our `pub2dmz` port mapping design.

