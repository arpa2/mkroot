# Whois registries for ARPA2 Address Allocation

> *We do static address configuration, based on an allocation policy.*

Each group of containers generates a `whois.txt` with written-out IPv6 addresses
assigned to containers in this "mkroot" repository.

WHen you create a new container and need addresses for it, please register them
in the respective `whois.txt.in` file, under the indicated policy.

  * `internetwide/whois.txt.in` holds the `::a2:0/112` namespace.  It is only
    allocated by InternetWide.org in the interest of a core set of services
    that implement the InternetWide Architecture.  Discuss with us if you
    think we should add something here, but do not make the allocation
    yourself.  Use the `contrib` space for that.  We usually take a port number
    in BCD-notation, or variants thereof by adding a letter behind it.

  * `contrib/whois.txt.in` holds the `::a3:0/112` namespace.  These are contributions
    from others, and they may evolve into the `internetwide` class if that
    makes sense for the InternetWide Architecture.  Unlike that category
    however, anyone can allocate an address in this list.  Leading are those
    that are in the head.

  * `demo/whois.txt.in` holds the `::a1:0/112` namespace.  This is filled
    with demonstration software.  Inasfar as those need an address, they are
    allocated by InternetWide.org, but much more accessible than the
    core set of services in the `internetwide` group of containers.

  * `local/whois.txt.in` holds the `::a0:0/112` namespace, and is intended
    for local use.  You should probably update it to reflect local choices.

We hope that this reflects the best public interest.  You may make local
modifications to these policies, but that may block upstream acceptance if
it is not in line with these policies.  Please refrain from distributing
under chaotic alternative policies.  Rather, discuss your ideas and we may
end up changing the policy overall.

Note that the `@IWO_SITENAME@.dev2xxx` interfaces (which you can obtain
through commands like `ifup @IWO_SITENAME@.dev2dmz` in the Debian case)
cover a `/108` prefix so all these address ranges are reachable.

