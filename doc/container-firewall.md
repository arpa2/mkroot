# Generated Container Firewalls

> *The firewall generated for a container is derived from
> the ports declaration.*

Firewalls for a container are stored in their network
name space.  This means that they are only evaluated
for traffic passing in and out of that host; when there
are many containers running in parallel, there is no
need to evaluate each and everyone's rule set for each
and every network frame.

## Example of a Generated Firewall

If your container's CMakeLists.txt specifies

```
# Ports 88 for Kerberos/KXOVER, 464, 749 for Kadmin
rootfs_ports (identity0
        IFACE_IDENT ::a2:88
        UDP_IN 88 464 749
        TCP_IN 88 464 749
)
```

then, assuming that `IWO_PUBLIC_PREFIX96` is set to
`2001:fd8:9:10:11:12::` the firewall generated for this
container instance is

```
table ip6 dmz0 {

   chain input {
      type filter hook input priority 0; policy drop;
      oif != "dmz0" reject
      ip6 daddr != 2001:fd8:9:10:11:12:a2:88 reject
      ip6 nexthdr ipv6-icmp accept
      tcp dport { 88, 464, 749 } accept
      udp dport { 88, 464, 749 } accept
   }

   chain output {
      type filter hook output priority 0; policy drop;
      iif != "dmz0" reject
      ip6 saddr != 2001:fd8:9:10:11:12:a2:88 reject
      ip6 nexthdr ipv6-icmp accept
      tcp sport { 88, 464, 749 } tcp flags & (syn | ack) == syn reject
      tcp sport { 88, 464, 749 } accept
      udp sport { 88, 464, 749 } accept
   }
}
```

## Stateless Filtering of Connections

This is a *stateless* firewall, so there are no expiration timers
that might spoil things.  This does mean that the `syn` flag without
`ack` is recognised as a form of TCP connection attempt, and since
`TCP_IN` is declared, this is not permitted in outward direction.
For `TCP_OUT` ports this requirement is added to the inward direction.
For `TCP` without direction, it is not required.  The ports for all
TCP directions are combined in the port-based `accept` statements.

For UDP, it makes very little sense to speak of directions, so the
ports are always passed bidirectionally.  Specifically, there is no
state built up when an initiative is taken, so there is no timeout.
The direction may still be of use for other derived code, but not
for the firewall.

For SCTP, we need to recognise the INIT frame, which we do with the
verification tag 0 that is sent and verified by communicating peers.

For DCCP, we have to recognise the REQUEST frame, sent by the client
to initiate a connection.

## Use in Scripts and Configurations

The firewalls are written to a file named `nft4<host>.sh` which asks
for `#!/bin/sh` and should run on both `bash` and `ash` shells, so
under normal Linux and BusyBox.

When used with Open Containers, the script should be called from a
`net4<host>.sh` script.  Be sure to run it in the network name space
of the `dmz0` interface!  It is possible to define separate scripts
for each host, tailored to each host's use of ports and interface
identifier.  You can choose your `<host>` identities freely to
accommodate that, there is no formal binding between the firewall
rules and their use in container instance names.

When running the root file system on a standalone machine, such as
with PXE, the scripts must be run, usually from the `rootfs/init`
script that is only run in these standalone situations.  Note that
`rootfs/etc/rcS` is less suitable, because it runs everywhere.

To support this approach, the firewall with the `<host>` set to
the container instance name is usually desired.  Since you call
it from your script, you can choose this.  **TODO:** PXE will
install all `nft4<host>.sh` scripts as well as `nft` into the
`initrd.gz` image file (maybe via `rootfs/var/lib/arpa2/mkroot/`).

## Future Options

We may vary the generated rule set, but this should give an impression
of the idea.  There may be a future need for variations in form also:

  * We might combine rules in different ways; for instance, a separate
    rule for TCP to check `tcp flags & (syn|ack) == syn` may be changed
    by merging them.  The general TCP rule may then range over fewer
    ports, though.  The use of sets limits the number of rules, and so
    not really much would change on average.  In a server, one might
    argue that most connections are incoming, and the style should
    emphasize that.

  * We may allow multiple instance names and/or interface identitites
    with the same configuration, or allow no instance name to fall back
    to the name for the container with a digit added.  You should be
    able to control it however, so you can match it with instance names
    for RunKinds.

  * We may consider dropping the interface identifier and then also
    dropping the rules that match `ip6 daddr` and `ip6 saddr` and leave
    that to the network layer.  Or we might reduce these rules to just
    the /96 prefix configured for the `dmz0` network and leave the
    matching of the interface identifier to the network interface.
    This can be helpful with shared firewall definitions for multiple
    instances, as well as serving single hosts with multiple addresses.

  * We may add flags to hint other forms of firewall.

  * A flag like `TCP_OUT_EPHEMERAL` may be used to flag a NAT64 prefix
    and allow outbound traffic to arbitrary ports from clients running on
    the network.  This may in fact be a place to perform NAT mapping on
    NAT64 address ranges.  Not sure if that would spread knowledge, but
    since we already do that for the standard `dmz0` prefix, why not do
    it for the NAT64 prefix too?

  * A parameter like `ICMP_RESTRICT 1 3 4` may be used to list ICMPv6
    types that may be sent out, rather than implicitly granting every
    ICMPv6 type.  We may also need a notation to grant nothing, but
    that does parse anyway.  Defining `ICMP_RESTRICT` without types may
    do this trick.

