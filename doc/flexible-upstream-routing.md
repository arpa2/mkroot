# Flexible Upstream Routing

> *The link into the infrastructure is the most difficult part.
> What flexibility do we have?*

Let's see how much tinkering and special cases could be warranted.
Maybe a framework arises that we could somehow incorporate by
putting the right script in the right place.


## Public Interface

(TODO: These names have been changed.)
The setup uses a link `iwo0pub` to `pub0pub` and imports the latter into the
`pub0ns` network name space by the name `pub0`.  When the network is dismantled,
the interface moves back.  This simulates what Linux would automatically do
with a physical interface (and works on those, too).

One might say that `iwo0pub` is not interesting at all; the only interface
that matters is `pub0pub` which might indeed by a physical interface.  That
makes it easy to incorporate a complete network.


## Address Assignment

There are a few options for configuring addresses on the public interface:

 1. Manual configuration
 2. Subsets of a public address/port space
 3. DHCP
 4. PPPoE
 5. Tunnels for IPv6 (or, in the future, for IPv4)
 6. Bootstrap access

Configurations for interfaces do not move along when they migrate to another
network name space, so it must all be done in `pub2dmz` if it is to work.

**Manual configuration** is a matter of setting up an IPv6 prefix for IPv6,
or an IPv4 address and subnet mask for IPv4.

**Subsets of public space** should be forwarded over a link, like we have
now between `iwo0pub` and `pub0pub`.  Filtering/forwarding is done on the
metal host.

**DHCP** is a known bootstrapping procedure, and may be separately chosen
for IPv4 and/or IPv6.  For IPv6, it only makes sense when subnets are
delegated of at least /64 size.

**PPPoE** can be used with a `pppoe-relay` that sits on the metal host
between the current `iwo0pub` and a physical interface.  The `pub2dmz`
then initiates PPPoE to retrieve address information for `pub0`.  This
is possible with IPv6 as well as IPv4, with variations of the same
protocol.

**Tunnels** may be added with Linux logic.  Their configuration would
normally be static if they are to be stable; think `6in4` or `6rd` but
reliability of `6to4` and `Teredo` are questioned.  The size of the
address space provided by a `6bed4peer` seems insufficient (no /64).

**Bootstrap access** such as SSH logins for an administrator may be
relayed back to the metal host, possibly over the link between
`iwo0pub` and `pub0pub`.  Take care that it actually works, and have
a fallback without functioning `pub2dmz` otherwise.

Most of this has been done before... we could incorporate existing logic
from OpenWRT, or ThinStation, or Puppy Linux, or a Debian installer, ...


## Not a /96 for IPv6

What makes us require at least a /96 for IPv6?

  * Service hosts have manual addresses, and connect to /96 `iwo0dmz`;
  * Internal bridges use internal addresses `fdXX:XXXX:XXXX:XXXX:XXXX:XXXX::/96`;
  * NAT64 has its own space `64:ff9b::/96`, so it does not matter;
  * Connections to remote sites are based on their /64 prefixes;
  * The `6bed4router` has a hard requirement for most of a /64.

In general, routing problems are likely when the host shares the /96
with `mkroot.dmz`; it should be assigned an interface to `mkroot.dmz` similar
to the `dev0dmz` example in `internetwide/pub2dmz/debian.interfaces`
and could then work.  These interfaces have a smaller subnet than the
full /96 but may have a /96 route into it.  This also helps to relay
the full /96 into `pub2dmz` without looking first.

The `6bed4router` has a strict requirement for a full /64.  If it
shares the /64 with the containers it should even be carefully
configured; either hand out 6bed4-specific `fc64::/16` or `TBD1::/32`
addresses or reconfigure all container addresses to interface identifiers
that hold the address and port of the `6bed4router`.  The difficult
part is that the `6bed4router` is not optional; it is a must-have for
reliable peer-to-peer operation, which neither IPv4 alone nor NAT64
with Port Forwarding can provide.  And we do need to be warm and open
to peer-to-peer services, in the interest of online freedom.


## Not an IPv4 subnet (or Full Address)

IPv4 is fallback support; in extreme cases, one might go without.
This is a special case of what is safe in `pub2dmz`, that is, to
configure broader IPv4 claims than are actually implemented or
used.

IPv4 always passes through the `6bed4router` or through NAT64 and
Port Forwarding.  The logic of NAPT therefore applies (and the
`6bed4router` bypass can be considered equivalent to Port Forwarding)
and any similar logic on the metal host would not aggrevate the
situation.  Do note that NAT64 and Port Forwarding are static, and
introduce no timeouts of any kind; the metal host might worsen the
NAPT situation if it introduced that.

It is clearly helpful that IPv4 operates in client-server mode.
The specific area where IPv6 shines is peer-to-peer operation,
and that is why there are stricter requirements in for IPv6.
In most situations this should not be problematic, though.

