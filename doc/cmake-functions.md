# CMake Functions for mkroot

> *The mkroot package builds root file systems for
> use in Open Containers, as well as PXE (network)
> and ISO (CDROM/USB) boot environments.  Each of
> the targets can use a set of CMake functions.*

The general structure of the `mkroot` tree is into sections that deliver for a different purpose:

  * `internetwide` comprises of rootfs's for the [InternetWide Architecture](http://internetwide.org/tag/architecture.html).  Since they usually build on concrete ARPA2 work packages, their names often look like `arpa2dns` and so on.
  * `contrib` comprises of rootfs's that may be useful in addition to the `internetwide` core.  They are not included by default, but define an option for each.  This is usually the place where external additions would land.
  * `local` comprises of rootfs's that are locally useful, but not (yet) intended for sharing.  This is used to insert rootfs's for your own use.  When they evolve into something generally useful, they may be proposed as part of the `contrib` hierarchy.  The general repository has no rootfs in this directory.

Every build target has its own description in a file like `internetwide/arpa2dns/README.MD` that indicates its purpose and perhaps some build hints.

The default build target builds all `internetwide` and `local` rootfs's.  They may also be built independently.  For `make`, the incarnation would be like `make internetwide/arpa2dns` to retrieve a built file system in `internetwide/arpa2dns/rootfs/` and, next to it, optional extra facilities for use with Open Containers, PXE and so on.

The assumption below is that you have setup a `mkhere` link in your build directory, or that you pointed to one in the `BUILDROOT_MKHERE` cache variable.  This will serve as a source of information.

## Adding a Component

```
add_rootfs(arpa2dns)
```

This defines a rootfs of the given name, in this case `arpa2dns`.  This is usually the start of a rootfs's `CMakeLists.txt` file.  It is a good habit to use the directory name as the rootfs name.

The rootfs name is setup in a variable `CURRENT_ROOTFS`, referenced by most of the following calls.

## Setup for Runtime Environments

Runtime environments are the places where the root file system is run, usually under some description of what resources should be made available and how.  We briefly refer to those as a "runkind".  By default, the runkind is `none`, calling for no extra work.

You can create any runkind that you like, and configure it in a cache variable `${CURRENT_ROOTFS}_RUNKIND`.  Multiple runkinds can be defined.

```
rootfs_runkind(OCI_bundle
	config.json
)
```

This introduces files to be configured to a resource-descriptive file.  The first argument names the supported runkind, any further arguments list file names to add when configuring for that runkind.  The directory should have a template bearing that name plus a `.in` extension.  CMake variables surrounded with `@` symbols will be expanded from this template to form the targeted file.


### Instances of Runtime Environments

For a given root file system, there may be a use to provide multiple instances,
each with different permissions but operating on that same root file system.
An example is to run multiple containers, say a few clients and a few servers,
for one set of software.

This is possible with named instances, and their corresponding instance files.
The name of an instance becomes a directory adjacent to the `rootfs` output
directory, and the instance files are configured and installed underneath.

```
rootfs_runkind(OCI_bundle
	net4demo6bed4.sh
	demo.sh
	INSTANCES peer1 peer2 peer3 peer4
	          router5 router6 router7
	          native8765 native4321
	INSTFILES config.json host4demo6bed4.sh
)
```

## Adding Directories and Files

```
rootfs_directories(
	/etc
	/dev
	/bin /sbin /usr/bin /usr/sbin
	/lib /usr/lib
	/proc /sys
)
```

This creates the listed directories in the root file system.  Directories will be empty and owned by root.

```
rootfs_files(
	/etc/nsswitch.conf
	/etc/ld.so.conf
	/etc/ld.so.conf.d/*
	README.MD
)
```

This can retrieve files from a number of places.

  * When the file starts with a slash, it is retrieved from the `mkhere` environment, and passed into the root file system in the same location.
  * When the file has no initial slash, it is retrieved from CMake's source directory for the rootfs, and placed in the root directory.
  * When the file has no initial slash and cannot be found in CMake's source directory, then the attachment `.in` is added and another lookup in the source directory is tried; if it works, then `configure_file()` is used and `@...@` symbols will be substituted.

Sometimes, you need an `@` symbol in a `.in` file, for example in identity strings.  This does not seem to be handled by CMake's `configure_file()` but we simply defined `@APESTAARTJE@` which expands to a literal `@`.


## Downloading Files

```
rootfs_downloads(
	"http://internetwide.org/index.html"
	"http://internetwide.org/tag/identity.html"
	DESTDIR /var/www/html
)
```

Download files from a given URL, and install in the root file system.  The `DESTDIR` option indicates where in the root file system the files should be stored, the default beint the root directory.   When a URL ends in a slash, no file name can be inferred.  An additional `RENAME` option may be used to set the file name, but this only works for a single download URL.  Note that multiple calls to `rootfs_downloads()` may be made.  Downloads are cached in a `FetchedFiles` subdirectory under CMake's binary directory for the rootfs.

## Adding OS Packages

```
rootfs_ospackages(
        bash
        python3.7-minimal
        libpython3.7-stdlib
        knot knot-dnsutils
)
```

Import packages from the `mkhere` environment.  These packages will be installed when needed.  In addition to the packages, any links and libraries from binary executables will be added to come to self-contained packages in spite of dependencies.

The option `MKROOT_DEBUGGING_SUPPORT` can be set to selectively add OS packages listed after a `DEBUG_SUPPORT` keyword, as in

```
rootfs_ospackages(
        bash
        python3.7-minimal
        knot knot-dnsutils
        DEBUG_SUPPORT ldnsutils tshark gdb strace ltrace
)
```

## Building Packages from Source

```
rootfs_packages(
        twin
	arpa2shell
)
```

Build the named `mkhere` packages from source, possibly after retrieving it first.  Extract the package, along with any libraries from the build environment that its executable binaries need.

It is possible to specify the modifiers `VERSION`, `VARIANT` and `FLAVOUR` as they are used by the `mkhere` package, by postfixing it to the package name, as in

```
rootfs_packages(
	twin       VERSION=6.6.6
	arpa2shell VERSION=12.13 VARIANT=zsh FLAVOUR=zesty
)
```

If you want, you can manually set CMake variables too, like `zonemaster_FLAVOUR_knot`.

## Users and Groups

```
roofs_user (root UID 0
	GROUP wheel GID 0
	HOME /root SHELL /bin/ash
	NOTE "Container Administrator"
)

rootfs_user (postfix GROUP postfix
	NOTE "Postfix MTA")

rootfs_user (nobody  GROUP nobody
	UID 65534 GID 65534
	HOME /nonexistent SHELL /usr/sbin/nologin
	NOTE "Ultimately Powerless User")
```

This defines a single user name, and when `GROUP` is provided also a group name.
The `UID` and `GID` values are used as numeric values, defaulting to an incremental
numbering scheme and with a default `GID` value that equals the `UID` value.

When additional users should be added to a group, add a `GIDS` declaration with
as many `UID` values as desired.  They will be added on top of the `GID` value
or, lacking that, the `UID` value to which it defaults.  This is only useful and
only possible while declaring a `GROUP` name.

The `SHELL` and `HOME` variable mean what they say, and the `NOTE` is also
added in the description field.  The results end up in `/etc/passwd` and,
when the `GROUP` is named, also in `/etc/group`.

After this function, CMake variables `$UID_${USER}` and `$GID_${GROUP}` are set
to the `UID` and `GID` values that were assigned in the files.  This enables
further scripting to use values such as `$UID_postfix` to reference the allocated
numeric userid value.

**TODO:** There is no support yet for adding users to a previously declared group.


## Logging Directories

It is common for programs to log into `rootfs/var`, but it is makes administration
easier if they are all available centrally.  For that purpose, logging can be
collected into `${CMAKE_BINARY_DIR}/log` in a subdirectory named by the current
root filesystem.

Programs may not all run as `root`, and may therefore have insufficient rights
if they want to write directly to a logging directory.  The general resolution
is to create a separate logging directory, preferrably underneath the central
one, with a different owner/group.

```
rootfs_logdir (
	USER apache GROUP apache
	apache2)

This creates a central logging subdirectory `apache2` owned by `apache:apache`
according to previously introduced user/group names in a `rootfs_user()` call.


## Variable Directories (Persistent State)

It is common for programs to store their state in `/var` under a directory
like `/var/lib/kamailio`.  Most containers will take this directory out and
store a separate `vardir` next to `rootfs` and mount it over `/var`.

Populagint this `vardir` may use information stored in the origin `/var`
directory, where various programs may have left it.  Since that is part
of a root file system that may be wiped and rewritten at any time, it is
useful to maintain persistent state across builds with this `vardir` approach.

```
rootfs_vardir (
	USER kamailio GROUP kamailio PERMISSIONS o-rwx
	DIRECTORIES lib/kamailio
)
```

This creates a subdirectory `vardir/lib/kamailio`, usually mounted later as
`/var/lib/kamailio`.  The user and group are set to `UID_kamailio` and `GID_kamailio`
as prepared by preceding `rootfs_user()` definitions.  The permissions are
set to the given arguments to the `chmod(1)` command.

```
rootfs_vardir (
	USER kamailio GROUP kamailio
	FILES lib/kamailio/*
)
```

This copies any matching file under `rootfs/var` to their new location in
`vardir`, but only those not there yet.  The `FILES` entries match by
glob patternsm with support for recursion. This allows persistent changes
to the stored files.  User and group are set as before, the permissions
are unchanged.

In general, absent `PERMISSIONS` cause no change.  Absent `USER` and/or
`GROUP` reverts to `root` account 0.  Existing `DIRECTORIES` are also
set to these values, but existing `FILES` are untouched.  Since the
`USER`, `GROUP` and `PERMISSIONS` encompass the entire `rootfs_vardir()`
declaration, it is possible to use more of them in sequence.

The result of this command is that `rootfs/var` is a store from which
selected parts can be initialised, but once that is done they are
persistent.  Wiping the `vardir` contents causes fresh initialisation.


## Fixup Scripts

```
rootfs_fixups(
	testfix.sh
	otherfix.csh
)
```

This defines fixup scripts that are run to tweak the target file system as desired.  The scripts run chrooted, and any files referenced are therefore part of the target file system.  Some tweaks may become a standard facility, and are part of the `fixup` directory in the project root.  The extensions to the script names help to diversify them across script interpreters on the target system, but usually the hash-bang notation will take care of that.

After a special word `FIX_BUILDDIR`, any further scripts are run in the build directory.  This means that they can change the `vardir` and other things that might be mounted from there.  There is still protection from `chroot`, but one level up.  The `/bin`, `/usr/bin`, `/lib` and `/usr/lib` directories are linked from the build directory, but you generally may want to prefix `rootfs/` before any paths.  Be careful not to misuse the empowerment.

The following environment variables are available with the same value as in the CMake cache: `IWO_DOMAIN_ISP`.

## Command Pipes

Like a trans-dimensional portal, `pico` or "pipelined commands" can be used to
warp a sequence of strings from one container to another.  This only works when
both intend to connect.  See the
[mkroot-utils](https://gitlab.com/arpa2/mkroot-utils)
for the details of the `picoput` and `picoget` commands, and how they are used
to pass not only commands but also environment variables.

```
rootfs_cmdpipe (zonectl zone READS  ctl )
rootfs_cmdpipe (zonectl ctl  WRITES zone)
```

This is a mirrorred pair, discussing commands that are named `zonectl` as a group.
The first occurs in the CMakeLists.txt with rootfs instance `zone0`, the second
in the CMakeLists.txt for `ctl0`.

The communication direction is from writer `ctl0` to reader `zone0` (or whatever
your instance number is, `0` is the simple/easy situation).  The writer
tries to send and retries later when an attempt fails; the reader runs a loop of
fetching a command and processing it, which can be simply done from `/etc/inittab`.

This is implemented with `mkfifo` for the first of these two occurrences to be
configured; the second will make a hard link to the same FIFO pipe.  These files
are written under `${CMAKE_BINARY_DIR}/pico` and mounted from there.  Queue
support is implied; that allows sending to a container that is currently offline.

A very simple usage example is `ctl0` asking to add a zone, for example with

```
/bin/picoput /pico/zone0/zonectl.pico.out ZONE=example.com add
```


## Declaring Services

```
rootfs_ports (
	nginx0
	IFACE_IDENT ::a2:80a
	TCP_IN 80 443
)
```

This gives a container name `nginx0` and an interface identifier `::a2:80a` to be appended to the /96 prefixes for the various connected interfaces.  It also specifies service ports `80` and `443` on the IPv6 address on the `mkroot.dmz` bridge.  In this case, `TCP_IN` for input TCP ports, but `UDP`, `SCPT` and `DCCP` are also supported; besides that, `_OUT` can be used to mention specific ports for outgoing traffic, or just a protocol like `TCP` for bidirectionality.

Clients do not reach out from specific ports, but from a range of [ephemeral ports](https://en.wikipedia.org/wiki/Ephemeral_ports).  Client service may well occur on the same host as incoming service, so there is a separate `_CLIENT` setting with no argument for each of the protocols.  The range of ephemeral ports can be tuned with `IWO_NFT_EPHEMERAL_RANGE` but is by default set broad enough to cover both IANA standardised ranges and Linux' inexplicable extensions.  An example could be a mail service, which receives on fixed TCP ports but is a client to other mail servers.

```
rootfs_ports (
	mail0
	IFACE_IDENT ::a2:25
	TCP_IN 25 587
	TCP_CLIENT
)
```

    **TODO:** There is a need to cover remote IPv4 hosts too.  This requires dynamic NAT and services to learn about the externally shown ports, such as STUN, and perhaps carrier protocols like SOCKS5 or TURN.  For now, clients can only connect to IPv6 services, which may actually be sufficient for our core protocols.

This declaration can be used for a container-specific `nftables` firewall, to be run inside its name space.  It is also used to generate variables for IPv4 port forwarding that the operator may assign.  Without these settings, the service is IPv6-only (which may be useful during tests).  If you need to inspect what is going on, you may benefit from counters, possibly with a tool like [nftwatch](https://github.com/flyingrhinonz/nftwatch) run from the host with `ip netns exec xxx nftwatch` to see what is happening).

The port forwarding variables generated for this declaration are `IWO_HOST_nginx0_TCP_80` and `IWO_HOST_nginx0_TCP_443`.  They end up in `portfwd.d/nginx0.map` next to the `rootfs/` for `internetwide/pub2dmz` and are incorporated by its `net4pub.sh` script.  Reloading of port mapping into a running `pub2dmz` environment is possible with `net4pub.sh portfwd`.

```
rootfs_ports (
	...
	IPV6ONLY
	...
)
```

This declaration would be typical for a eer-to-peer service, because they generally work better if they are IPv6-only.  We introduced 6bed4 in the router to make IPv6 available anywhere.  The result is usually that peers need no intermediate at a level higher than IP routing, which ultimately benefits privacy.  By setting `IPV6ONLY` or its alias `IP6ONLY` there will be no variables for IPv4 address:port port forwarding.  Typical usecase would be a SIP server which negotiates media endpoints to use with RTP.

As part of processing, link-local addresses for the given host are also derived, using an internal procedure `derive_linkaddrs (mail0)` that will produce `IWO_mail0_dmz0br_fe80`, `IWO_mail0_dmz0if_MAC`, and so on, for the hostname (here `mail0`), the various interfaces (shown here is just `dmz0`) on both sides (`br` for bridge-side or metal-side, `if` for interface-side or container-side), and in the variants of a MAC and `fe80::/64` link-local addresses.  An especially noteworthy one is the global alias `IWO_dmz0_OUTROUTE_fe80` which is the link-local address for outbound routing over `dmz0`, as needed by most containers.


## Shared Keys between Containers

```
rootfs_secret (KEYNAME
	SEED "xxx..."
	[BITS 128|256|512]
	[HEX|BASE16|BASE32|BASE64]
)
```

This derives a key and stores it in `${IWO_SECRET_KEYNAME}`, sized 256 bits (or 128 or 512, if `BITS` is specified) and represents the output in hexadecimal (or `BASE32` or `BASE64` if so requested).  Do not store the key in the cache or another name scope but rely on consistent values for similar runs.

The following factors scatter the key:

  * A secret that is specific to the build.  It is stored in `${CMAKE_BINARY_DIR}/.base_secret` and as long as that is kept it will be reused for later rebuilds.  Only the first run will initialise it with a long random bit sequence.
  * The number of `BITS`, so keys are not each other's prefix.
  * The `SEED "xxx..."` value.  Make it long enough to be secure.  A random UUID might be generated to cover 128 bits, for example, or you might run `hexdump /dev/urandom | head | openssl sha512`.

Note how the `SEED "xxx..."` can be published in source repositories because it is only one part of the scattering process.  There is enough entropy in `${CMAKE_BINARY_DIR}/.base_secret` to avoid derivation of the secret from only that information.  The purpose of the `SEED "xxx..."` is to avoid derived relations between the keys of various containers.

Repeated runs will derive the same secret, which is why they need not be stored.  Following the same logic, another container can derive the same key value, perhaps with another `KEYNAME`; this provides a shared key between two containers.  It is also possible to derive it in another representation, for instance `BASE64` in one container and `HEX` in another; their binary forms match.

*Future mechanisms may be added for secret sharing across build directories; this may be based on Diffie-Hellmann.*


## Declaring Subnets between Containers

Containers can declare any number of subnets that they share with other containers, in the current as well as other Site Instances.  It names the host names of peers and expects each peer to include its own name.  There is flexibility in the interconnect, in that the peer may include or exclude names on the local peers list.  This enables configurations such as clients that only connect to servers, but not to other clients; while servers welcome all clients but perhaps not other servers (at least not on the same subnet).

```
rootfs_subnet (50053
	NAME nsupdate
	PURPOSE "Pass DNS information from hidden master to public masters"
	SALT "343aa7b418b4920f2853b02080ed0663a8310693baf8297d6a8a729d4282c177"
	LOCAL name zone
	PEERS name zone
	IP_ANY
)
```

This declares a subnet with informal name `nsupdate` communicating over port 50053.  The subnet is (currently) implemented with Wireguard, so this is an UDP port.  For subnet connections to succeed, peers must use the same port number and the same salt.  The port number allows the peers to connect, and the salt establishes mutual security.

The `SALT` adds confusion to the key material, and make it more different than other local key material.  Wireguard combines it with the `base_secret` to derive a pre-shared to key offer protection from quantum computer attacks, including future decryption of stored information of today.  It is a best practice to provide 256-bit random values in the `SALT` parameter.  Since salt is not secret, we store them openly in the "mkroot" repository (unlike the `base_secret` which kneads it into a secret).

The `LOCAL` list provides the local host names that implement this subnet.  This list may be the same as `PEERS` below, or it may be completely different.  It is furthermore possible to split a `LOCAL` list into multiple `rootfs_subnet()` declarations that use the same name, port and salt; these define partial views on the same subnet, each with possibly different `PEERS` and firewall.

The `PEERS` list iterates the host names of peers, without their instance number.  In a Site with Instances 0, 1 and 3, including the current one, the `PEERS name zone` would add support for connections with `name0`, `zone0`, `name1`, `zone`, `name3` and `zone3`.  Note that it would be possible for a host to list its own name; this enables connections to the same hosts in other Site Instances, possibly to send and receive updates.

Finally, the `IP_ANY` is a placeholder for future firewall facilities.  For now, this declaration states that all traffic over the subnet is trusted.  Future variantes may constrain ports, and possible the direction of traffic, to distinguish the behaviour as a client or server on the subnet.

This subnet facility is far more refined than the `bck0` backend.  It scopes communication between peers, it adds encryption and it allows connections to other Instances of the same Site across an untrusted network.  The latter is helpful to carry internal replication protocols, and thereby to present a replicated external service with more reliable online presence.

There will be a `@IWO_SUBNET_nsupdate_HOSTS@` pattern for inclusion in `/etc/hosts.in` templates.  This defines names such as `name3.nsupdate.localdomain` to be used from any scripts that need a name.  There willl also be `@IWO_SUBNET_nsupdate_HOST_name0@` for the IPv6 address of `name0` on the `nsupdate` subnet, and `@IWO_SUBNET_nsupdate_HOSTS_name@` for, respectively, the IPv6 address of `name0` via the `nsupdate` subnet, and a semicolon-separated list of all `name` peer addresses via the `nsupdate` subnet; you may want to exclude your own address from the latter by comparing the strings in it with the former.


## Publishing DNS records

The ISP zone configured in `${IWO_DOMAIN_ISP}` has a mostly predictable configuration, and can be generated.  As part of this, DNS records can be posted.  These are not suitable for the more frivolous setup for customer domains.  This is an include file without a `SOA` record of itself.

```
rootfs_dnsrecord ("mail0" "MX" 10 "mail0")
```

This sets up a DNS record in the file `/etc/mkroot/ispzone.txt` in the `internetwide/zonesmater` container.  The first word is a name (and may be `""` or `"@"` to reference the domain) and the second word is the record type.  Some record types are given special treatment:

  * `TXT` is followed by strings, which will be surrounded with double quotes
  * `A` can be derived from port forwarding strings; prefix a list of these with `PORTFWD` to generate an IPv4 address *only if* the same IPv4 address occurs in all strings
  * Future adaptations are added when they seem sensible

You can add a `COMMENT` line that will be inserted before the record.  There are already comments that name the container that provides the records.


## Plugins to the Web Server

The webserver for the ISP runs under `web0.${IWO_DOMAIN_ISP}` and is expected to be aliased.  To allow scattered bits and pieces of web-published information, it supports plugins that run on the backend network.

```
rootfs_proxyweb ("/arpa2/xyz/"
	HTTP
	SERVER "${IWO_bck0_PREFIX96}:123:456"
	PORT 80)
```

This passes external traffic, presumably over HTTPS, over the trusted backend network which can use only HTTP for expedient backend access.

The limitations of HTTP have triggered an escape through `Upgrade:` headers into WebSockets, which need special treatment,

```
rootfs_proxyweb ("/arpa2/xyz/ws"
	WEBSOCKET
	SERVER "${IWO_bck0_PREFIX96}:123:456"
	PORT 80)
```

This redirects the URI prefix `http[s]://web0.${IWO_DOMAIN_ISP}/arpa2/xyz/` via a websocket proxy to the said IPv6 address and port 80.  The default assumption is a trusted internal network, so it uses `ws://` instead of `wss://` forwarding.

Many plugins, including several that we built, are standalone FastCGI programs that run their isolated functions and thereby yield very good security, which is vital for our secret-shuffling tools,

```
rootfs_proxyweb ("/arpa2/jkl/"
	FASCTCGI
	SERVER "${IWO_bck0_PREFIX96}:123:789"
	PORT 9090)
```

As with the rest, these interactions are not encrypted because they flow over the internal `bck0` backend network.

Another popular backend type is WSGI,

```
rootfs_proxyweb ("/arpa2/mno/"
	WSGI
	SERVER "${IWO_bck0_PREFIX96}:123:789"
	PORT 9090)
```

For stsatic files, a variation on this kind of declarations enables sharing of directories between containers.  A good usage pattern is to take a readonly portion of the web name space and export it writeable to a container that fills its content.

```
rootfs_proxyweb ("/arpa2/pqr/"
	DIRSHARE
	PATHVAR IWO_host0_CANDY_DIR)
```

The variable `@IWO_host0_CANDY_DIR@` can subsequently be used in a mount in `config.json.in`.
As part of the `DIRSHARE` option, it is possible to use `.shtml` extensions with Server-Side Includes, one of the earliest and simplest ways of creating dynamic website content based on a few simple patterns.

### Web-based Authentication

To answer *who are you?* by establishing a `user@domain.name` login.

```
rootfs_proxyweb (
	...
	ARPA2LOGIN "Login with Realm Crossover"
	LOGINFAIL "/path/to/loginfail.shtml")
```

This setup authenticates via a Quick-DiaSASL backend, using a TCP
connection to the `internetwide/lanservice` container via the
`mkroot.svc` bridge.  The server uses HTTP-SASL towards the HTTP client.
On Apache, this uses the `mod_arpa2_diasasl` module.

You can supply an optional `LOGINFAIL` path on the web server,
which will be presented to the website visitor when login fails.

### Web-based Authorisation

To answer *can you do this?* after establishing a login identity.
On Apache, this uses the `mod_arpa2_access` module.  It needs an
additional Access Type and Access Name definition.  This assumes
the ARPA2 Access Control framework; other HTTP authentication
mechanisms could also determine the login identity, but "mkroot"
does not support the generation of such configurations at present.

To establish the login identity for any Access Type, we currently
generate a warning when no `ARPA2LOGIN` clause is present and
silently add one.

```
rootfs_proxyweb (
	...
	ARPA2LOGIN ...
	...
	ACCESSFAIL "/path/to/accessfail.shtml"
	ACCESSTYPE Document
	ACCESSNAME "...")
```

This setup validates with ARPA2 Document Access, and follows the
[Access Name structure for Documents](http://common.arpa2.net/md_doc_ACCESS_DOCUMENT.html)
to locate a path to a (group of) documents.
You can supply an optional `ACCESSFAIL` path on the web server,
which will be presented to the website visitor when access fails.


```
rootfs_proxyweb (
	...
	ARPA2LOGIN ...
	...
	ACCESSFAIL "/path/to/accessfail.shtml"
	ACCESSTYPE Communication
	ACCESSNAME "recpt@domain.name")
```

This setup validates with ARPA2 Communication Access, following the
[Access Name structure for Communication](http://common.arpa2.net/md_doc_ACCESS_COMM.html).
The access name is the recipient of the attempted communication.
You can supply an optional `ACCESSFAIL` path on the web server,
which will be presented to the website visitor when access fails.

The `ACCESSNAME` directive may become a word list in future versions,
because there may be a need for expressive forms that derive parts
from URL paths.  For now, the content is passed to the underlying web
server, which means that any expression patterns available there
will be usable but may not be portable to other web servers.  Note
that no explicit web server is assumed by the other parameters to
`rootfs_proxyweb()`, so we may vary this in the future.

### Service Keys or Domain Key

For any Access Type, there is a need to define a Service Key,
as part of the
[stepwise key derivation for Rules](http://common.arpa2.net/md_doc_RULES.html)
which can be derived with the
[a2rule(8) command](http://internetwide.org/blog/2021/06/24/xs-5-ruledb-keying.html).
The definition of these Access Types is integrated into "mkroot" and uses either
`A2XS_SERVICEKEY_Xxx` for Access Type `Xxx` or derives it from `A2XS_DOMAINKEY`.

Generally, the `A2XS_DOMAINKEY` gives less work, but it also allows all Service Keys
to be derived.  In a "mkroot" setup, which is intended to host a domain, this would
not immediately be a problem (neither kind of key should be considered public).

When additional host names are added, especially others the the `IWO_DOMAIN_ISP`,
then a Service Key must be configured along with it (and never a Domain Key).


## Service Certificates

To construct public key certificates in a variety of output forms, use

```
rootfs_certificate(SERVER www.@IWO_DOMAIN_ISP@)
```

There are several options that can be used:

  * `SERVER` lists server names.  *Currently, exactly one server name must be listed.*
  * `PEM`, `DER` and `TLSPOOL` flag output forms.  For now, `PEM` is the default and `DER` may be added, but `TLSPOOL` is unimplmented.
  * `FILEBASE` is the path to the TLS files.  By default, it is `tlsdir/` next to `rootfs/` but it may also be a relative path to be found inside `rootfs/` or it may be an absolute path.
  * `REQS`, `CERT` and `PRIV` override paths that default to these names in lowercase underneath `FILEBASE`.  The files placed in them will be the `SERVER` name plus a timestamp `-%Y%m%d` and an extension `.pem` or `.der` for the form.
  * `NAME` can be an explicit extension of LDAP distinguishedNames prefixed with a slash and appended to `/CN=<SERVER>`; there is a default.
  * `DANE` followed by one or more transport/port strings like `"tcp/443"` can be used to request DANE support; this is not present in the builtin CA, but it is possible under `TLS_CONTROL_TOWER=ON` for which certificates are acquired via the Control Tower.

There are a few CMake variables to influence the certificate authority in use:

  * `TLS_OPENSSL_CONF` is the path to the `openssl.cnf` file to use; it may hold `@VARIABLE_NAMES@` that will be expanded.  The default is `${CMAKE_SOURCE_DIR}/ca/openssl.cnf.in`.
  * `TLS_OPENSSL_CADIR` is the directory in which OpenSSL certificate authority administration is setup.  The default is `${CMAKE_BINARY_DIR}/ca`.
  * `TLS_ROOTCERT_FILES` is a list of file names with trusted root certificates to add to `calist.pem` in the TLS directory of a project.

## Terminal Information

You can add `terminfo(5)` database entries for your selection of terminals
with a simple command

```
rootfs_terminfo(
	ansi
	linux
	xterm*
	vt*
)
```

Note how glob-styled patterns are possible, such as `vt*` that will likely
select things like `vt52`, `vt100` and `vt220` and perhaps more along the
same lines.


## Kernels and Kernel Modules

This is *early and experimental support* for retrieving a kernel with
specified modules.

Most of this is generic functionality, as all boot loader start an image
(usually a kernel) and can supply additional information (usually a rootfs).
Also, the rootfs (that we already build here) can often hold some form of
modules that can plug into the kernel.  This can be specified with this
target:

```
rootfs_kernel(
	ftdi_sio
	loop
)
```

The mere call of this function implied that the kernel should be retrieved,
in a form fit for a boot loader.  It is possible that more than one kernel
is provided.

**TODO:** Currently we install kernels in the root file system, which 

If you want to limit the output to a specific kernel version, start with
`VERSION=1.2.3` or so.  If you want to limit the output to a specific
variant, usually denoted by words in the kernel's file name, then you can
specify this with something like `VARIANT=smp` or VARIANT="smp pae"` or
such.

Note that the underlying operating system may have no kernel, or it may
not have one matching the `VERSION` and `VARIANT` constraints.  If this
is the case, you shall not have the kernel installed.  **TODO:** Error!

The named modules will be retrieved from their position in the underlying
file system, and they will be installed in the same location in the target
rootfs.  It is possible to name no modules at all.  When multiple kernels
are installed, then all these kernels will independently receive these
kernel modules.  **TODO:** Error when not found!

**TODO:** Modules may have dependencies on other modules; these should be
automatically included.  This is not done yet.

*As you can see, there are many TODO for the `rootfs_kernel()` command.*

