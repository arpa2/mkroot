#!bin/ash

chownhere() {
	USER="$1"
	DOMAIN="$2"
	UID=$(awk -F: '{ if ($1 == "'$USER'@'$DOMAIN'") print ($3); }' < etc/passwd | tail -n 1)
	if [ -n "$UID" ]
	then
		mkdir -p        "../homedir/$DOMAIN/$USER"
		chown -R "$UID" "../homedir/$DOMAIN/$USER"
	else
		echo >&2 "Found no numeric userid for $USER@$DOMAIN"
	fi
}

chownhere demo  arpa2.net
chownhere demo1 arpa2.net
chownhere demo2 arpa2.net
chownhere demo3 arpa2.net
chownhere john  ${IWO_DOMAIN_ISP:-example.com}
chownhere mary  ${IWO_DOMAIN_ISP:-example.com}
chownhere mary  example.org

