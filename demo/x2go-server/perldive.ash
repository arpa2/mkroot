#!/bin/ash
#
# Perl5 has an extensive @INC but it still is incomplete!
# Simply move the files where they are being sought...
#
# From: Rick van Rein <rick@openfortress.nl>


if [ ! -r /usr/lib/x86_64-linux-gnu/perl/5.28 ]
then
	ln -s /usr/lib/x86_64-linux-gnu/perl/5.28.* /usr/lib/x86_64-linux-gnu/perl/5.28
fi

if [ ! -r /usr/share/perl/5.28 ]
then
	ln -s /usr/share/perl/5.28.* /usr/share/perl/5.28
fi
