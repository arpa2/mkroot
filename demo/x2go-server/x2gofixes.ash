#!/bin/ash

chown root:x2gouser /usr/lib/x2go/libx2go-server-db-sqlite3-wrapper &&
chmod 2755 /usr/lib/x2go/libx2go-server-db-sqlite3-wrapper &&
rm -rf /var/lib/x2go/x2go_sessions &&
/usr/sbin/x2godbadmin --createdb

