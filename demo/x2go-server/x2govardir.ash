#!bin/ash

chownvardir() {
	USER="$1"
	VICTIM="$2"
	UID=$(awk -F: '{ if ($1 == "'$USER'") print ($3); }' < etc/passwd | tail -n 1)
	if [ -n "$UID" ]
	then
		mkdir -p        "../vardir/${VICTIM#/var/}"
		chown -R "$UID" "../vardir/${VICTIM#/var/}"
	else
		echo >&2 "Found no numeric userid for $USER"
	fi
}

chownvardir x2gouser /var/lib/x2go
