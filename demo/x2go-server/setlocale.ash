#!/bin/ash

localedef -f UTF-8 -i C C.UTF-8
update-locale LANG=C.UTF-8 LANGUAGE=C LC_ALL=C.UTF-8
