#!/bin/ash

MESSAGE="
Welcome to the TWINtty Workstation

This is a textmode interface that
can boot over PXE.  It uses TWIN as
its \"graphical\" interface.

You can open terminals for external
connections over SSH and MOSH.  The
Minicom menu serves you with serial
port connectivity using Minicom and
X/Y/ZMODEM uploads and downloads.

Application programs installed are
f-irc and w3m for IRC and HTTP.

(Enter or click OK to close this.)
"

/usr/bin/dialog --msgbox "$MESSAGE" 22 40

